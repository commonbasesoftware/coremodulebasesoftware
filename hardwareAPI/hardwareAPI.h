/**
 * @file hardwareAPI.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief This file provides definitions of the interface functions to the hardware level.
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_HARDWAREAPI_HARDWAREAPI_H_
#define ILB_HARDWAREAPI_HARDWAREAPI_H_

#include <stdint.h>
#include "../ilb.h"

//
//	General hardware initialisation
//
/**
 * General hardware initialisation routine. Is called by the base software before any other action is undertaken
 * @return ::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_hardwareInit(void);

//
//	Messaging interface
//
/**
 * Queries if the ILB communication line is busy and no message can be sent
 * @return ::ILB_HAL_OK if the line is busy, ILB_HAL_BUSY if line is busy, error code in case of error
 */
ILB_HAL_StatusTypeDef ILB_HAL_isLineBusy(void);

/**
 * Sends bytes into the ILB bus. No additional check if the message should be sent is undertaken. The status should
 * be checked by ILB_HAL_isLineBusy() beforehand. This function is non-blocking. The return code ::ILB_HAL_OK should
 * only indicate that the sending has been started. Success of message sending is indicated through the callback
 * lib_messageSendSuccess()
 * @param buffer	Pointer to first element in list in bytes to send
 * @param nOfBytes	Number of bytes to send
 * @return			::ILB_HAL_OK on successfull start of sending, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_UARTSendBytes(uint8_t* buffer, uint16_t nOfBytes);

//
//	Reset window handling
//
/**
 * Returns the number of currently stored power cycle flags. Flags are set when the reset window is entered and erased
 * when the window is exited.
 * @return	Number of currently active flags
 */
uint8_t ILB_HAL_getNumberOfPowerCycleFlags(void);

/**
 * Add a power cylce flag to the number of currently stored flags.
 */
void ILB_HAL_addPowerCycleFlag(void);

/**
 * Clear all power cycle flags currently stored.
 */
void ILB_HAL_clearPowerCycleFlags(void);

//
//	Hardware information
//
/**
 * Access to individual serial number
 * @return	serial number or UID of device
 */
uint32_t ILB_HAL_getUID(void);

//
//	Random number generator
//
/**
 * Generates a random or pseudo random number
 * @param randomNumber	pointer to store random number in
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_GenerateRandomNumber(uint32_t* randomNumber);

//
//	Power cycle
//
/**
 * Triggers a power reset of the device.
 */
void ILB_HAL_systemReset(void);

//
//	Memory interface
//
/**
 * Load light engine settings from the persistent memory
 * @param settings	Pointer to location where settings are stored
 * @param version	Version of supported settings
 * @return			::ILB_HAL_OK on success, error code otherwise. This includes a non-matching version
 */
ILB_HAL_StatusTypeDef ILB_HAL_loadLightEngineSettings(LightEngineSettings_t* settings, uint8_t version);

/**
 * Save light engine settings in the persistent memory
 * @param settings	Pointer to settings to store
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_saveLightEngineSettings(LightEngineSettings_t* settings);

//
//	Indicator access
//
/**
 * Set the hardware indicator
 * @param indicatorDef	Specifies which color and repeat settings
 * @param pattern1		Pattern for green led as byte code
 * @param pattern2		Pattern for red led as byte code
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_setIndicator(deviceIndicatorTypeDef indicatorDef, uint8_t pattern1, uint8_t pattern2);

//
//	Light Engine interface
//
/**
 * Set the PWM level of the two output channels without fade.
 * @param targetPWMA	PWM level of channel A as a value between 0 and 1000
 * @param targetPWMB	PWM level of channel B as a value between 0 and 1000
 */
void ILB_HAL_core_setLightEngine(uint16_t targetPWMA, uint16_t targetPWMB);

/**
 * Get the currently active PWM output levels
 * @param PWMA	current PWM level of channel A
 * @param PWMB	current PWM level of channel B
 */
void ILB_HAL_core_getLightEngineOutput(uint16_t* PWMA, uint16_t* PWMB);

//
//	Telemetry storing and retreival
//
/**
 * Store the telemetry of the device in the persisten memory
 * @param onTime		On time in seconds to save
 * @param burnTime		Burn time in secodns to save
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_storeTelemetry(uint32_t onTime, uint32_t burnTime);

/**
 * Retreive the stored telemetry (burn and on time) from the persistent memory
 * @param onTime	On time in seconds
 * @param burnTime	Burn time in seconds
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_getTelemetry(uint32_t* onTime, uint32_t* burnTime);

//
//	Functions called by hardware layer
//
/**
 * Called by hardware layer when a message has been completed
 * @param pData		First byte in received message
 * @param length	Number of bytes in message
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef messaging_addIncomingMessageToBuffer(uint8_t* pData, uint8_t length);		// called when message has been received

/**
 * Callback on successfull or failed message transmission. Called when last byte has been sent.
 * @param s			::ILB_HAL_OK on success, error code otherwise
 */
void lib_messageSendSuccess(ILB_HAL_StatusTypeDef s);								// called when message has been send with success

//
//	Random number and time keeping
//
/**
 * Get the number of ticks (ms) since start of the device
 * @return	Number of ms since the microcontroller has powered up
 */
uint32_t ILB_HAL_getTick(void);

/**
 * Delay the program execution. This function is blocking
 * @param delay	Time to delay in ms
 */
void ILB_HAL_Delay(uint32_t delay);

/**
 * Get a random or pseudo random number
 * @param nr	Location where random number should be stored
 * @return		::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_getRandomNumber(uint32_t* nr);

//
//	Logic loading, writing and eraseing
//
/**
 * Erase the logic from the memory
 * @return	::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_eraseLogic(void);										//!< erase the logic (or better the header of the logic)

/**
 * Unlock the memory to store or erase the logic. This function must be called before erasing or writing logic, otherwise the operation will fail.
 * @return	::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicPrepareMemory(void);								//!< prepare the memory to upload the logic

/**
 * Write a page of the logic.
 * @param pageNumber	Page index to write
 * @param pData			Beginning of data segment to write
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicWritePage(uint16_t pageNumber, uint32_t* pData);	//!< write the page with the provided data

/**
 * Secure the logic. This operatio protects the memory section against accidential manipulation
 * @return	::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicSecureLogic(void);								//!< secure the memory after the process has finished.

//
//	Storing and retreival of address (for driver peripheral mode)
//
/**
 * Get the address stored memory
 * @param address	Location where to write the stored address to
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_getAddress(uint8_t* address);

/**
 * Store a new address in memory
 * @param address	Address to store
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_storeAddress(uint8_t* address);

//
//	Memory access for logic
//
/**
 * Interface function to access eeprom for reading and writing. This interface is used by the core to store the address table and the logic to manage
 * persistent variables
 * @param accessor		Accessor specification
 * @param offsetBytes	Offset from beginning of memory
 * @param pVar			Variable to read or write
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_EEPROMInterface(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar);

/**
 * Erase the complete eeprom section managed by the logic and core
 * @return		::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_EEPROMErase(void);

//
//	ADC readout
//
/**
 * Trigger an ADC measurement
 * @param pValues			Location where read values should be stored
 * @param completeCallback	Callback executed when measurement is complete
 * @return					::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_StartADCMeasuremnt(ADCReadings_t* pValues, void (*completeCallback)(void));

//
//	Chip protection handling
//
/**
 * Check the protection status of the chip. If the protection status does not correspond to the requested level,
 * it will be changed and the chip power cycled.
 * @param level		level 0: no protection, level 1: read with mass erase on set to 0; debug interface active, level 2 and higher: full protection, disabling debug interface
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_CheckChipProtection(uint8_t level);

/**
 * Returns the currently active chip protection level.
 * @return Set level. 0xFF indicates that this feature is not implemented
 */
uint8_t ILB_HAL_GetChipProtection(void);

#endif /* ILB_HARDWAREAPI_HARDWAREAPI_H_ */
