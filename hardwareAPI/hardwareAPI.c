/*
 * hardwareAPI.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include "../ilbstd.h"
#include "../ilb.h"
#include "../version/version.h"

#define PRNG_MODULUS	42
#define PRNG_MULTIPLIER	8
#define PRNG_INCREMENT	(3 + ILB_BASE_SW_VERSION_MINOR + ILB_BASE_SW_VERSION_MAJOR + ILB_BASE_SW_VERSION_BUILD + PRNG_SEED)
#define PRNG_SEED		5

static uint32_t PRNG_value = PRNG_SEED;

__weak ILB_HAL_StatusTypeDef ILB_HAL_hardwareInit(void) { return ILB_HAL_ERROR; }

__weak ILB_HAL_StatusTypeDef ILB_HAL_isLineBusy(void) { return ILB_HAL_ERROR; }

__weak ILB_HAL_StatusTypeDef ILB_HAL_UARTSendBytes(uint8_t* buffer, uint16_t nOfBytes) { return ILB_HAL_ERROR; }


//
//	Power cycle flags
//
__weak uint8_t ILB_HAL_getNumberOfPowerCycleFlags(void) { return 0; }
__weak void ILB_HAL_addPowerCycleFlag(void) { };
__weak void ILB_HAL_clearPowerCycleFlags(void) { }

//
//	Hardware information
//
__weak uint32_t ILB_HAL_getUID(void) { return 0; }


//
//	Reset
//
__weak void ILB_HAL_systemReset(void) { }

//
//	Memory interface
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_loadLightEngineSettings(LightEngineSettings_t* settings, uint8_t version)
{
	return ILB_HAL_ERROR;
}

__weak ILB_HAL_StatusTypeDef ILB_HAL_saveLightEngineSettings(LightEngineSettings_t* settings) { return ILB_HAL_ERROR; }


//
//	Indicator access
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_setIndicator(deviceIndicatorTypeDef indicatorDef, uint8_t pattern1, uint8_t pattern2) { return ILB_HAL_ERROR; }

//
//	Light Engine interface
//
__weak void ILB_HAL_core_setLightEngine(uint16_t targetPWMA, uint16_t targetPWMB) { }

__weak void ILB_HAL_core_getLightEngineOutput(uint16_t* PWMA, uint16_t* PWMB)
{
	*PWMA = 0;
	*PWMB = 0;
}

//
//	Telemetry
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_storeTelemetry(uint32_t onTime, uint32_t burnTime)
{
	return ILB_HAL_ERROR;
}

__weak ILB_HAL_StatusTypeDef ILB_HAL_getTelemetry(uint32_t* onTime, uint32_t* burnTime)
{
	*onTime = 0;
	*burnTime = 0;
	return ILB_HAL_ERROR;
}

//
//	Random number and time keeping
//
__weak uint32_t ILB_HAL_getTick(void)
{
	return 0;
}

__weak void ILB_HAL_Delay(uint32_t delay)
{
	// nothing to do
}

__weak ILB_HAL_StatusTypeDef ILB_HAL_GenerateRandomNumber(uint32_t* randomNumber)
{
	PRNG_value = (PRNG_MULTIPLIER * PRNG_value + PRNG_INCREMENT) % PRNG_MODULUS;
	*randomNumber = PRNG_value;
	return ILB_HAL_OK;
}

//
//	Logic
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_eraseLogic(void) { return ILB_HAL_ERROR; }
__weak ILB_HAL_StatusTypeDef ILB_HAL_logicPrepareMemory(void) { return ILB_HAL_ERROR; }
__weak ILB_HAL_StatusTypeDef ILB_HAL_logicWritePage(uint16_t pageNumber, uint32_t* pData)  { return ILB_HAL_ERROR; }
__weak ILB_HAL_StatusTypeDef ILB_HAL_logicSecureLogic(void) { return ILB_HAL_ERROR; }


//
//	Memory access for logic
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_EEPROMInterface(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar) { return ILB_HAL_ERROR; }
__weak ILB_HAL_StatusTypeDef ILB_HAL_EEPROMErase(void) {return ILB_HAL_ERROR;}

//
//	Store and retreival of address for driver peripehral mode
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_getAddress(uint8_t* address) { *address = 0; return ILB_HAL_ERROR; }
__weak ILB_HAL_StatusTypeDef ILB_HAL_storeAddress(uint8_t* address) { return ILB_HAL_ERROR; }

//
//	ADC conversion functions
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_StartADCMeasuremnt(ADCReadings_t* pValues, void (*completeCallback)(void)) { return ILB_HAL_ERROR;}

//
//	Chip protection level handling
//
__weak ILB_HAL_StatusTypeDef ILB_HAL_CheckChipProtection(uint8_t level) { return ILB_HAL_OK; }
__weak uint8_t ILB_HAL_GetChipProtection(void) { return 0xFF; }
