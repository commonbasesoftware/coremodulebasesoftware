/*
 * keyChallenge.c
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#include "keyChallenge.h"
#include "../ilb.h"
#include "../scheduler/scheduler.h"
#include "../settings/settings.h"
#include "../hardwareAPI/hardwareAPI.h"

uint8_t currentKey = 0;											//!< currently active key that can be used to access special commands
keyMechanismStatusTypeDef currentKeyStatus = SECURITY_KEY_EXPIRED;

// private functions
uint8_t getKeyForOperation(uint8_t value, uint8_t index);
void keyValidityTimeout(void);

/**
 * Open a window using the key challenge.
 *
 * The challenge is valid for ::ILB_KEY_VALIDITY_MS
 * @param seed				Seed of the challenge
 * @param challengeIndex	Index of the challenge to be used with the seed
 */
void security_openKeyWindow(uint8_t* seed, uint8_t* challengeIndex)
{
	// generate the random number for the seed and the index
	uint32_t randomNumber;
	ILB_HAL_GenerateRandomNumber(&randomNumber);
	*seed = (randomNumber & 0xFF);
	while(1) {
		ILB_HAL_GenerateRandomNumber(&randomNumber);
		if ((randomNumber & 0x0F) > 9) {
			continue;
		} else {
			*challengeIndex = (randomNumber & 0x0F);
			break;
		}
	}
	*challengeIndex = (randomNumber & 0x0F);

	// store answer
	currentKey = getKeyForOperation(*seed, *challengeIndex);
	currentKeyStatus = SECURITY_KEY_VALID;

	scheduler_registerTimerFunction(&keyValidityTimeout, ILB_KEY_VALIDITY_MS);
}

keyMechanismStatusTypeDef security_useKey(uint8_t* key)
{
	if (currentKeyStatus == SECURITY_KEY_VALID &&
		*key == currentKey)
	{
		currentKeyStatus = SECURITY_KEY_EXPIRED;
		return SECURITY_KEY_VALID;
	}
	return SECURITY_KEY_EXPIRED;
}

/**
 *	Timeout function to set the current key status invalid
 *
 *	Sets the current key to invalid via the ::currentKeyStatus variable
 */
void keyValidityTimeout(void)
{
	currentKeyStatus = SECURITY_KEY_EXPIRED;
}

/**
 * Key generator function
 *
 * This function takes a value and an index and outputs a key that can be used to access restricted functions.
 * The key is valid for ::ILB_KEY_VALIDITY_MS
 *
 * @param value The value that shall be used for the operation
 * @param index Index of the operation. The index must be between 0 and 9. Exceeding this range will always return 0
 *
 * @retval Results of the operatio on the value
 */
uint8_t getKeyForOperation(uint8_t value, uint8_t index)
{
	uint16_t operationBase = value;
	switch (index) {
	case 0:
		operationBase = (operationBase << 2);
		break;
	case 1:
		operationBase = (operationBase * 2 + 1);
		break;
	case 2:
		operationBase =  (operationBase * 2 - 1);
		break;
	case 3:
		operationBase =  (operationBase << 1) - 1;
		break;
	case 4:
		operationBase =  ((operationBase * 3) - 5);
		break;
	case 5:
		operationBase =  ((operationBase + 10)/2);
		break;
	case 6:
		operationBase =  ((operationBase + 15) >> 1);
		break;
	case 7:
		operationBase = (operationBase^ 85);
		break;
	case 8:
		operationBase = (operationBase & 15);
		break;
	case 9:
		operationBase = (operationBase | 24);
		break;
	default:
	{
		// to prevent hacking by sending an out of range index, randomise the response
		uint32_t randomNumber = 0;
		ILB_HAL_GenerateRandomNumber(&randomNumber);
		operationBase = (randomNumber & 0xFF);
	}
	}
	return (uint8_t)(operationBase & 0xFF);
}
