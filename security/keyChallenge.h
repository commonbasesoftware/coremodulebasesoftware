/**
 * @file keyChallenge.h
 *
 * @date 09.08.2021
 * @author A.Niggebaum
 *
 * @brief Handling routines for the key challenge mechanism
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_ILB_SECURITY_KEYCHALLENGE_H_
#define ILB_ILB_SECURITY_KEYCHALLENGE_H_

#include "../ilb.h"

/**
 * Status of the key challenge. The access window may be open or closed
 */
typedef enum keyMechanismStatusEnum {
	SECURITY_KEY_VALID,												//!< indicates that the key is valid and can be used
	SECURITY_KEY_EXPIRED											//!< indicates that the key is invalid and has expired
} keyMechanismStatusTypeDef;

/**
 * Access to currently active key. The key is consumed and the window closed
 * @param key Key used.
 * @return ::SECURITY_KEY_VALID if the key is valid and the functionality can be accessed, ::SECURITY_KEY_EXPIRED otherwise
 */
keyMechanismStatusTypeDef security_useKey(uint8_t* key);

/**
 * Opens the key challenge window.
 * @param seed	Value to use the key challenge operation on
 * @param challengeIndex index of key challenge
 */
void security_openKeyWindow(uint8_t* seed, uint8_t* challengeIndex);

#endif /* ILB_ILB_SECURITY_KEYCHALLENGE_H_ */
