/**
 * @file version.h
 *
 * @date Sep 15, 2021
 * @author A.Niggebaum
 *
 * @brief Version and revision definitions for the base software
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_VERSION_VERSION_H_
#define ILB_VERSION_VERSION_H_

#define ILB_BASE_SW_VERSION_MAJOR			0
#define ILB_BASE_SW_VERSION_MINOR			1
#define ILB_BASE_SW_VERSION_BUILD			22

#endif /* ILB_VERSION_VERSION_H_ */
