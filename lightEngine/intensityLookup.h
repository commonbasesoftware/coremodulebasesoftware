/**
 * @file intensityLookup.h
 *
 * @date 07.12.2021
 * @author A.Niggebaum
 *
 * @brief Cosntants and defines for intenstiy lookup handling
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_LIGHTENGINE_INTENSITYLOOKUP_H_
#define ILB_LIGHTENGINE_INTENSITYLOOKUP_H_

#define ADVANCED_CMD_INTENSITY_MASK		0x0F		//!< mask for intensity lookup type (e.g. DALI, linear) in advanced set command
#define ADVANCED_CMD_FADE_TIME_MASK		0xF0		//!< mask for fade time multiplier in advanced set command

extern const uint8_t ILB_INTENSITY_LOOKUP_DALI[256];	//!< definition of lookup table for DALI
extern const uint8_t ILB_INTENSITY_LOOKUP_LDV[256];		//!< definition of lookup table for LEDVANCE internal lookup

extern const uint8_t ILB_INTENSITY_LOOKUP_REVERSE_DALI[256];	//!< reverse intensity lookup. Needed to be able to change mid fade
extern const uint8_t ILB_INTENSITY_LOOKUP_REVERSE_LDV[256];		//!< reverse intensity lookup. Needed to be able to change mid fade.

#endif /* ILB_LIGHTENGINE_INTENSITYLOOKUP_H_ */
