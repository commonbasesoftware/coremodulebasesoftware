/*
 * settings.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "../ilb.h"
#include "../ilbstd.h"
#include "settings.h"
#include "../messaging/messaging.h"
#include "../scheduler/scheduler.h"
#include "../hardwareAPI/hardwareAPI.h"
#include "../baseSoftware/state.h"
#include "../register/register.h"

//
//	Defines
//
#define LIGHT_ENGINE_SETTINGS_VERSION		1

#define COMMUNICATION_TIMEOUT_MS			500
#define MIRED_LOADING_CHUNK_SIZE			20			// number of bytes we request in one chunk from the cct lookup table
#define MIRED_LOOKUP_TABLE_MAX_SIZE			0xFF

#define REQUEST_VERSION_RESPONSE_SIZE				2	// contains the version and a 0x00 padding byte
#define REQUEST_HEADER_RESPONSE_SIZE				4	// contains limitA, limitB, channelMapping and follower Group
#define REQUEST_CCT_HEADER_RESPONSE_SIZE			5

#define CHANNEL_LIMIT_DEFAULT				102
#define CHANNEL_MAPPING_DEFAULT				(CHANNEL_MAPPING_DEF_1_TO_A | CHANNEL_MAPPING_DEF_2_TO_B)
//#define FOLLOWER_GROUP_DEFAULT				FOLLOWER_GROUP_NONE
#define FOLLOWER_GROUP_DEFAULT				FOLLOWER_GROUP_1
#define MIRED_MIN_DEFAULT					0
#define MIRED_MAX_DEFAULT					0
#define MIRED_STEP_DEFAULT					0
#define MIRED_LOOKUP_DEFAULT				0x80

//
//	Private variables
//
static LightEngineSettings_t* LightEngineSettings = NULL;
static uint8_t* miredLookup;
static LightEngineLoadingSettingsStatusTypeDef loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_DONE;
static uint8_t nOfRequestedCCTBytes = 0;		//!< tracker variable on number of requested CCT lookup bytes.
static uint8_t nOfCCTBytesToRequest = 0;

//
//	Function prototypes
//
void settingsRequestTimeout(void);
void settingsRequestCallback(ILBMessageStruct* m);

/**
 * Initialisation of light engine settings
 *
 * @retval Returns the loaded group membership
 */
uint8_t lightEngineSettings_init(void)
{
	// allocate memory for the settings structure
	LightEngineSettings = malloc(sizeof(*LightEngineSettings));

	// check if we have a stored settings structure. If so, use the loaded one, otherwise use the default values.
	if (ILB_HAL_loadLightEngineSettings(LightEngineSettings, LIGHT_ENGINE_SETTINGS_VERSION) != ILB_HAL_OK)
	{
		// no structure or incompatible structure found. We need to load the defaults.
		LightEngineSettings->version = LIGHT_ENGINE_SETTINGS_VERSION;
		LightEngineSettings->Limit_CHA = CHANNEL_LIMIT_DEFAULT;
		LightEngineSettings->Limit_CHB = CHANNEL_LIMIT_DEFAULT;
		LightEngineSettings->channelMapping = CHANNEL_MAPPING_DEFAULT;
		LightEngineSettings->followerGroup = FOLLOWER_GROUP_DEFAULT;
		LightEngineSettings->miredMin = MIRED_MIN_DEFAULT;
		LightEngineSettings->miredMax = MIRED_MAX_DEFAULT;
		LightEngineSettings->miredStep = MIRED_STEP_DEFAULT;
		miredLookup = malloc(sizeof(*miredLookup));
		miredLookup[0] = MIRED_LOOKUP_DEFAULT;
		LightEngineSettings->miredLookup = &miredLookup[0];
	}
	else
	{
		// update the reference to the mired lookup table
		miredLookup = LightEngineSettings->miredLookup;
	}

	// update the pwm information
	register_updatePWMInformation(LightEngineSettings);

	return LightEngineSettings->followerGroup;
}

/**
 * Trigger to load
 */
void lightEngineSettings_startUpload(void)
{
	// make sure that we are initialised.
	if (LightEngineSettings == NULL)
	{
		lightEngineSettings_init();
	}

	// set the state of the system
	ilb_setState(ILB_BASE_SETTINGS_LOADING);

	// set status start
	loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_GET_VERSION;

	// prepare the first message. We want to request the limits
	ILBMessageStruct message;
	message.address = ILB_MESSAGE_BROADCAST_ADDRESS;
	message.identifier = ILB_GENERATE_IDENTIFIER;
	message.messageType = ILB_MESSAGE_COMMAND;
	message.payloadSize = 2;
	message.payload[0] = ILB_CONFIGURE_PWM_SETTINGS;
	message.payload[1] = LIGHT_ENGINE_LOADING_CMD_GET_VERSION;

	messaging_sendMessage(&message, settingsRequestCallback, COMMUNICATION_TIMEOUT_MS, settingsRequestTimeout);
}

/**
 * Handler when the settings request has timed out
 */
void settingsRequestTimeout(void)
{
	// we requested a setting but did not get an answer in time
	loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_ABORT;
}

/**
 * Handler of answers to the requests of the data
 * @param m
 */
void settingsRequestCallback(ILBMessageStruct* m)
{
	// check if we are in an error state. This may happen if the answer is delayed longer than it should
	if (loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_ABORT || loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_ERROR || loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_DONE)
	{
		return;
	}
	// handle the message depending of the state
	// step 1: getting the limits
	if (loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_GET_VERSION)
	{
		// check for correct size of payload
		if (m->payloadSize == REQUEST_VERSION_RESPONSE_SIZE &&
			m->payload[0] == LIGHT_ENGINE_SETTINGS_VERSION)
		{
			// store version
			LightEngineSettings->version = m->payload[0];

			// respond ok
			messaging_respondErrorMessage(m, ILB_OK);

			// proceed to next step
			ILBMessageStruct message;
			message.address = ILB_MESSAGE_BROADCAST_ADDRESS;
			message.identifier = ILB_GENERATE_IDENTIFIER;
			message.messageType = ILB_MESSAGE_COMMAND;
			message.payloadSize = 2;
			message.payload[0] = ILB_CONFIGURE_PWM_SETTINGS;
			message.payload[1] = LIGHT_ENGINE_LOADING_CMD_GET_HEADER;

			// set status
			loadingStatus = LIGHT_ENGINE_SETTINGS_HEADER;

			// send message
			messaging_sendMessage(&message, settingsRequestCallback, COMMUNICATION_TIMEOUT_MS, settingsRequestTimeout);

			return;
		}
		else
		{
			// either payload size or version to not match
			// respond error and wait for other chance
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			// give another chance
			scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
			return;
		}
	}

	// receiving of limits, lookup and mapping
	else if (loadingStatus == LIGHT_ENGINE_SETTINGS_HEADER)
	{
		// check correct size of payload
		if (m->payloadSize == REQUEST_HEADER_RESPONSE_SIZE)
		{
			// store in structure
			LightEngineSettings->Limit_CHA = m->payload[0];
			LightEngineSettings->Limit_CHB = m->payload[1];
			LightEngineSettings->channelMapping = m->payload[2];
			LightEngineSettings->followerGroup = m->payload[3];

			// send acknowledge
			messaging_respondErrorMessage(m, ILB_OK);

			// proceed to next step
			loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_HEADER;

			ILBMessageStruct message;
			message.address = ILB_MESSAGE_BROADCAST_ADDRESS;
			message.identifier = ILB_GENERATE_IDENTIFIER;
			message.messageType = ILB_MESSAGE_COMMAND;
			message.payloadSize = 2;
			message.payload[0] = ILB_CONFIGURE_PWM_SETTINGS;
			message.payload[1] = LIGHT_ENGINE_LOADING_CMD_GET_CCT_HEADER;

			messaging_sendMessage(&message, settingsRequestCallback, COMMUNICATION_TIMEOUT_MS, settingsRequestTimeout);

			return;
		}
		else
		{
			// response size does not match
			// respond error and wait for other chance
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			// give another chance
			scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
			return;
		}
	}

	// handle the CCT lookup header
	else if (loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_HEADER)
	{
		// check payload size. We expect5 bytes (mired min: 2bytes, mired max: 2bytes, mired step: 1byte)
		if (m->payloadSize == REQUEST_CCT_HEADER_RESPONSE_SIZE)
		{
			// parse content
			uint16_t miredMin = ((m->payload[0] << 8) | m->payload[1]);
			uint16_t miredMax = ((m->payload[2] << 8) | m->payload[3]);
			uint8_t  miredStep = m->payload[4];

			// do some sanity checks.
			// check that the maximal value is indeed larger than the minimal value
			if (miredMax < miredMin || miredMax == miredMin || miredStep == 0)
			{
				// respond error
				messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
				// give another chance
				scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
				return;
			}

			// check maximal allowed size of table
			if ((miredMax - miredMin)/miredStep > MIRED_LOOKUP_TABLE_MAX_SIZE)
			{
				// respond error
				messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
				// give another chance
				scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
				return;
			}

			// store values
			LightEngineSettings->miredMin = miredMin;
			LightEngineSettings->miredMax = miredMax;
			LightEngineSettings->miredStep = miredStep;

			// acknowledge
			messaging_respondErrorMessage(m, ILB_OK);

			// next, we need to load the cct lookup table.
			// we can request the number of byte we want to load in chunks.

			// set state
			loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_LOOKUP;

			// reset tracker variable
			nOfRequestedCCTBytes = 0;

			// calculate number of entries. We need at least one
			nOfCCTBytesToRequest = ((miredMax - miredMin) / miredStep + 1);
			if (nOfCCTBytesToRequest == 0)
			{
				nOfCCTBytesToRequest = 1;
			}

			// calculate number of bytes to request in this message
			uint8_t nOfBytesInBatch = nOfCCTBytesToRequest;
			if (nOfBytesInBatch > MIRED_LOADING_CHUNK_SIZE)
			{
				nOfBytesInBatch = MIRED_LOADING_CHUNK_SIZE;
			}

			// allocate the memory
			miredLookup = malloc(sizeof(*miredLookup) * nOfCCTBytesToRequest);
			// store new pointer
			LightEngineSettings->miredLookup = &miredLookup[0];

			// request first batch
			ILBMessageStruct request;
			request.address = m->address;
			request.identifier = ILB_GENERATE_IDENTIFIER;
			request.messageType = ILB_MESSAGE_COMMAND;
			request.payloadSize = 4;
			request.payload[0] = ILB_CONFIGURE_PWM_SETTINGS;
			request.payload[1] = LIGHT_ENGINE_LOADING_CMD_GET_CCT_TABLE;
			request.payload[2] = nOfRequestedCCTBytes;
			request.payload[3] = nOfBytesInBatch;

			messaging_sendMessage(&request, settingsRequestCallback, COMMUNICATION_TIMEOUT_MS, settingsRequestTimeout);

			return;
		}
		else
		{
			// payload size does not match. Allow for try again
			// respond error
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			// give another chance
			scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
		}
	}
	// cct table request procedure
	else if (loadingStatus == LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_LOOKUP)
	{
		// we received an answer to the requested chunk of the lookup table
		// We expect a message of the chunk length if the remaining bytes exceed the chunk size. we
		// expect the difference if less than the chunk size of bytes are missing. All other conditions
		// are invalid
		if (m->payloadSize == MIRED_LOADING_CHUNK_SIZE ||
			m->payloadSize == (nOfCCTBytesToRequest - nOfRequestedCCTBytes))
		{
			// the payload size matches the expected number of bytes

			// push the received bytes into the structure
			memcpy(&miredLookup[nOfRequestedCCTBytes], &m->payload[0], m->payloadSize);

			// acknowledge receival
			messaging_respondErrorMessage(m, ILB_OK);

			// count up
			nOfRequestedCCTBytes += m->payloadSize;

			// check if we need to request more or save the values
			if (nOfRequestedCCTBytes == nOfCCTBytesToRequest)
			{
				// finished. We now need to store the values in memory
				ILB_HAL_StatusTypeDef result = ILB_HAL_saveLightEngineSettings(LightEngineSettings);

				// check result
				if (result == ILB_HAL_OK)
				{
					// all good.
					messaging_respondErrorMessage(m, ILB_OK);
					// set state to finished
					loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_DONE;
					return;
				}
				// something went wrong. Could not save the settings
				else
				{
					messaging_respondErrorMessage(m, ILB_ERROR);
					loadingStatus = LIGHT_ENGINE_SETTINGS_LOADING_ERROR;
				}
			}
			// bytes are missing
			else
			{
				// prepare the next message
				uint8_t nOfBytesInBatch = (nOfCCTBytesToRequest - nOfRequestedCCTBytes);
				if (nOfBytesInBatch > MIRED_LOADING_CHUNK_SIZE)
				{
					nOfBytesInBatch = MIRED_LOADING_CHUNK_SIZE;
				}

				ILBMessageStruct request;
				request.address = m->address;
				request.identifier = ILB_GENERATE_IDENTIFIER;
				request.messageType = ILB_MESSAGE_COMMAND;
				request.payloadSize = 4;
				request.payload[0] = ILB_CONFIGURE_PWM_SETTINGS;
				request.payload[1] = LIGHT_ENGINE_LOADING_CMD_GET_CCT_TABLE;
				request.payload[2] = nOfRequestedCCTBytes;
				request.payload[3] = nOfBytesInBatch;

				messaging_sendMessage(&request, settingsRequestCallback, COMMUNICATION_TIMEOUT_MS, settingsRequestTimeout);

				return;
			}
		}
		// payload size is not valid
		else
		{
			// revert back to initial values
			lightEngineSettings_init();
			// communicate the error
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			// give another chance
			scheduler_registerHandlerFunction(m->identifier, settingsRequestCallback, settingsRequestTimeout, COMMUNICATION_TIMEOUT_MS);
		}
	}
}


LightEngineLoadingSettingsStatusTypeDef lightEngineSettings_getStatus(void)
{
	return loadingStatus;
}


void lightEngineSettings_getChannelOutput(uint8_t level, uint16_t mired, uint16_t* CHA, uint16_t* CHB)
{
	// fail save
	if (LightEngineSettings == NULL)
	{
		*CHA = level;
		*CHB = 0;
		return;
	}

//	// Step 1: Apply intensity lookup
//	switch (LightEngineSettings->intensityLookup)
//	{
//	case INTENSITY_LOOKUP_LINEAR:
//		// nothign to do
//		break;
//
//	case INTENSITY_LOOKUP_DALI:
//		level = ILB_INTENSITY_LOOKUP_DALI[level];
//		break;
//
//	case INTENSITY_LOOKUP_LDV:
//		level = ILB_INTENSITY_LOOKUP_LDV[level];
//		break;
//	}

	// step 2: get the splitting ratio for the requested mired
	// push value in range if necessary
	if (mired > LightEngineSettings->miredMax)
	{
		mired = LightEngineSettings->miredMax;
	}
	if (mired < LightEngineSettings->miredMin)
	{
		mired = LightEngineSettings->miredMin;
	}

	// find index in look up table
	uint8_t CCTLUTIndex = 0;
	if (LightEngineSettings->miredStep != 0)
	{
		CCTLUTIndex = (mired - LightEngineSettings->miredMin)/LightEngineSettings->miredStep;
	}

	// get ratio
	uint8_t ratio = miredLookup[CCTLUTIndex];

	// split according to ratio
	uint32_t PWMA = ((uint32_t)(ratio) * (uint32_t)level * 1000) / ((uint32_t)(0xFF * 0xFF));
	uint32_t PWMB = ((uint32_t)(0xFF-ratio) * (uint32_t)level * 1000) / ((uint32_t)(0xFF * 0xFF));

	uint32_t outputA, outputB = 0;

	// Step 3: channel mapping
	// swap or discard according to channel mapping
	if (IS_BIT_SET(LightEngineSettings->channelMapping, CHANNEL_MAPPING_DEF_1_TO_A))
	{
		outputA = PWMA;
	}
	if (IS_BIT_SET(LightEngineSettings->channelMapping, CHANNEL_MAPPING_DEF_1_TO_B))
	{
		outputB = PWMA;
	}
	if (IS_BIT_SET(LightEngineSettings->channelMapping, CHANNEL_MAPPING_DEF_2_TO_A))
	{
		outputA = PWMB;
	}
	if (IS_BIT_SET(LightEngineSettings->channelMapping, CHANNEL_MAPPING_DEF_2_TO_B))
	{
		outputB = PWMB;
	}

	// step 4: intensity scaling
	outputA = (outputA * LightEngineSettings->Limit_CHA)/0xFF;
	outputB = (outputB * LightEngineSettings->Limit_CHB)/0xFF;

	// output
	*CHA = outputA;
	*CHB = outputB;
}

/**
 * Acccessor to structure
 * @param settings
 */
LightEngineSettings_t* lightEngineSettings_getSettings(void)
{
	return LightEngineSettings;
}

/**
 * Accessor to follower group
 * @return
 */
uint8_t lightEngineSettings_getFollowerGroup(void)
{
	return LightEngineSettings->followerGroup;
}
