/**
 * @file lightEngine.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Attached light engine related controls
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_LIGHTENGINE_LIGHTENGINE_H_
#define ILB_LIGHTENGINE_LIGHTENGINE_H_

#include <stdint.h>
#include "../ilb.h"

/**
 * Initialize all light engine related data structures. Reserve memory
 * @return
 */
uint8_t lightEngine_init(void);

/**
 * Set the color of the light engine. Intensity will stay constant
 * @param mired				Mired to set
 * @param fadeTimeMS		Fade time until new output is reached
 * @param intensityLookup	Intensity lookup to use
 */
void lightEngine_setColour(uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup);

/**
 * Set the level/intensity of the attached light engine. The color stays constant
 * @param level				Intensity level to set
 * @param fadeTimeMS		Time before the new output is reached
 * @param intensityLookup	Intensity lookup to use
 */
void lightEngine_setLevel(uint8_t level, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup);

/**
 * Set the level and color of the attached light engine.
 * @param level			New target level
 * @param mired			New target mired
 * @param fadeTimeMS	Fade time to new level and mired in ms
 * @param intensityLUT	Intensity lokup to use
 * @return	::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef lightEngine_startFade(uint8_t level, uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLUT);

/**
 * Get the status and power consumption estimates and measurements of attached light engine
 *
 * @param status				Status of the attached light engine. May be off, fading or on
 * @param primarySidePower		Current primary side power consumption (estimated)
 * @param secondarySidePower	Current secondary side power consumption (measured)
 */
void attachedLightEngine_getStatus(uint8_t* status, uint16_t* primarySidePower, uint16_t* secondarySidePower);		//!< access function for core logic to get status information of the light engine

/**
 * Should be called when a new measurement on the secondary side is ready. Calculates the estimated primary side power consumption
 *
 * @param latestMeasurement
 */
void lightEngine_updateMeasuredOutput(ADCReadings_t* latestMeasurement);			//!< accessor function to update light engine information with the latest readings


#endif /* ILB_LIGHTENGINE_LIGHTENGINE_H_ */
