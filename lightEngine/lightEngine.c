/*
 * lightEngine.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include "lightEngine.h"

#include <stddef.h>

#include "../hardwareAPI/hardwareAPI.h"
#include "../messaging/messaging.h"
#include "../scheduler/scheduler.h"
#include "../settings/settings.h"
#include "../baseSoftware/state.h"
#include "intensityLookup.h"
#include "settings.h"

//
//	Settings for fading
//
#define FADING_MAX_NUMBER_OF_SUPPORT_POINTS			4			//!< maximal number of support points in a fade when following the intensity lookup table
#define FADING_MIN_NOF_STEPS_PER_FADE_SECTION		10			//!< minimal number of points per fade section


//
//	Primary to secondary side power conversion
//
#define POWER_OUTPUT_DIVISOR		50			//!< output is calculated in mW. Dividing by 50 yields multiples of 0.05W, as defined in the ILB specifications
static uint16_t primarySidePowerInput = 0;
static uint16_t secondarySidePowerOutput = 0;

typedef struct FadeSectionStruct {
	uint16_t startA;
	uint16_t startB;
	uint32_t progress;
	uint32_t progressTarget;
} FadeSection_t;

typedef struct PropertyProgressStruct {
	uint16_t start;
	uint16_t stop;
} PropertyProgress_t;

typedef enum PropertyEnum {
	PROPERTY_LINEAR_LEVEL,
	PROPERTY_MIRED,
	PROPERTY_MAX,
} ProgressType;

typedef struct attachedLightEngineStruct {
	lightEngineStateTypeDef state;		//!< status of the light engine. E.g. on/off/fading
	uint32_t progress;					//!< progress of the current fade absolute
	uint32_t progressTarget;			//!< max value of progress
	uint8_t currentLevel;				//!< current level in % PWM as a value between 0x00 and 0xFF
	uint16_t currentMired;				//!< current mired output
	PropertyProgress_t property[PROPERTY_MAX];	//!< tracking of individual properties (mired, linear output and LUT output)
	IntensityLookup_t intensityLUT;		//!< currently used look up table for fade
	uint8_t currentFadeSection;			//!< currently active fade section
	uint8_t maxFadeSection;				//!< highest fade section that is part of this fade
	FadeSection_t fadeSections[FADING_MAX_NUMBER_OF_SUPPORT_POINTS + 2];	//!< list of fade sections. We need one more support points to get to the number of required sections and one additional for the final target
} AttachedLightEngineFade_t;

typedef struct peripheralLightEngineStruct {
	uint8_t address;
	lightEngineStateTypeDef state;
	uint8_t currentLevel;
	uint8_t currentMired;
} PeripheralLightEngineStatus_t;

//
//	Private functions
//
void attachedLightEngine_fadeCallback(void);
void sendFadeFinishedNotification(void);

//
//	Private variables
//
// light engines are initialised as null pointer. Null pointer indicates that the type is not present
static AttachedLightEngineFade_t attachedLightEngine;

/**
 * Initialisation function for light engine
 *
 * Loads settings from memory or default values.
 *
 * @retval Returns the group membership
 */
uint8_t lightEngine_init(void)
{
	// initialise the structures
	uint8_t group = lightEngineSettings_init();

	return group;
}

/**
 *
 * @param mired
 * @param fadeTimeMS
 */
void lightEngine_setColour(uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup)
{
	lightEngine_startFade(attachedLightEngine.currentLevel, mired, fadeTimeMS, intensityLookup);
}

/**
 *
 * @param level
 * @param fadeTimeMS
 */
void lightEngine_setLevel(uint8_t level, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup)
{
	lightEngine_startFade(level, attachedLightEngine.currentMired, fadeTimeMS, intensityLookup);
}

/**
 * Starts a fade of the light engine.
 *
 * Prepares the structure for general tracking and also prepares the structures to follow the intensity lookup curve
 *
 * @param level			Target level of the fade
 * @param mired			Target mired of the fade
 * @param fadeTimeMS	fade time in ms
 * @param intensityLUT	Intensity lookup to use for the fade
 */
ILB_HAL_StatusTypeDef lightEngine_startFade(uint8_t level, uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLUT)
{

	// calculate how many overall steps we need for the fade
	uint32_t nOfSteps = fadeTimeMS/CORE_ATTACHED_LIGHT_ENGINE_FADE_RESOLUTION_MS;

	// update fade progress structure
	attachedLightEngine.progress = 0;
	attachedLightEngine.progressTarget = nOfSteps;

	// fill in the tracking structures
	attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].start = attachedLightEngine.currentLevel;
	attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].stop = level;
	attachedLightEngine.property[PROPERTY_MIRED].start = attachedLightEngine.currentMired;
	attachedLightEngine.property[PROPERTY_MIRED].stop = mired;

	// in case another look up system than linear is chosen, we need to adjust the starting level
	uint8_t actualStartLevel = attachedLightEngine.currentLevel;
	attachedLightEngine.intensityLUT = intensityLUT;
	switch (intensityLUT)
	{
	case INTENSITY_LOOKUP_DALI:
		actualStartLevel = ILB_INTENSITY_LOOKUP_REVERSE_DALI[attachedLightEngine.currentLevel];
		break;

	case INTENSITY_LOOKUP_LDV:
		actualStartLevel = ILB_INTENSITY_LOOKUP_REVERSE_LDV[attachedLightEngine.currentLevel];
		break;

	default:
		break;
	}

	// decide how many support sections to use
	uint32_t stepsPerFadeSection = FADING_MIN_NOF_STEPS_PER_FADE_SECTION;
	uint8_t nOfSupportPoints = nOfSteps/stepsPerFadeSection;
	// check if we need to adjust the step size
	if (nOfSupportPoints > FADING_MAX_NUMBER_OF_SUPPORT_POINTS + 1)
	{
		// re-adjust the points per fading section
		stepsPerFadeSection = nOfSteps/(FADING_MAX_NUMBER_OF_SUPPORT_POINTS + 1);
		nOfSupportPoints = FADING_MAX_NUMBER_OF_SUPPORT_POINTS + 1;
	}
	else if (nOfSupportPoints == 0)
	{
		nOfSupportPoints = 1;
	}

	// prepare the fade sections
	// set up the first fading point
	attachedLightEngine.currentFadeSection = 0;
	attachedLightEngine.maxFadeSection = nOfSupportPoints;

	uint16_t outputA, outputB = 0;
	int16_t levelDelta = (int16_t)attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].stop - (int16_t)actualStartLevel;
	int16_t miredDelta = (int16_t)mired - (int16_t)attachedLightEngine.currentMired;

	// calculate the start
	lightEngineSettings_getChannelOutput(actualStartLevel, attachedLightEngine.currentMired, &outputA, &outputB);
	attachedLightEngine.fadeSections[0].startA = outputA;
	attachedLightEngine.fadeSections[0].startB = outputB;

	// build up the fade sections
	for (uint8_t i=0; i<nOfSupportPoints; i++)
	{
		// calculate start position of fade section
		uint8_t fadeSectionTarget = (int16_t)actualStartLevel + ((int16_t)(i+1)*(levelDelta/(int16_t)nOfSupportPoints));
		uint16_t fadeSectionMired = (int16_t)attachedLightEngine.currentMired + (((int16_t)(i+1)*miredDelta)/(int16_t)nOfSupportPoints);

		// prepare progress in fade section
		attachedLightEngine.fadeSections[i].progress = 1;
		attachedLightEngine.fadeSections[i].progressTarget = stepsPerFadeSection;

		// apply intensity look up
		switch (intensityLUT)
		{
		case INTENSITY_LOOKUP_DALI:
			fadeSectionTarget = ILB_INTENSITY_LOOKUP_DALI[fadeSectionTarget];
			break;

		case INTENSITY_LOOKUP_LDV:
			fadeSectionTarget = ILB_INTENSITY_LOOKUP_LDV[fadeSectionTarget];
			break;

		default:
			break;
		}

		lightEngineSettings_getChannelOutput(fadeSectionTarget, fadeSectionMired, &outputA, &outputB);

		// the target of the fade is the start value of the NEXT section
		attachedLightEngine.fadeSections[i+1].startA = outputA;
		attachedLightEngine.fadeSections[i+1].startB = outputB;

		attachedLightEngine.fadeSections[i+1].progress = 1;
		attachedLightEngine.fadeSections[i+1].progressTarget = 0;

	}

	// Check if we need to apply immediately
	if (fadeTimeMS < CORE_ATTACHED_LIGHT_ENGINE_FADE_RESOLUTION_MS)
	{
		ILB_HAL_core_setLightEngine(attachedLightEngine.fadeSections[1].startA, attachedLightEngine.fadeSections[1].startB);
		// set state
		if (attachedLightEngine.fadeSections[1].startA != 0 || attachedLightEngine.fadeSections[1].startB != 0)
		{
			attachedLightEngine.state = LIGHT_ENGINE_ON;
		}
		else
		{
			attachedLightEngine.state = LIGHT_ENGINE_OFF;
		}
		attachedLightEngine.currentLevel = level;
		attachedLightEngine.currentMired = mired;
		return ILB_HAL_OK;
	}

	// set state
	attachedLightEngine.state |= LIGHT_ENGINE_FADING;

	// schedule tracker
	scheduler_registerTimerFunction(&attachedLightEngine_fadeCallback, CORE_ATTACHED_LIGHT_ENGINE_FADE_RESOLUTION_MS);

	return ILB_HAL_OK;

}

/**
 * Callback triggered when we need to adjust the PWM output of the attache light engine
 */
void attachedLightEngine_fadeCallback(void)
{
	// advance section
	attachedLightEngine.progress++;

	// advance currently active section
	attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progress++;

	// check if we need to advance a section
	if (attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progress > attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progressTarget)
	{
		// advance
		attachedLightEngine.currentFadeSection++;

		// check if we have reached the end of the fade
		if (attachedLightEngine.currentFadeSection >= attachedLightEngine.maxFadeSection)
		{
			// apply last output
			ILB_HAL_core_setLightEngine(attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startA, attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startB);

			// set state
			if (attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startA != 0 || attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startB != 0)
			{
				attachedLightEngine.state = LIGHT_ENGINE_ON;
			}
			else
			{
				attachedLightEngine.state = LIGHT_ENGINE_OFF;
			}

			attachedLightEngine.currentLevel = attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].stop;
			attachedLightEngine.currentMired = attachedLightEngine.property[PROPERTY_MIRED].stop;

			// send a fade finished notification if we are a driver peripheral
			if (ilb_getState() == ILB_BASE_DRIVER_PERIPHERAL && addressing_getAssignedAddress() != 0)
			{
				sendFadeFinishedNotification();
			}

			return;
		}
	}

	// track linear level
	int32_t deltaLevel = (((int32_t)attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].stop - (int32_t)attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].start) * (int32_t)attachedLightEngine.progress / (int32_t)attachedLightEngine.progressTarget);
	attachedLightEngine.currentLevel = (uint8_t)((int32_t)attachedLightEngine.property[PROPERTY_LINEAR_LEVEL].start + deltaLevel);

	int32_t deltaMired = (((int32_t)attachedLightEngine.property[PROPERTY_MIRED].stop - (int32_t)attachedLightEngine.property[PROPERTY_MIRED].start) * (int32_t)attachedLightEngine.progress / (int32_t)attachedLightEngine.progressTarget);
	attachedLightEngine.currentMired = (uint8_t)((int32_t)attachedLightEngine.property[PROPERTY_MIRED].start + deltaMired);

	switch (attachedLightEngine.intensityLUT)
	{
	case INTENSITY_LOOKUP_DALI:
		attachedLightEngine.currentLevel = ILB_INTENSITY_LOOKUP_DALI[attachedLightEngine.currentLevel];
		break;

	case INTENSITY_LOOKUP_LDV:
		attachedLightEngine.currentLevel = ILB_INTENSITY_LOOKUP_LDV[attachedLightEngine.currentLevel];
		break;

	default:
		break;
	}

	// calculate current level
	uint16_t levelA = (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startA + ((int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection+1].startA - (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startA) * (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progress / (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progressTarget;
	uint16_t levelB = (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startB + ((int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection+1].startB - (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].startB) * (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progress / (int16_t)attachedLightEngine.fadeSections[attachedLightEngine.currentFadeSection].progressTarget;

	// apply output
	ILB_HAL_core_setLightEngine(levelA, levelB);

	// trigger again
	scheduler_registerTimerFunction(&attachedLightEngine_fadeCallback, CORE_ATTACHED_LIGHT_ENGINE_FADE_RESOLUTION_MS);
}

/**
 * Accessor to status of light engine
 *
 * @param status 				of light engine. Bits are described in lightEngineStateTypeDef
 * @param primarySidePower		primary side power consumption of light engine
 * @param secondarySidePower	secondary side power consumption of light engine
 */
void attachedLightEngine_getStatus(uint8_t* status, uint16_t* primarySidePower, uint16_t* secondarySidePower)
{
	// push status byte
	*status = (uint8_t)attachedLightEngine.state;

	//*primarySidePower = (attachedLightEngine.currentLevel * 100 * 20)/0xFF;
	//*secondarySidePower = (attachedLightEngine.currentLevel * 100 * 20)/0xFF;
	// push latest measurements of primary and secondary side power
	*primarySidePower = primarySidePowerInput/POWER_OUTPUT_DIVISOR;
	*secondarySidePower = secondarySidePowerOutput/POWER_OUTPUT_DIVISOR;
}

/**
 * Update the measured output
 *
 * @param latestMeasurement
 */
void lightEngine_updateMeasuredOutput(ADCReadings_t* latestMeasurement)
{
	// reset
	secondarySidePowerOutput = 0;

	// decide which channels to consider
	uint16_t PWMA, PWMB = 0;
	ILB_HAL_core_getLightEngineOutput(&PWMA, &PWMB);
	if (PWMA != 0)
	{
		secondarySidePowerOutput += (latestMeasurement->I1) * (latestMeasurement->U1);
	}
	if (PWMB != 0)
	{
		secondarySidePowerOutput += (latestMeasurement->I2) * (latestMeasurement->U2);
	}
	// secondary side power calculation
	primarySidePowerInput = ((secondarySidePowerOutput * SETTING_PRIM_SEC_CONF_FACTOR) + SETTING_PRIM_SEC_CONV_OFFSET)/SETTING_PRIM_SEC_CONF_DIVISOR;
}

/**
 * Send a notification message that the fade has finished.
 *
 * Applies only if we are in driver peripheral mode
 */
void sendFadeFinishedNotification(void)
{
	ILBMessageStruct msg;
	msg.address = addressing_getAssignedAddress();
	msg.identifier = ILB_GENERATE_IDENTIFIER;
	msg.messageType = ILB_MESSAGE_COMMAND;
	msg.payloadSize = 1;
	msg.payload[0] = ILB_FADE_FINISHED;
	messaging_sendMessage(&msg, NULL, 0, NULL);
}

