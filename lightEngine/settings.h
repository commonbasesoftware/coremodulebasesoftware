/**
 * @file settings.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Handling of the light engine settings stored in memory and the loading of new settings.
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_LIGHTENGINE_SETTINGS_H_
#define ILB_LIGHTENGINE_SETTINGS_H_

#include <stdint.h>

/**
 * Status of loading procedure
 */
typedef enum LightEngineLoadSettingsStatus {
	LIGHT_ENGINE_SETTINGS_LOADING_DONE,                     /**< LIGHT_ENGINE_SETTINGS_LOADING_DONE */
	LIGHT_ENGINE_SETTINGS_LOADING_ABORT,                    /**< LIGHT_ENGINE_SETTINGS_LOADING_ABORT */
	LIGHT_ENGINE_SETTINGS_LOADING_ERROR,                    /**< LIGHT_ENGINE_SETTINGS_LOADING_ERROR */
	LIGHT_ENGINE_SETTINGS_LOADING_GET_VERSION,              /**< LIGHT_ENGINE_SETTINGS_LOADING_GET_VERSION */
	LIGHT_ENGINE_SETTINGS_HEADER,							/**< LIGHT_ENGINE_SETTINGS_LOADING_GET_LIMITS_LOOKUP_MAPPING */
	LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_HEADER,           /**< LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_HEADER */
	LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_LOOKUP            /**< LIGHT_ENGINE_SETTINGS_LOADING_GET_CCT_LOOKUP */
} LightEngineLoadingSettingsStatusTypeDef;

//
//	Routines
//
/**
 * Get the status of the loading routine
 * @return Status
 */
LightEngineLoadingSettingsStatusTypeDef lightEngineSettings_getStatus(void);

/**
 * Trigger the upload of new settings
 */
void lightEngineSettings_startUpload(void);

/**
 * Initialize all necessary data structures associated with the settings. Also pulls settings from memory or places default values in memroy
 * @return 0x00 on success, non 0x00 on error
 */
uint8_t lightEngineSettings_init(void);

/**
 * Get the follower group byte stored in memory. Each bit is equivalent to a group.
 * @return Follower group flags.
 */
uint8_t lightEngineSettings_getFollowerGroup(void);

/**
 * Get the PWM levels for a set of level and mired information. Uses the channel mapping and lookup information provided by the stored settings.
 * @param level	Level for which the PWM levels should be calculated
 * @param mired	Mired value for which the PWM levels should be calculated
 * @param CHA	Found PWM level (0 to 1000) for channel A
 * @param CHB	Found PWM level (0 to 1000) for channel B
 */
void lightEngineSettings_getChannelOutput(uint8_t level, uint16_t mired, uint16_t* CHA, uint16_t* CHB);

#endif /* ILB_LIGHTENGINE_SETTINGS_H_ */
