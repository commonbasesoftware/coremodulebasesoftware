/**
 * @file ilb.h
 *
 * @author A.Niggebaum
 * @date 15.09.2021
 *
 * @brief General definitions and structures required across all programs using ILB infrastructure via the common base software
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_ILB_H_
#define ILB_ILB_H_

#include <stdint.h>
#include "settings/settings.h"

#define ILB_GENERATE_IDENTIFIER			0xFF									//!< Flag to generate an identifier if passed as an identifier argument to send functions
#define ILB_MESSAGE_IDENTIFIER_MIN      0x01                                    //!< Minimal possible value for identifier
#define ILB_MESSAGE_IDENTIFIER_MAX      0x14                                    //!< Maximal possible value for identifier

#define ILB_PERIPHERAL_INFO_BASE_ADDRESS			0x00						//!< base address for static ILB information register
#define ILB_PERIPHERAL_STATUS_BASE_ADDRESS			0x01						//!< base address for peripheral status in volatile register
#define ILB_TRACKER_SUB_ADDRESS						0x00						//!< address of tracker variable
#define ILB_UID_BASE_ADDRESS						0x00						//!< address for UID
#define ILB_SW_REVISION_BASE_ADDRESS				0x04						//!< base address of software revision register

/**
 * Return type for functions in the ILB related hardware and other abstraction layers
 */
typedef enum ILB_HAL_StatusEnum {
	ILB_HAL_OK			= 0x00U,    /**< Execution success */
	ILB_HAL_ERROR		= 0x01U,  	/**< General error occured. No success */
	ILB_HAL_BUSY		= 0x02U,   	/**< Could not complete or execute because busy. */
	ILB_HAL_TIMEOUT		= 0x03U,	/**< Could not complete or execute becasue timeout. */
	ILB_HAL_INVALID		= 0x04U,	/**< Parameters invalid. */
} ILB_HAL_StatusTypeDef;

//
//	Intensity lookup definitions
//
/**
 * Intensity lookup defines
 */
typedef enum LightEngineIntensityLookup
{
	INTENSITY_LOOKUP_LINEAR 	= 0x00,	  /**< INTENSITY_LOOKUP_LINEAR */
	INTENSITY_LOOKUP_DALI		= 0x01,   /**< INTENSITY_LOOKUP_DALI */
	INTENSITY_LOOKUP_LDV		= 0x02    /**< INTENSITY_LOOKUP_LDV */
} IntensityLookup_t;

/**
 * Acknowledge type messages
 */
typedef enum MessageErrorCodes {
  ILB_OK                        = 0x00,                                         //!< Everything OK
  ILB_ERROR                     = 0x01,                                         //!< Unspecified error
  ILB_ERROR_INVALID             = 0x02,                                         //!< Invalid command (syntax or checksum error)
  ILB_ERROR_NOT_ALLOWED         = 0x03,                                         //!< command not allowed
  ILB_ERROR_NOT_KNOWN           = 0x04,                                         //!< command not known
  ILB_ERROR_DEVICE_BUSY         = 0x05,                                         //!< device currently busy, e.b buffers full
  ILB_OK_INDICATE_POSITIVE		= 0x06,											//!< the message was received and the PERIPHERAL should indicate once positively
  ILB_OK_INDICATE_NEGATIVE		= 0x07,											//!< the message was received and the PERIPHERAL should indicate once negatively
  ILB_ERROR_OUT_OF_BOUNDS		= 0x08											//!< indicates that the command/request cannot be executed because the provided parameters are out of bounds. This could be a read request to a non-existing register or a set request to a non-allowed value
} ILBErrorTypeDef;

/**
 * Type of an ILB message
 */
typedef enum ilbBusMessageTypeEnum{
  ILB_MESSAGE_ACHKNOWLEDGE = 0x80,                                              //!< Marks the mesasge as an an acknowledge message
  ILB_MESSAGE_READ         = 0x00,                                              //!< Marks the message as a read message type
  ILB_MESSAGE_WRITE        = 0x40,                                              //!< Marks the message as write message type
  ILB_MESSAGE_COMMAND      = 0xC0,                                              //!< Marks the message as a command
  ILB_MESSAGE_HANDLER_ONLY = 0xAA,												//!< Marks the message as a placeholder. The sending function does not actually send the message but re-registers the identifier and the callbacks
} ILBBusMessageTypeTypeDef;

/**
 * Commands send to devices
 */
typedef enum ilbCommandEnum {
  ILB_REPORT_UID                	= 0x01,                                     //!< the peripheral is instructed to report its UID to the core
  ILB_CHANGE_ADDRESS            	= 0x02,                                     //!< The peripheral is instructed to change its address. It is identified by UID not address
  ILB_RESET_DEVICE	 				= 0x03,										//!< the device is instructed to perform a power cycle
  ILB_FACTORY_RESET_DEVICE			= 0x04,										//!< the device is instructed to perform a factory reset. All non-volatile variables are reset to their default value
  ILB_GET_KEY_CHALLENGE				= 0x05,										//!< request a key challenge from the recipient
  ILB_MUTE_DEVICE					= 0x06,										//!< instructs the receiver to enter/exit mute mode
  ILB_START_FW_UPGRADE				= 0x07,										//!< instructs the receiver to enter FW upgrade mode
  ILB_SET_DEVICE_INDICATOR			= 0x08,										//!< instructs the receiver to set its indicator LEDs until a different pattern is required.
  ILB_BROADCAST_INFO				= 0x09,										//!< tags this message to contain the periodically broadcasted system status. Send by the core
  ILB_REPORT_MOTION_DAYLIGHT_EVENT	= 0x0A,										//!< A peripheral reports a motion or daylight event to the core
  ILB_REPORT_BUTTON_EVENT			= 0x0B,										//!< A peripheral reports a button event to the core
  ILB_SET_OUTPUT_LEVEL				= 0x0C,										//!< A peripheral wants the system to output a certain light level on one or more channels
  ILB_SET_OUTPUT_CCT				= 0x0D,										//!< A peripheral wants the system to output a certain light level and CCT value on one or more channels
  ILB_SET_INDICATE					= 0x0E,										//!< A peripheral wants the system to visible indicate or stop indicating
  ILB_FADE_FINISHED					= 0x0F,										//!< Indication that the fade of a peripheral has finsihed.
  ILB_START_OPERATION				= 0x10,										//!< instructs the peripheral to commence normal operation after the factory configuration has been written
  ILB_REPORT_DAYLIGHT_LEVEL			= 0x11,										//!< reports a daylight level (of the complete system) to a device. E.g. communiation module
  ILB_SETTINGS_RESET				= 0x12,										//!< instructs the receiver to reset its settings to the factory default.
  ILB_FOLLOWER_INSTRUCTION			= 0x13,										//!< instruction set for followers attached to the ILB
  ILB_FOLLOWER_COMMAND				= 0x14,										//!< command for follower. Commands can be used for temporary address assignment
  ILB_CONFIGURE_PWM_SETTINGS		= 0x15,										//!< starts the configuration routine to receive settings for the pwm output
  ILB_ERASE_LOGIC					= 0x16,										//!< instructs the core to erase the logic
  ILB_UPLOAD_LOGIC					= 0x17,										//!< instructs the core to start the process to load a new logic
  ILB_SET_LEVEL_ADVANCED			= 0x18,										//!< advanced level set command. Allows control over fade time factor and dimming curve
  ILB_SET_CCT_ADVANCED				= 0x19,										//!< advanced cct and level set command. Allows control over fade time factor and dimming curve
  ILB_TEST_INFORMATION				= 0x1A,										//!< Trigger an internal device test and return results (device specififc)
  ILB_CMD_MAX						= 0x1B,										//!< maximal index for commands. Used for range checks
} ILBCommandTypeDef;

/**
 * Enumeration of peripheral classes
 */
typedef enum ILBDeviceClassEnum {
	ILB_CORE_CLASS =								0x01,		//!< Motion and daylight sensor class identifier
	ILB_PERIPHERAL_MOTION_DAYLIGHT_CLASS =			0x02,		//!< Motion and daylight sensor class identifier
	ILB_PERIPHERAL_ZIGBEE_CLASS	=					0x07,		//!< ZigBee (only) peripheral class identifier
	ILB_PERIPHERAL_BLUETOOTH_CLASS =				0x08,		//!< Bluetooth peripheral class identifier
	ILB_PERIPHERAL_DALI_CLASS =						0x09,		//!< DALI peripheral class identifier
	ILB_PERIPHERAL_DRIVER_CLASS =					0x0A,		//!< Driver peripheral class identifier
	ILB_FOLLOWER_CLASS = 							0xF0,		//!< (Driver) follower device type. This type cannot be part of a network. Equivalent to base software only
	ILB_CLASS_MAX = 								0xF1,		//!< max for range check of class
} ILBDeviceClass_t;

/**
 * Structure representing an ILB message
 */
typedef struct ilbMessageStruct {
	uint8_t address;                                                              //!< source address for incomming message (and if known), target for outgoing
	ILBBusMessageTypeTypeDef messageType;                                         //!< message type
	uint8_t identifier;                                                           //!< messagse identifier. If set to ILB_GENERATE_IDENTIFIER, the processing message will take the next free identifier
	uint8_t payloadSize;                                                          //!< size of the payload in bytes.
	uint8_t payload[ILB_MAX_MESSAGE_LENGTH];                                      //!< payload of message. Size will be initialised to ILB_RX_BUFFER_DEPTH (settings)
} ILBMessageStruct;

/**
 * Motion and daylight specific reporting flags
 */
typedef enum {
	ILB_REPORT_MOTION_DAYLIGHT_MOTION_FLAG						= 0x01,			//!< report motion to the core
	ILB_REPORT_MOTION_DAYLIGHT_DAYLIGHT_ABOVE_THRESHOLD_FLAG	= 0x02,			//!< report daylight above threshold to core
	ILB_REPORT_MOTION_DAYLIGHT_DAYLIGHT_BELOW_THRESHOLD_FLAG	= 0x04			//!< report daylight below threshold to core
} ILBMotionDaylihgReport_t;

/**
 * Typedef that defines the flags for the indicators
 */
typedef enum deviceIndicatorEnum{
	DEVICE_GREEN_INDICATOR			= 0x01,										//!< Flag that defines the green indicator or LED1
	DEVICE_RED_INDICATOR			= 0x02,										//!< Flag that defines the red indicator or LED2
	DEVICE_LOW_PRIO_GREEN_INDICATOR = 0x04,										//!< Flag that defines a low priority indication on the green indicator or LED1
	DEVICE_LOW_PRIO_RED_INDICATOR	= 0x08,										//!< flag that defines a low priority indication on the red indicator or LED2
	DEVICE_GREEN_ONESHOT			= 0x40,										//!< flag that marks the green or LED1 pattern as a "oneshot", meaning it will be cleared after completion
	DEVICE_RED_ONESHOT				= 0x80										//!< flag that marks the red or LED2 pattern as a "oneshot", meaning it will be cleared after completion
} deviceIndicatorTypeDef;

/**
 * Sub commands when loading light engine settings
 */
enum LightEngineLoadingCommands
{
	LIGHT_ENGINE_LOADING_CMD_GET_VERSION					= 0x00,         /**< LIGHT_ENGINE_LOADING_CMD_GET_VERSION */
	LIGHT_ENGINE_LOADING_CMD_GET_HEADER						= 0x01,			/**< LIGHT_ENGINE_LOADING_CMD_GET_LIMITS_LOOKUP_MAPPING */
	LIGHT_ENGINE_LOADING_CMD_GET_CCT_HEADER					= 0x02,        	/**< LIGHT_ENGINE_LOADING_CMD_GET_CCT_HEADER */
	LIGHT_ENGINE_LOADING_CMD_GET_CCT_TABLE					= 0x03          /**< LIGHT_ENGINE_LOADING_CMD_GET_CCT_TABLE */
};

/**
 * Channel mapping definitions
 */
typedef enum groupChannelRouting {
	CHANNEL_MAPPING_DEF_1_TO_A			= 0x01,				//!< mapping of CCT output 1 to channel A
	CHANNEL_MAPPING_DEF_1_TO_B			= 0x02,				//!< mapping of CCT output 1 to channel B
	CHANNEL_MAPPING_DEF_2_TO_A			= 0x10,				//!< mapping of CCT output 2 to channel A
	CHANNEL_MAPPING_DEF_2_TO_B			= 0x20				//!< mapping of CCT output 2 to channel B
} groupChannelRoutingTypeDef;

/**
 * List of follower groups. Entries are flags.
 */
enum followerGroupEnum {
	FOLLOWER_GROUP_NONE			= 0x00,	  /**< Member of no group*/
	FOLLOWER_GROUP_1			= 0x01,   /**< Member of group 1 */
	FOLLOWER_GROUP_2			= 0x02,   /**< Member of group 2 */
	FOLLOWER_GROUP_3			= 0x04,   /**< Member of group 3 */
	FOLLOWER_GROUP_4			= 0x08,   /**< Member of group 4 */
	FOLLOWER_GROUP_5			= 0x10,   /**< Member of group 5 */
	FOLLOWER_GROUP_6			= 0x20,   /**< Member of group 6 */
	FOLLOWER_GROUP_7			= 0x40,   /**< Member of group 7 */
	FOLLOWER_GROUP_8			= 0x80    /**< Member of group 8 */
};

/**
 * Structure describing light engine settings
 */
typedef struct LightEngineSettings {
	uint8_t version;										//!< the version of the settings
	uint8_t Limit_CHA;										//!< limit for channel A
	uint8_t Limit_CHB;										//!< limit for channel B
	uint8_t channelMapping;									//!< channel mapping definition
	uint8_t followerGroup;									//!< follower group association
	uint16_t miredMin;										//!< minimal mired value in lookup table
	uint16_t miredMax;										//!< maximal mired value in lookup table
	uint8_t miredStep;										//!< step size in mired lookup table
	uint8_t* miredLookup;									//!< pointer to first element in mired lookup table
} LightEngineSettings_t;

/**
 * Access description for eeprom reading and writing
 */
typedef enum eepromInterfaceDef {
	EEPROM_VAR_BYTE 	 = 0x01,   	/**< EEPROM_VAR_BYTE */
	EEPROM_VAR_HALF_WORD = 0x02,	/**< EEPROM_VAR_HALF_WORD */
	EEPROM_VAR_WORD		 = 0x04,   	/**< EEPROM_VAR_BYTE */
	EEPROM_VAR_WRITE	 = 0x80    	/**< EEPROM_VAR_WRITE */
} EEPROMAccess_t;

/**
 * Bit flag for broadcast information byte.
 */
typedef enum broadcastStatusByteEnum {
	BROADCAST_FLAG_SYSTEM_FAILURE 	= 0x01, 	/**< Indicates that the overall system has encountered an error */
	BROADCAST_FLAG_LAMP_FAILURE		= 0x02,   	/**< Indicates that the lamp has a failure.  Could be short cirucit */
	BROADCAST_FLAG_LAMP_ON			= 0x04,     /**< Indicates that the lamp is on */
	BROADCAST_FLAG_LAMP_ERROR		= 0x08,     /**< Indicates that the lamp has an error. Could be open circuit (no lamp) */
	BROADCAST_FLAG_FADE_RUNNING		= 0x10,   	/**< Indicates that a fade is running and the power output levels are subject to change */
	BROADCAST_FLAG_INDICATE_RUNNING = 0x20,		/**< Indicates that an indication pattern is running */
	BRADCAST_FLAG_TEMPERATURE_ERROR = 0x40 		/**< Indicates that the driver is operating outside the specified temperature limits */
} BroadcastStatusFlag_t;

/**
 * Status of light engine
 */
typedef enum lightEngineState {
	LIGHT_ENGINE_OFF 	= 0x00,								//!< no flags. Light engine off
	LIGHT_ENGINE_ON		= BROADCAST_FLAG_LAMP_ON,			//!< same as light on flag in register => easier to handle, no if required
	LIGHT_ENGINE_FADING = BROADCAST_FLAG_FADE_RUNNING, 		//!< same as light engine fading flag in register  => no handle required
	LIGHT_ENGINE_ERROR 	= BROADCAST_FLAG_LAMP_ERROR,		/**< LIGHT_ENGINE_ERROR */
} lightEngineStateTypeDef;

/**
 * ADC readings to monitor supply
 */
typedef struct ADCReadingStruct {
	uint32_t U1;								//!< Voltage on channel 1
	uint32_t I1;								//!< Current on channel 1
	uint32_t I2;								//!< Current on channel 2
	uint32_t U2;								//!< Voltage on channel 2
	uint32_t T;									//!< temperature measured
} ADCReadings_t;

/**
 * Telemetry interface. Contains the list of parameters that can be accessed.
 */
typedef enum TelemetryValueEnum {
	TELEMETRY_DRIVER_TEMPERATURE				//!< measured temperature of the driver
} TelemetryValue_t;

//
//	Logic interface structures.
//

/**
 * A generic callback handle
 */
typedef void (*callbackHandle_t)(void);

/**
 * A generic message handler that takes a message as an argument. Normally the reponse to an outgoing message
 * @param msg	Received message
 */
typedef void (*messageHandler_t)(ILBMessageStruct* msg);

/**
 * Initialisation function for timers with 1s resolution needed by the logic
 * @param nOfSlowTimers	Number of slow timers needed.
 * @return				::ILB_HAL_OK on success
 */
typedef ILB_HAL_StatusTypeDef (*initFunc_t)(uint8_t* nOfSlowTimers);

/**
 * Initialisation function for timers with 1s resolution needed by the logic
 * @param nOfSlowTimer	Number of slow timers needed
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*initFuncTimer_t)(uint8_t nOfSlowTimer);

/**
 * Function that can send a message into the ILB bus
 * @param msg			Pointer to message structure to send
 * @param callback		Pointer to callback to triggered if a response is expected
 * @param timeout		Timeout in ms before the timeout handle is called
 * @param timeoutHandle Timeout handle
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*sendMessage_t)(ILBMessageStruct* msg, messageHandler_t callback, uint16_t timeout, callbackHandle_t timeoutHandle);

/**
 * Function that sends a response containing only of an error code with respect to a demanding message. The response
 * will match the identifier of the requesting message.
 * @param msg		Pointer to message structure to be sent
 * @param code		Response or error code to send
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*sendResponse_t)(ILBMessageStruct* msg, ILBErrorTypeDef code);

/**
 * Function that schedules a task with on a 5ms resolution base
 * @param handle	Handle to function to be executed when the timer expires
 * @param timeout	Timeout in ms
 * @return
 */
typedef ILB_HAL_StatusTypeDef (*scheduleTask_t)(callbackHandle_t handle, uint16_t timeout);

/**
 * Function that schedules a task with a 1ms resolution base. The number of slots is limited and must be requested
 * by the logic during initilisation. (see ::initFucnTimer_t and ::initFunc_t). Timers are accessed via indices that
 * are managed by the logic.
 * @param index		Index of timer to start or reset
 * @param handle	Handle to execute once timer expires
 * @param timeout	Timeout in seconds of the timer
 * @return
 */
typedef ILB_HAL_StatusTypeDef (*scheduleSlowTask_t)(uint8_t index, callbackHandle_t handle, uint16_t timeout);

/**
 * Function returning the tick counter of the microcontroller.
 * @return			Number of ms (ticks) since microcontroller was powered up
 */
typedef uint32_t 			  (*getTick_t)(void);

/**
 * Delay execution of the program for a set number of ms
 * @param delay		Number of ms to delay
 */
typedef void 				  (*delay_t)(uint32_t delay);

/**
 * Function to get a random or pseudo random number
 * @param nr	Pointer to location to place random number
 * @return		::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*getRandomNumber_t)(uint32_t* nr);

/**
 * Set the indicator on the hardware
 * @param def		Color and duration definition. May specify color affected and duration (single shot for 1s)
 * @param pattern1	Pattern for green LED
 * @param pattern2	Pattern for red LED
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*setIndicator_t)(deviceIndicatorTypeDef def, uint8_t pattern1, uint8_t pattern2);

/**
 * Function providing access to light engine status
 * @param status				Status byte of light engine. May indicate error, fading or on state
 * @param primarySidePower		Primary side power consumption in watts
 * @param secondarySidePower	Secondary side power output in watts
 */
typedef void 				  (*getLightEngineStatus_t)(uint8_t* status, uint16_t* primarySidePower, uint16_t* secondarySidePower);

/**
 * Function starting a fade on the light engine.
 * @param targetLevel	Target level to fade to relative to current output (0x00 to 0xFF)
 * @param targetMired	Target mired to fade to relative to current output
 * @param fadeTimeMS	Time the fade should take in ms
 * @param intensityLUT	Intensity lookup to follow. May be linear, DALI or Ledvance internal
 * @return
 */
typedef ILB_HAL_StatusTypeDef (*startLightEngineFade_t)(uint8_t targetLevel, uint16_t targetMired, uint32_t fadeTimeMS, IntensityLookup_t intensityLUT);

/**
 * Interface function to EEPROM memory.
 * @param accessor		Accessor specifier
 * @param offsetBytes	Offset from begining of memory
 * @param pVar			Pointer to memory to store or retrieve
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*eepromInterface_t)(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar);

/**
 * Accessor function for logic to retrieve measured telemetry values
 * @param target	location where to store the measured value
 * @param value		Which measurement to return
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*telemetryInterface_t)(uint8_t* target, TelemetryValue_t value);

/**
 * Logic routine that should be executed regularly
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
typedef ILB_HAL_StatusTypeDef (*logicRoutine_t)(void);

/**
 * Interface structure that allows the logic to interface functions of the base software
 */
typedef struct BaseSoftwareInterfaceStructure {
	uint16_t 				version;				//!< version of the interface
	initFuncTimer_t 		init;					//!< initialisation function
	sendMessage_t 			sendMessage;			//!< send an ILB message
	sendResponse_t 			sendResponseCode;		//!< send a response code to a message
	scheduleTask_t			scheduleSystemTask;		//!< schedule a task with 5ms resolution
	scheduleSlowTask_t		scheduleSlowTask;		//!< schedule a task with 1s resolution
	getTick_t				getTick;				//!< get the number of ticks (ms) since system start
	delay_t					delay;					//!< delay execution for a set of ms
	getRandomNumber_t		getRandomNumber;		//!< pointer to interface for random number generation
	setIndicator_t			setIndicator;			//!< pointer to indicator accessor
	getLightEngineStatus_t	getLightEngineStatus;	//!< get the status of the light engine
	startLightEngineFade_t	startLightEngineFade;	//!< start a light engine fade
	eepromInterface_t		eepromInterface;		//!< interface to eeprom memory
	telemetryInterface_t	telemetryInterface;		//!< interface to access telemetry values
} BaseSoftwareInterface_t;

/**
 * Interface structure to allow the base software control and run the ogic
 */
typedef struct CoreLogicInterfaceStructure {
	uint16_t			version;				//!< version of interface. The version also encodes the hardware
	uint8_t 			GTIN[6];				//!< gtin of product
	uint16_t			versionMajor;			//!< major version of logic
	uint16_t 			versionMinor;			//!< minor version of logic
	initFunc_t			init;					//!< pointer to initialisation function
	logicRoutine_t		logic;					//!< pointer to main logic function
	messageHandler_t	commandHandler;			//!< pointer to command handling function
	messageHandler_t	readHandler;			//!< pointer to read handling function
	messageHandler_t	writeHandler;			//!< pointer to write handling function
} CoreLogicInterface_t;

/**
 * Flags of the (light engine related) system
 *
 * The bit positions are chosen to be the same as in the broadcast mesage
 *
 * This enum defines the flags within the ILB_BROADCAST_BYTE_SYSTEM_STATUS byte
 */
typedef enum iblSystemReportFlagEnum {
	ILB_SYSTEM_REPORT_SYSTEM_FAILURE_FLAG	= 0x01,								//!< indicates that the system is in a general failure state
	ILB_SYSTEM_REPORT_LAMP_FAILURE_FLAG		= 0x02,								//!< indicate that the lamp has failed
	ILB_SYSTEM_REPORT_LAMP_ON_FLAG			= 0x04,								//!< indicate that the lamp is on
	ILB_SYSTEM_REPORT_LAMP_ERROR_FLAG		= 0x08,								//!< indicate that the lamp has encountered an error
	ILB_SYSTEM_REPORT_FADE_RUNNING_FLAG		= 0x10,								//!< indicate that a fade is running
	ILB_SYSTEM_REPORT_INDICATE_RUNNING_FLAG = 0x20,								//!< indicate that the indication is running
	ILB_SYSTEM_REPORT_OVERTEMPERATURE		= 0x40								//!< indicate that the system is operating outside specified temperature bounds
} SystemErrorFlag_t;

//
//	Main routine
//
/**
 * Main logic routine called by the hardware project
 */
void ilbLogic(void);

//
//	Access functions for hardware layer
//
/**
 * Handler function called by hardware layer if a message is reeived
 * @param pData		Pointer to first element of message bytes
 * @param length	Number of bytes in message
 * @return			::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef lib_ilbMessageReceived(uint8_t* pData, uint8_t length);

/**
 * Callback that a mesasge has been sent out successuflly
 * @param s	::ILB_HAL_OK on success, error code otherwise
 */
void lib_messageSendSuccess(ILB_HAL_StatusTypeDef s);

/**
 * Called by hardware whenever the scheduler may execute. The scheduler runs the normal working tasks of the program. This
 * are handling received or sending waiting messages
 */
void lib_setSchedulerStatusWaiting(void);

/**
 * Should be called by the hardware layer every second and indicates that the slow (1s) timer can be executed.
 */
void lib_arm1sTimer(void);

#endif /* ILB_ILB_H_ */
