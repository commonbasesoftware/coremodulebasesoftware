/**
 * @file messaging.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Messaging related functionality. Sending and handling
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_MESSAGING_MESSAGING_H_
#define ILB_MESSAGING_MESSAGING_H_

#include <stdint.h>
#include "../hardwareAPI/hardwareAPI.h"
#include "../ilb.h"

/**
 * Initializes all necessary memory regions
 */
void messaging_init(void);

/**
 * Handle waiting messages. Calls the associated handlers if there are any
 */
void messaging_handleWaitingMessages(void);

/**
 * Send a message via the hardware interface. This function directly accesses the hardware interface
 *
 * @param m					Pointer to message to send
 * @param callback			Callback for a response to this message. NULL if no response is expected or the response should not be handled
 * @param timeoutMS			Timeout before the timeout handler is called
 * @param timeoutCallback	Handler to call if no answer is received in the specified time period
 * @return					::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef messaging_sendMessage(ILBMessageStruct *m, void (*callback)(ILBMessageStruct* m), uint16_t timeoutMS, void (*timeoutCallback)(void));

/**
 * Send an answer to a message. Generates and sends an acknowledge type message. This version should be used for a payload that is not an error code
 * @param m				Original message. The identifier is used
 * @param payload		Payload to bundle in the message
 * @param payloadSize	Size of the payload
 * @return				::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef messaging_sendAnswer(ILBMessageStruct* m, uint8_t* payload, uint8_t payloadSize);

/**
 * Respond to a message with an error code (1 byte)
 * @param m		Message to send a response to. The identifier is used
 * @param error	Error code to bundle in message
 * @return		::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef messaging_respondErrorMessage(ILBMessageStruct* m, ILBErrorTypeDef error);

/**
 * Send messages waiting in the sending queue
 */
void messaging_sendWaitingMessages(void);

//
//	Adressing related functions. Defined in addressing.c
//
/**
 * Initialize the addressing mechanism. The filter to which messages should be handled is set depending on the class. Also recovers any stored address.
 * @param deviceClass		Class of this device
 * @param commandHandler	Handler for command messages received
 * @param readHandler		Handler for read messages received
 * @param writeHandler		Handler for write messages received
 * @return					::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef addressing_init(uint8_t deviceClass, void(*commandHandler)(ILBMessageStruct* m), void(*readHandler)(ILBMessageStruct* m), void(*writeHandler)(ILBMessageStruct* m));

/**
 * Main handling routine for any incoming messages. The message is passed through a filter, that selects, depending on the device type, which messages should be handled.
 * @param message	Message received
 */
void addressing_handleMessage(ILBMessageStruct* message);

/**
 * Accessor function for device class
 * @return	Device class
 */
uint8_t addressing_getDeviceClass(void);

/**
 * Accessor for address. Effectively a wrapper for the hardware interface
 * @return	0x00 if no address is found or recovered, the address otherwise.
 */
uint8_t addressing_getAssignedAddress(void);

#endif /* ILB_MESSAGING_MESSAGING_H_ */
