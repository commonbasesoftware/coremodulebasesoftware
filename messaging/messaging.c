/*
 * messaging.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "../ilb.h"
#include "../register/register.h"
#include "../scheduler/scheduler.h"
#include "../settings/settings.h"
#include "../messaging/messaging.h"
#include "../hardwareAPI/hardwareAPI.h"

//
//	Private variables
//
static ILBMessageStruct* ILBMessageRXBuffer;               //!< Buffer for incoming messages
static ILBMessageStruct* ILBMessageTXBuffer;               //!< Buffer for outgoing messages

static volatile uint8_t nextRXMessageIndex = 0;                  //!< Index of next received message to process
static volatile uint8_t activeRXMessageIndex = 0;                //!< Index of currently active received message e.g. message currently received
static volatile uint8_t activeTXMessageIndex = 0;                //!< index of currently sent message
static volatile uint8_t TXMessagesWaiting = 0;

static uint8_t txBuffer[ILB_MAX_MESSAGE_LENGTH] = {0};			//!< buffer for the outgoing message

static uint8_t deviceMessageIdentifierCounter = ILB_MESSAGE_IDENTIFIER_MIN;

typedef enum ilbBusStatusEnum{
  ILB_BUS_RX_IDLE,                                                              //!< ILB Bus is idle in RX direction
  ILB_BUS_RX_RECEIVING,                                                         //!< ILB bus is receiving bytes
  ILB_BUS_RX_PROCESSING,														//!< ILB is processing a message
  ILB_BUS_TX_IDLE,                                                              //!< ILB bus is idle in TX direction
  ILB_BUS_TX_FINISHED,															//!< ILB bus has send a message and needs to respect the timeout. This is one iteration of the scheduler timer
  ILB_BUS_TX_SENDING,                                                           //!< IBL bus is sending bytes
  ILB_BUS_TX_ERROR																//!< indicates that an error occured while sending
} ILBBusStatusTypeDef;
static volatile ILBBusStatusTypeDef lineStatus = ILB_BUS_TX_IDLE;

typedef enum ilbMessageHandlerStatusEnum {
  ILB_MESSAGE_HANDLER_IDLE,                                                     //!< indicates that the message handler is idle
  ILB_MESSAGE_HANDLER_BUSY                                                      //!< indicates that the message handler is busy and no messages can be processed at the moment
} ILBMessageHandlerStatusTypeDef;
static ILBMessageHandlerStatusTypeDef messageHandlerStatus = ILB_MESSAGE_HANDLER_IDLE;

//
//	Function prototypes
//
static uint8_t encodeILBMessage(uint8_t* buffer, ILBMessageStruct* message);
static uint8_t placeByteInBuffer(uint8_t* buffer, uint8_t byte);
static uint8_t messaging_queueTXMessage(uint8_t targetAddress, ILBBusMessageTypeTypeDef type, uint8_t identifier, uint8_t* payload, uint8_t payloadSize);

/**
 * Initialisation routine for all relevant structure
 */
void messaging_init(void)
{
	// reserve space for the incoming and outgoing message buffer
	ILBMessageRXBuffer = malloc(ILB_MESSAGE_RX_BUFFER_DEPTH * (sizeof(*ILBMessageRXBuffer)));
	ILBMessageTXBuffer = malloc(ILB_MESSAGE_TX_BUFFER_DEPTH * (sizeof(*ILBMessageTXBuffer)));

	// initialise the register
	register_init();
}


ILB_HAL_StatusTypeDef messaging_sendMessage(ILBMessageStruct *m, void (*callback)(ILBMessageStruct* m), uint16_t timeoutMS, void (*timeoutCallback)(void))
{
	// check if we only need to re-register the handler
	if (m->messageType == ILB_MESSAGE_HANDLER_ONLY)
	{
		scheduler_registerHandlerFunction(m->identifier, callback, timeoutCallback, timeoutMS);
		return ILB_HAL_OK;
	}
	if (callback == NULL)
	{
		messaging_queueTXMessage(m->address, m->messageType, m->identifier, m->payload, m->payloadSize);
		return ILB_HAL_OK;
	}
	uint8_t identifier = messaging_queueTXMessage(m->address, m->messageType, ILB_GENERATE_IDENTIFIER, m->payload, m->payloadSize);
	scheduler_registerHandlerFunction(identifier, callback, timeoutCallback, timeoutMS);
	return ILB_HAL_OK;
}


ILB_HAL_StatusTypeDef messaging_sendAnswer(ILBMessageStruct* m, uint8_t* payload, uint8_t payloadSize)
{
	messaging_queueTXMessage(m->address, ILB_MESSAGE_ACHKNOWLEDGE, m->identifier, payload, payloadSize);
	return ILB_HAL_OK;
}


ILB_HAL_StatusTypeDef messaging_respondErrorMessage(ILBMessageStruct* m, ILBErrorTypeDef error)
{
	if (TXMessagesWaiting >= ILB_MESSAGE_TX_BUFFER_DEPTH)
	{
		return ILB_HAL_ERROR;
	}

	uint8_t targetIndex = activeTXMessageIndex + TXMessagesWaiting;
	if (targetIndex >= ILB_MESSAGE_TX_BUFFER_DEPTH)
	{
		targetIndex -= ILB_MESSAGE_TX_BUFFER_DEPTH;
	}
	ILBMessageTXBuffer[targetIndex].address = m->address;
	ILBMessageTXBuffer[targetIndex].identifier = m->identifier;
	ILBMessageTXBuffer[targetIndex].messageType = ILB_MESSAGE_ACHKNOWLEDGE;
	ILBMessageTXBuffer[targetIndex].payloadSize = 1;
	ILBMessageTXBuffer[targetIndex].payload[0] = error;
	TXMessagesWaiting++;
	return ILB_HAL_OK;
}


ILB_HAL_StatusTypeDef lib_ilbMessageReceived(uint8_t* pData, uint8_t length)
{
	// check length
	if (length > ILB_MAX_MESSAGE_LENGTH)
	{
		return ILB_HAL_ERROR;
	}

	// handle index in message queue
	activeRXMessageIndex++;
	if (activeRXMessageIndex >= ILB_MESSAGE_RX_BUFFER_DEPTH) {
		activeRXMessageIndex = 0;
	}
	// check collision with active read pointer. This means the buffer is full
	if (activeRXMessageIndex == nextRXMessageIndex) {
		// reset the counter
		activeRXMessageIndex--;
		if (activeRXMessageIndex >= ILB_MESSAGE_RX_BUFFER_DEPTH) {
			activeRXMessageIndex = ILB_MESSAGE_RX_BUFFER_DEPTH-1;
		}
		// send notification that we are busy
		uint8_t payload = ILB_ERROR_DEVICE_BUSY;
		messaging_queueTXMessage(pData[0], ILB_MESSAGE_ACHKNOWLEDGE, (pData[ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION] & ILB_MESSAGE_IDENTIFIER_MASK), &payload, 1);
	} else {
		// store in message queue
		ILBMessageRXBuffer[activeRXMessageIndex].address = (pData[ILB_MESSAGE_ADDRESS_BYTE_POSITION]);
		ILBMessageRXBuffer[activeRXMessageIndex].identifier = (pData[ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION] & ILB_MESSAGE_IDENTIFIER_MASK);
		ILBMessageRXBuffer[activeRXMessageIndex].messageType = (pData[ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION] & ILB_MESSAGE_TYPE_MASK);
		ILBMessageRXBuffer[activeRXMessageIndex].payloadSize = length - (ILB_MESSAGE_NOF_EXTRA_BYTES - 1);		// extra bytes minus checksum byte
		// copy payload
		memcpy(&ILBMessageRXBuffer[activeRXMessageIndex].payload[0], &pData[ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION + 1], length-2);
	}
	return ILB_HAL_OK;
}


uint8_t encodeILBMessage(uint8_t* buffer, ILBMessageStruct* message)
{
	uint8_t bufferIndex = 0;

	// assemble message in buffer
	// place start byte
	*buffer = ILB_MESSAGE_START_BYTE;
	bufferIndex++;
	// write address
	bufferIndex += placeByteInBuffer(buffer + bufferIndex, message->address);
	// write length byte
	bufferIndex += placeByteInBuffer(buffer + bufferIndex, message->payloadSize + 2);
	// write identifier and message type
	// this is also the first byte to be included in the checksum
	uint8_t checksum = (message->messageType | message->identifier);
	bufferIndex += placeByteInBuffer(buffer + bufferIndex, checksum);
	// write payload, compute checksum while at it
	for (uint8_t i=0; i<message->payloadSize; i++)
	{
		checksum += message->payload[i];
		bufferIndex += placeByteInBuffer(buffer + bufferIndex, message->payload[i]);
		if (bufferIndex >= ILB_MAX_MESSAGE_LENGTH)
		{
			return bufferIndex + 1;
		}
	}
	// place checksum
	bufferIndex += placeByteInBuffer(buffer + bufferIndex, checksum);

	return bufferIndex;
}


uint8_t placeByteInBuffer(uint8_t* buffer, uint8_t byte)
{
	if (byte == ILB_MESSAGE_START_BYTE || byte == ILB_MESSAGE_ESCAPE_BYTE)
	{
		*buffer = ILB_MESSAGE_ESCAPE_BYTE;
		*(buffer + 1) = ILB_MESSAGE_ESCAPE_OPERATION(byte);
		return 2;
	}
	*buffer = byte;
	return 1;
}

void messaging_sendWaitingMessages(void)
{
	//
	//	Handle message sending
	//
	// check if we are busy or no messages waiting
	if (!(TXMessagesWaiting != 0 && lineStatus == ILB_BUS_TX_IDLE))
	{
		return;
	}

	// encode message and place in buffer
	uint8_t nOfBytesInMessage = encodeILBMessage(txBuffer, &ILBMessageTXBuffer[activeTXMessageIndex]);

	// check for idle line. This can change on short notice, hence here and not at the beginning.
	if (ILB_HAL_isLineBusy() != 0) {
		return;
	}

	// set the status to sending
	lineStatus = ILB_BUS_TX_SENDING;
	// push message through hardware
	ILB_HAL_StatusTypeDef success = ILB_HAL_UARTSendBytes(&txBuffer[0], nOfBytesInMessage);

	if (success == ILB_HAL_OK) {
		// TODO: Consider to move the counting of the active message counter to the TX complete callback.
		// This would allow to react on failures while the message is transmitted! Since we see issues with the
		// larger/extended bus to the follower, this may be a good
		// adjust status variables. This needs to be handled here. Moving the block to the
		// HAL_UART_TxCpltCallback callback routine causes the program to miscount because the
		// callback is triggered occasionally more than once.
		// !!!! moved section to setTXMessageFinished() this should help to manage bus errors
		//    activeTXMessageIndex++;
		//    if (activeTXMessageIndex >= ILB_MESSAGE_TX_BUFFER_DEPTH) {
		//      activeTXMessageIndex = 0;
		//    }
		//    // count the number of waiting messages down
		//    TXMessagesWaiting--;
	} else {
		lineStatus = ILB_BUS_TX_IDLE;
	}
}

/**
 * Queue a message to be send over the ILB
 *
 * This function will queue a message, which will be send a s soon as possible.
 *
 * @param targetAddress The address of the receiver
 * @param type			Type of the message
 * @param identifier	Identifier of the message. if NULL, the next free identifier of this device is assigned
 * @param payload		Pointer to beginning of payload data block. A copy will be made into the transmission buffer
 * @param payloadSize	Number of bytes in the payload
 * @return				Assigned identifier to message
 */
uint8_t messaging_queueTXMessage(uint8_t targetAddress, ILBBusMessageTypeTypeDef type, uint8_t identifier, uint8_t* payload, uint8_t payloadSize)
{
	// check if we have space in the queue
	if (TXMessagesWaiting >= ILB_MESSAGE_TX_BUFFER_DEPTH) {
		return 0;
	}

	// find the index of the next free slot
	uint8_t targetIndex = activeTXMessageIndex + TXMessagesWaiting;
	if (targetIndex >= ILB_MESSAGE_TX_BUFFER_DEPTH) {
		targetIndex -= ILB_MESSAGE_TX_BUFFER_DEPTH;
	}
	ILBMessageTXBuffer[targetIndex].address = targetAddress;
	// select identifier
	if (identifier == ILB_GENERATE_IDENTIFIER) {
		ILBMessageTXBuffer[targetIndex].identifier = deviceMessageIdentifierCounter;
		deviceMessageIdentifierCounter++;
		if (deviceMessageIdentifierCounter >= ILB_MESSAGE_IDENTIFIER_MAX) {
			deviceMessageIdentifierCounter = ILB_MESSAGE_IDENTIFIER_MIN;
		}
	} else {
		ILBMessageTXBuffer[targetIndex].identifier = identifier;
		// prevent identifier collision
		if (identifier == deviceMessageIdentifierCounter) {
			deviceMessageIdentifierCounter++;
		}
	}
	ILBMessageTXBuffer[targetIndex].messageType = type;
	ILBMessageTXBuffer[targetIndex].payloadSize = payloadSize;
	memcpy(&ILBMessageTXBuffer[targetIndex].payload, payload, payloadSize);
	// mark as ready to be sent
	TXMessagesWaiting++;

	return ILBMessageTXBuffer[targetIndex].identifier;
}

/** Get the next received message to process it
 *
 * Calling this message assumes that the message will be processed and will increase the counter
 *
 * \param
 * \retval
 */
ILBMessageStruct* messaging_getNextWaitingMessage(void)
{
  if (nextRXMessageIndex == activeRXMessageIndex) {
    return NULL;
  }
  nextRXMessageIndex++;
  if (nextRXMessageIndex >= ILB_MESSAGE_RX_BUFFER_DEPTH) {
    nextRXMessageIndex = 0;
  }
  return &ILBMessageRXBuffer[nextRXMessageIndex];
}

/**
 * Service routine that executes pending handler functions and tasks
 *
 * This function must be executed regularly and will
 * - Call the handler function that are associated with an acknowledge message. The assocication is done via the identifier of the returned message. If the core sends a message, it assigns an identifier that must be packaged with the reply
 * - Push command type message to the appropriate handler functions
 * - Push read messages to the appropriate handler functions
 * - Push write messages to the appropriate handler functions.
 *
 */
void messaging_handleWaitingMessages(void)
{

	// TODO: implement address check. If no logic is present, these registers can be read out via the broadcast address
	// 	     once an address has been assigned, the registers should only be accessible via that address
	// 		 this is particularly important for read and write messages

	//
	// handler functions for waiting messages
	//
	if (messageHandlerStatus != ILB_MESSAGE_HANDLER_BUSY) {
		// check if there are unhandled messages
		// check if there is a message for us
		ILBMessageStruct* nextMessage = messaging_getNextWaitingMessage();
		if (nextMessage != NULL) {
			messageHandlerStatus = ILB_MESSAGE_HANDLER_BUSY;
			// if the message is an acknowledge to a previous message, the handler scheduler is responsible
			switch(nextMessage->messageType) {
			case ILB_MESSAGE_ACHKNOWLEDGE:
				// execute the appropriate handler function
				scheduler_handleAnswerToMessage(nextMessage);
				break;

			default:
				// push to address and device type filter
				addressing_handleMessage(nextMessage);
				break;
			}
			messageHandlerStatus = ILB_MESSAGE_HANDLER_IDLE;
		}
	}
}

/**
 * Routine called by hardware layer to indicate that the message has been send
 * @param s
 */
void lib_messageSendSuccess(ILB_HAL_StatusTypeDef s)
{
	switch(s)
	{
	case ILB_HAL_OK:
		activeTXMessageIndex++;
		if (activeTXMessageIndex >= ILB_MESSAGE_TX_BUFFER_DEPTH) {
			activeTXMessageIndex = 0;
		}
		if (TXMessagesWaiting > 0)
		{
			TXMessagesWaiting--;
		}
		lineStatus = ILB_BUS_TX_IDLE;
		break;

	default:
		lineStatus = ILB_BUS_TX_ERROR;
		break;
	}
}
