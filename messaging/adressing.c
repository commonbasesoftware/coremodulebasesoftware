/*
 * adressing.c
 *
 *  Created on: 06.10.2021
 *      Author: A.Niggebaum
 *
 * This file contains routines for address filtering. Depending on the configuration and type of
 * logic, the base software should ignore or handle certain commands or messages.
 *
 * The following device classes are known
 * ILB_CORE_CLASS =								0x01,
 * ILB_PERIPHERAL_MOTION_DAYLIGHT_CLASS =		0x02,
 * ILB_PERIPHERAL_ZIGBEE_CLASS	=				0x07,
 * ILB_PERIPHERAL_BLUETOOTH_CLASS =				0x08,
 * ILB_PERIPHERAL_DALI_CLASS =					0x09,
 * ILB_PERIPHERAL_DRIVER_CLASS =				0x0A,
 * ILB_FOLLOWER_CLASS = 						0xF0
 *
 *
 */

#include <stddef.h>
#include "../ilb.h"
#include "../register/register.h"
#include "../settings/settings.h"
#include "../security/keyChallenge.h"
#include "../reset/reset.h"
#include "../lightEngine/lightEngine.h"
#include "../lightEngine/settings.h"
#include "../logic/loading.h"
#include "../baseSoftware/state.h"
#include "../lightEngine/intensityLookup.h"
#include "../selfTest/test.h"
#include "ilbCommandSelection.h"
#include "messaging.h"

//
//	In case we are compiled as a library, the message handler are defined externally
//
#ifdef LIBRARY_LOGIC
extern const uint8_t libLogic_deviceClass;
extern void libLogic_commandHandler(ILBMessageStruct* message);
extern void libLogic_readHandler(ILBMessageStruct* message);
extern void libLogic_writeHandler(ILBMessageStruct* message);
#endif

// pointer to message handler
static void dummyMessageHandler(ILBMessageStruct* m) { }		// dummy function that the handlers to not run into a null pointer segmentation fault when no logic is loaded
static void (*logicCommandHandler)(ILBMessageStruct* m) 	= dummyMessageHandler;
static void (*logicReadHandler)(ILBMessageStruct* m) 		= dummyMessageHandler;
static void (*logicWriteHandler)(ILBMessageStruct* m) 		= dummyMessageHandler;

//
//	Lookup for functions handling command
//

// function prototypes
void bsw_handleKeyChallengeRequest(ILBMessageStruct* m);

// address handling
void bsw_handleReportUID(ILBMessageStruct* m);
void bsw_handleChangeAddress(ILBMessageStruct* m);

// reset routines
void bsw_handleDeviceReset(ILBMessageStruct* m);
void bsw_handleSettingsReset(ILBMessageStruct* m);
void bsw_handleFactoryReset(ILBMessageStruct* m);

// loading of logic and pwm settings
void bsw_handlePWMSettings(ILBMessageStruct* m);
void bsw_handleLogicErase(ILBMessageStruct* m);
void bsw_handleLogicUpload(ILBMessageStruct* m);

// light output control
void bsw_handleFollowerCMD(ILBMessageStruct* m);
void bsw_handleLightLevelSet(ILBMessageStruct* m);
void bsw_handleCCTSet(ILBMessageStruct* m);
void bsw_handleLightLevelAdvanced(ILBMessageStruct* m);
void bsw_handleCCTAdvanced(ILBMessageStruct* m);

// test procedure
void bsw_handleTestProcedure(ILBMessageStruct* m);

#ifndef LIBRARY_LOGIC
// main lookup structure
static const messageHandler_t BSWCommandHandleLookup[ILB_CMD_MAX] =  {
		dummyMessageHandler,			// index 0 does not exist as command
		bsw_handleReportUID,			// ILB_REPORT_UID
		bsw_handleChangeAddress,		// ILB_CHANGE_ADDRESS
		bsw_handleDeviceReset,			// ILB_RESET_DEVICE
		bsw_handleFactoryReset,			// ILB_FACTORY_RESET_DEVICE
		bsw_handleKeyChallengeRequest,	// ILB_GET_KEY_CHALLENGE
		dummyMessageHandler,			// ILB_MUTE_DEVICE
		dummyMessageHandler,			// ILB_START_FW_UPDATE,
		dummyMessageHandler,			// ILB_SET_DEVICE_INDICATOR
		dummyMessageHandler,			// ILB_BROADCAST_INFO
		dummyMessageHandler,			// ILB_REPORT_MOTION_DAYLIGHT
		dummyMessageHandler,			// ILB_REPORT_BUTTON_EVENT
		bsw_handleLightLevelSet,		// ILB_SET_OUTPUT_LEVEL
		bsw_handleCCTSet,				// ILB_SET_OUTPUT_CCT
		dummyMessageHandler,			// ILB_SET_INDICATE
		dummyMessageHandler,			// ILB_FADE_FINISHED
		dummyMessageHandler,			// ILB_START_OPERATION
		dummyMessageHandler,			// ILB_REPORT_DAYLIGHT_LEVEL
		bsw_handleSettingsReset,		// ILB_SETTINGS_RESET
		bsw_handleFollowerCMD,			// ILB_FOLLOWER_INSTRUCTION
		dummyMessageHandler,			// ILB_FOLLOWER_COMMAND
		bsw_handlePWMSettings,			// ILB_CONFIGURE_PWM_SETTINGS
		bsw_handleLogicErase,			// ILB_ERASE_LOGIC
		bsw_handleLogicUpload,			// ILB_UPLOAD_LOGIC
		bsw_handleLightLevelAdvanced,	// ILB_SET_LEVEL_ADVANCED
		bsw_handleCCTAdvanced,			// ILB_SET_CCT_ADVANCED
		bsw_handleTestProcedure,		// ILB_TEST_INFORMATION
};
#else
// main lookup structure
static const messageHandler_t BSWCommandHandleLookup[ILB_CMD_MAX] =  {
		dummyMessageHandler,			// index 0 does not exist as command
		bsw_handleReportUID,			// ILB_REPORT_UID
		bsw_handleChangeAddress,		// ILB_CHANGE_ADDRESS
		bsw_handleDeviceReset,			// ILB_RESET_DEVICE
		bsw_handleFactoryReset,			// ILB_FACTORY_RESET_DEVICE
		bsw_handleKeyChallengeRequest,	// ILB_GET_KEY_CHALLENGE
		dummyMessageHandler,			// ILB_MUTE_DEVICE
		dummyMessageHandler,			// ILB_START_FW_UPDATE,
		dummyMessageHandler,			// ILB_SET_DEVICE_INDICATOR
		dummyMessageHandler,			// ILB_BROADCAST_INFO
		dummyMessageHandler,			// ILB_REPORT_MOTION_DAYLIGHT
		dummyMessageHandler,			// ILB_REPORT_BUTTON_EVENT
		dummyMessageHandler,			// ILB_SET_OUTPUT_LEVEL
		dummyMessageHandler,			// ILB_SET_OUTPUT_CCT
		dummyMessageHandler,			// ILB_SET_INDICATE
		dummyMessageHandler,			// ILB_FADE_FINISHED
		dummyMessageHandler,			// ILB_START_OPERATION
		dummyMessageHandler,			// ILB_REPORT_DAYLIGHT_LEVEL
		bsw_handleSettingsReset,		// ILB_SETTINGS_RESET
		dummyMessageHandler,			// ILB_FOLLOWER_INSTRUCTION
		dummyMessageHandler,			// ILB_FOLLOWER_COMMAND
		dummyMessageHandler,			// ILB_CONFIGURE_PWM_SETTINGS
		dummyMessageHandler,			// ILB_ERASE_LOGIC
		dummyMessageHandler,			// ILB_UPLOAD_LOGIC
};
#endif

//
//	Definitions for command map
//
typedef enum readWriteAccess {
	NO_ACCESS,
	BROADCAST_ACCESS,
	ADDRESS_ACCESS,
} ReadWriteAccess_t;
// members are bit fields that are equivalent to indices of commands. If the index is set the appropriate handling is performed
// the member readWriteAccess indicates how registers can be accessed
// the member commandMaskBroadcast marks the command to be handled when encountered with a broadcast address
// the member commandMaskAddress marks the command to be handled when encountered with the assigned address
// the member commandMaskLogic pushes the command to the logic instead of handling it in the base software. The command must be active in one of the previous members
typedef struct DeviceHandleLookup {
	ReadWriteAccess_t readWriteAccess;
	uint32_t commandMaskBroadcast;			// command is handled by base software if received via broadcast
	uint32_t commandMaskAddress;			// command is handled by base software if received via directed address (if assigned)
	uint32_t commandMaskLogic;				// command is forwarded to logic instead of being handled by base software. The broadcast and address criteria apply
} DeviceHandleLookup_t;

//
//	Structures defining message handling for different operation modes
//
static const DeviceHandleLookup_t FollowerMessageHandleLook = {
		BROADCAST_ACCESS,
		(CMD_REPORT_UID | CMD_CHANGE_ADDRESS | CMD_RESET_DEVICE | CMD_FACTORY_RESET_DEVICE | CMD_GET_KEY_CHALLENGE | CMD_SETTINGS_RESET | CMD_CONFIGURE_PWW_SETTIGNS | CMD_FOLLOWER_INSTRUCTION | CMD_ERASE_LOGIC | CMD_UPLOAD_LOGIC | CMD_TEST_INFORMATION),		// broadcast
		CMD_NONE,		// address
		CMD_NONE,		// logic
};
static const DeviceHandleLookup_t CoreMessageHandleLookup 	= {
		BROADCAST_ACCESS,
		(CMD_REPORT_UID | CMD_RESET_DEVICE | CMD_FACTORY_RESET_DEVICE | CMD_GET_KEY_CHALLENGE | CMD_SETTINGS_RESET | CMD_CONFIGURE_PWW_SETTIGNS | CMD_ERASE_LOGIC | CMD_UPLOAD_LOGIC | CMD_FADE_FINISHED | CMD_REPORT_BUTTON_EVENT | CMD_REPORT_MOTION_DAYLIGHT | CMD_SET_OUTPUT_LEVEL | CMD_SET_OUTPUT_CCT | CMD_TEST_INFORMATION),
		CMD_NONE,
		CMD_FADE_FINISHED | CMD_REPORT_BUTTON_EVENT | CMD_REPORT_MOTION_DAYLIGHT | CMD_SET_OUTPUT_CCT | CMD_SET_OUTPUT_LEVEL | CMD_SET_OUTPUT_LEVEL_ADVANCED | CMD_SET_OUTPUT_CCT_ADVANCED
};
static const DeviceHandleLookup_t DriverPeripheralHandleLookup 	= {
		ADDRESS_ACCESS,
		(CMD_REPORT_UID | CMD_CHANGE_ADDRESS | CMD_BROADCAST_INFO | CMD_TEST_INFORMATION),
		(CMD_FACTORY_RESET_DEVICE | CMD_RESET_DEVICE | CMD_GET_KEY_CHALLENGE | CMD_SETTINGS_RESET | CMD_SET_OUTPUT_LEVEL | CMD_SET_OUTPUT_CCT | CMD_SET_OUTPUT_LEVEL_ADVANCED | CMD_SET_OUTPUT_CCT_ADVANCED | CMD_CONFIGURE_PWW_SETTIGNS | CMD_ERASE_LOGIC | CMD_UPLOAD_LOGIC | CMD_TEST_INFORMATION),
		CMD_NONE
};
#ifdef LIBRARY_LOGIC
static const DeviceHandleLookup_t PeripheralLibraryLookup = {
		ADDRESS_ACCESS,
		(CMD_REPORT_UID | CMD_CHANGE_ADDRESS | CMD_BROADCAST_INFO | CMD_START_OPERATION | CMD_TEST_INFORMATION),
		(CMD_FACTORY_RESET_DEVICE | CMD_RESET_DEVICE | CMD_SETTINGS_RESET | CMD_GET_KEY_CHALLENGE | CMD_TEST_INFORMATION),
		(CMD_START_OPERATION)
};
#endif

//
//	Management of device type and address
//
static ILBDeviceClass_t deviceType 			= ILB_FOLLOWER_CLASS;						// default class when booting or no logic is loading
static uint8_t assignedAddress 				= ILB_MESSAGE_BROADCAST_ADDRESS;			// default address: none. Equivalent to broadcast address
static DeviceHandleLookup_t activeLookup 	= FollowerMessageHandleLook;

ILB_HAL_StatusTypeDef addressing_init(uint8_t deviceClass, void(*commandHandler)(ILBMessageStruct* m), void(*readHandler)(ILBMessageStruct* m), void(*writeHandler)(ILBMessageStruct* m))
{
	uint8_t storedAddress = 0;
	ILB_HAL_StatusTypeDef success = ILB_HAL_OK;
	// check if we can support the device class and set the filter for the messages
	switch (deviceClass)
	{
	case ILB_FOLLOWER_CLASS:
		deviceType = ILB_CORE_CLASS;
		activeLookup = FollowerMessageHandleLook;
		break;

	case ILB_CORE_CLASS:
		deviceType = ILB_CORE_CLASS;
		activeLookup = CoreMessageHandleLookup;
		logicCommandHandler = commandHandler;
		logicReadHandler    = readHandler;
		logicWriteHandler   = writeHandler;
		break;

	// request to be a driver peripehral
	case ILB_PERIPHERAL_DRIVER_CLASS:
		if(ILB_HAL_getAddress(&storedAddress) == ILB_HAL_OK)
		{
			if (storedAddress >= ILB_ADDRESS_MIN && storedAddress <= ILB_ADDRESS_MAX)
			{
				success = ILB_HAL_OK;
				deviceType = ILB_PERIPHERAL_DRIVER_CLASS;
				activeLookup = DriverPeripheralHandleLookup;
				assignedAddress = storedAddress;
			} else {
				success = ILB_HAL_INVALID;
			}
		} else {
			success = ILB_HAL_ERROR;
		}
		break;

	default:
		// case for all other peripheral types.
#ifdef LIBRARY_LOGIC
		if (ILB_HAL_getAddress(&storedAddress) == ILB_HAL_OK)
		{
			deviceType = libLogic_deviceClass;
			activeLookup = PeripheralLibraryLookup;
			logicCommandHandler = libLogic_commandHandler;
			logicReadHandler = libLogic_readHandler;
			logicWriteHandler = libLogic_writeHandler;
			if (storedAddress >= ILB_ADDRESS_MIN && storedAddress <= ILB_ADDRESS_MAX)
			{
				success = ILB_HAL_OK;
				assignedAddress = storedAddress;
			} else {
				success = ILB_HAL_INVALID;
			}
		} else {
			success = ILB_HAL_ERROR;
		}
#else
		success = ILB_HAL_ERROR;
#endif
	}
	return success;
}

void addressing_handleMessage(ILBMessageStruct* message)
{

	// only handle message if we are addressed or it is a broadcast
	// note that broadcast has higher priority
	uint32_t mask = 0;
	if (message->address == ILB_MESSAGE_BROADCAST_ADDRESS ||
		(deviceType == ILB_CORE_CLASS && message->address == ILB_MESSAGE_BROADCAST_ADDRESS_CORE))
	{
		mask = activeLookup.commandMaskBroadcast;
	}
	else if (message->address == assignedAddress)
	{
		mask = activeLookup.commandMaskAddress;
	}
	else if (deviceType == ILB_CORE_CLASS)
	{
		mask = activeLookup.commandMaskLogic;
	}
	else
	{
		// the message is not of our concern
		return;
	}

	// handle the type of message

	// command
	if (message->messageType == ILB_MESSAGE_COMMAND &&
		message->payload[0] < ILB_CMD_MAX)
	{
		uint32_t commandMask = (0x01 << message->payload[0]);
		// check if we need to execute the command
		if ((commandMask & mask) != commandMask)
		{
			return;
		}

		// execute the command if the BSW needs to handle it. Otherwise forward to logic
		if ((commandMask & activeLookup.commandMaskLogic) == commandMask)
		{
			logicCommandHandler(message);
		}
		else
		{
			BSWCommandHandleLookup[message->payload[0]](message);
		}
		return;
	}

	// exit if we want to read but need to be addressed
	if (activeLookup.readWriteAccess == ADDRESS_ACCESS && assignedAddress != message->address)
	{
		return;
	}

	// read
	else if (message->messageType == ILB_MESSAGE_READ &&
			 message->payloadSize == 3)
	{
		if (register_handleRead(message) != ILB_HAL_OK)
		{
			logicReadHandler(message);
		}
		return;
	}

	// write
	else if (message->messageType == ILB_MESSAGE_WRITE)
	{
		if (register_handleWrite(message) != ILB_HAL_OK)
		{
			logicWriteHandler(message);
		}
		return;
	}
}

//
//	Implementation of handling functions
//
/**
 * Report the UID
 *
 * @param m
 */
void bsw_handleReportUID(ILBMessageStruct* m)
{
	// to avoid collision, we need to wait a bit
	uint32_t delay = ILB_HAL_getTick() + (ILB_HAL_getUID() & 0xFF);
	while (ILB_HAL_getTick() < delay)
	{
		// do nothing
	}
	// prepare answer
	ILBMessageStruct answer;
	answer.address = deviceType;
	answer.identifier = m->identifier;
	answer.messageType = ILB_MESSAGE_ACHKNOWLEDGE;
	uint32_t UID = ILB_HAL_getUID();
	answer.payloadSize = 4;
	answer.payload[0] = ((UID >> 24) & 0xFF);
	answer.payload[1] = ((UID >> 16) & 0xFF);
	answer.payload[2] = ((UID >> 8) & 0xFF);
	answer.payload[3] = (UID & 0xFF);

	// send message
	messaging_sendMessage(&answer, NULL, 0, NULL);
}

/**
 * Change the address of the device
 *
 * @param m
 */
void bsw_handleChangeAddress(ILBMessageStruct* m)
{
	// check if the message has all the information we need
	if (m->payloadSize != 6)
	{
		// invalid. Respond with error code
		messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
		return;
	}

	// the first 4 bytes need to match our UID, the last is the address we should assume
	uint32_t UID = ((m->payload[1] << 24) | (m->payload[2] << 16) | (m->payload[3] << 8) | m->payload[4]);
	if (UID != ILB_HAL_getUID())
	{
		// its not us that is being talked to. Ignore
		return;
	}

	// UID matches. Assume the address
	if (m->payload[5] > ILB_ADDRESS_MIN && m->payload[5] < ILB_ADDRESS_MAX)
	{
		// store address
		assignedAddress = m->payload[5];
		ILB_HAL_storeAddress(&assignedAddress);
#ifndef LIBRARY_LOGIC
		// change role
		deviceType = ILB_PERIPHERAL_DRIVER_CLASS;
		activeLookup = DriverPeripheralHandleLookup;
#endif
		// overwrite source address
		m->address = assignedAddress;
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	// address out of range
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

/**
 * Answer to key challenge request
 *
 * @param m
 */
void bsw_handleKeyChallengeRequest(ILBMessageStruct* m)
{
	// prepare payload
	uint8_t payload[] = {0, 0};
	security_openKeyWindow(&payload[0], &payload[1]);

	// send message
	messaging_sendAnswer(m, &payload[0], 2);
}

/**
 * Handle power cycle of the device
 *
 * @param m
 */
void bsw_handleDeviceReset(ILBMessageStruct* m)
{
	// check for valid key
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		reset_triggerPowerCycle();
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}


/**
 * Handle settings reset of device
 *
 * @param m
 */
void bsw_handleSettingsReset(ILBMessageStruct* m)
{
	// check for valid key
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		reset_triggerSettingsReset();
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

/**
 * Handle factory reset of device
 * @param m
 */
void bsw_handleFactoryReset(ILBMessageStruct* m)
{
	// check for valid key
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		reset_triggerFactoryReset();
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

#ifndef LIBRARY_LOGIC
/**
 * Erase the logic
 *
 * @param m
 */
void bsw_handleLogicErase(ILBMessageStruct* m)
{
	// check for valid key
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		// send acknowledge, indicating that we started the process
		messaging_respondErrorMessage(m, ILB_OK);

		// start process
		if (logicLoading_eraseLogic() != ILB_HAL_OK)
		{
			// respond failure
			messaging_respondErrorMessage(m, ILB_ERROR);
		}
		else
		{
			// respond success
			messaging_respondErrorMessage(m, ILB_OK);
		}

		// now that the logic has been erased or partially erased, we cannot allow any eventual
		// logic to continue to run. We must reboot but give it some time to send out the success message
		ilb_rebootSystem(500);
	}
}

/**
 * Upload new logic
 * @param m
 */
void bsw_handleLogicUpload(ILBMessageStruct* m)
{
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		// send acknowledge
		messaging_respondErrorMessage(m, ILB_OK);

		// start the process
		logicLoading_startUpload();
	}
}

/**
 * Handle a follower command
 *
 * @param m
 */
void bsw_handleFollowerCMD(ILBMessageStruct* m)
{
	// get follower group
	uint8_t followerGroup = lightEngineSettings_getFollowerGroup();
	if (followerGroup != FOLLOWER_GROUP_NONE &&
			 m->payloadSize == 7 &&
			 ((m->payload[1] & followerGroup) == followerGroup))
	{
		// handle fade
		uint16_t fadeTimeMS = ((m->payload[2] << 8) | m->payload[3]);
		uint8_t targetLevel = m->payload[4];
		uint16_t targetMired = ((m->payload[5] << 8) | m->payload[6]);

		lightEngine_startFade(targetLevel, targetMired, fadeTimeMS, INTENSITY_LOOKUP_LINEAR);

		// NO response is necessary
	}
	// advanced version of command
	else if (followerGroup != FOLLOWER_GROUP_NONE &&
			 m->payloadSize == 8 &&
			 ((m->payload[1] & followerGroup) == followerGroup))
	{
		// handle fade
		uint16_t fadeTimeMS = ((m->payload[2] << 8) | m->payload[3]);
		uint8_t targetLevel = m->payload[4];
		uint16_t targetMired = ((m->payload[5] << 8) | m->payload[6]);

		lightEngine_startFade(targetLevel, targetMired, fadeTimeMS, (m->payload[7] & ADVANCED_CMD_INTENSITY_MASK));

		// NO response is necessary
	}
}

/**
 * Handle upload of PWM settings
 *
 * @param m
 */
void bsw_handlePWMSettings(ILBMessageStruct* m)
{
	// check for validity
	if (m->payloadSize == 2 && security_useKey(&m->payload[1]) == SECURITY_KEY_VALID)
	{
		// send acknowledge
		messaging_respondErrorMessage(m, ILB_OK);

		// start process
		lightEngineSettings_startUpload();
	}
}

/**
 * Handle set command for light level
 *
 * @param m
 */
void bsw_handleLightLevelSet(ILBMessageStruct* m)
{
	// we expect 3 extra bytes payload. 16bits for fade time in ms, 1 byte for target level
	if (m->payloadSize == 4)
	{
		// extract values and start fade
		lightEngine_setLevel(m->payload[3], ((m->payload[1] << 8) | m->payload[2]), INTENSITY_LOOKUP_LINEAR);
		// reply ok
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

/**
 * Handle command to set light level and CCT
 *
 * @param m
 */
void bsw_handleCCTSet(ILBMessageStruct* m)
{
	// we expect 5 extra bytes payload. 16bits fade tim in ms, 1 byte target level 16bits target mired
	if (m->payloadSize == 6)
	{
		// start fade
		lightEngine_startFade(m->payload[3], ((m->payload[4] << 8) | m->payload[5]), ((m->payload[1] << 8) | m->payload[2]), INTENSITY_LOOKUP_LINEAR);
		// reply ok
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

/**
 * Handle set command for light level
 *
 * @param m
 */
void bsw_handleLightLevelAdvanced(ILBMessageStruct* m)
{
	// we expect 3 extra bytes payload. 16bits for fade time in ms, 1 byte for target level
	if (m->payloadSize == 5)
	{
		// extract values and start fade
		lightEngine_setLevel(m->payload[3], ((m->payload[1] << 8) | m->payload[2]), (m->payload[4] & ADVANCED_CMD_INTENSITY_MASK));
		// reply ok
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

/**
 * Handle command to set light level and CCT
 *
 * @param m
 */
void bsw_handleCCTAdvanced(ILBMessageStruct* m)
{
	// we expect 5 extra bytes payload. 16bits fade tim in ms, 1 byte target level 16bits target mired
	if (m->payloadSize == 7)
	{
		// start fade
		lightEngine_startFade(m->payload[3], ((m->payload[4] << 8) | m->payload[5]), ((m->payload[1] << 8) | m->payload[2]), (m->payload[6] & ADVANCED_CMD_INTENSITY_MASK));
		// reply ok
		messaging_respondErrorMessage(m, ILB_OK);
		return;
	}
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}
#endif

/**
 * Handle the test procedure command
 *
 * Two modes are supported
 * 1) Completely automatic test sequence
 * 2) Parameters in the payload determine the settings. This override any eventual PWM limits.
 *
 * The command must be authorized with a key challenge unless the device is already in a test sequence.
 *
 * @param m
 */
void bsw_handleTestProcedure(ILBMessageStruct* m)
{
	// are we already in test mode?
	// if not, the command must be terminated with a valid key challenge byte
	if (ilb_getState() != ILB_BASE_TEST)
	{
		switch (m->payloadSize)
		{
		case 2:		// case just the command byte and the payload byte
			if (security_useKey(&m->payload[1]) != SECURITY_KEY_VALID)
			{
				messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
				return;
			} else {
				// cut off security key. Through this we acknowlege that the key is valid.
				m->payloadSize--;
			}
			break;

		case 4:		// case where PWM levels for channel A and B are supplied, the command byte and the challenge byte
			if (security_useKey(&m->payload[3]) != SECURITY_KEY_VALID)
			{
				messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
				return;
			} else {
				// cut off security key. By this, we acknowledge that the key is valid.
				m->payloadSize--;
			}
			break;

		default:	// no valid format
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			return;
		}
	}

	// distribute to the calls
	switch (m->payloadSize)
	{

	case 2:		// we are already in test mode but the key challenge was sent again
		if (security_useKey(&m->payload[1]) != SECURITY_KEY_VALID)
		{
			// this is not allowed
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			return;
		}
		// no break needed, we determined that the key is valid
		m->payloadSize--;
	case 1:		// trigger for start sequence
		selfTest_startSequence(m);
		return;
		break;

	case 4:		// we are already in test mode but the key challenge was sent again
		if (security_useKey(&m->payload[3]) != SECURITY_KEY_VALID)
		{
			// invalid key
			messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
			return;
		}
		// no break needed. We determined that the key is valid
		m->payloadSize--;
	case 3:		// apply settings. Case that we are already in test mode or have unlocked the test state
		selfTest_setSetting(m);
		return;
		break;

	default:		// no valid length
		messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
		return;
	}

	// not in a valid state or message
	messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
}

//
//	Accessor funtions
//
/**
 * Accessor to device type. Intended for light engine to check if a fade finished command should be emitted
 *
 * @return
 */
uint8_t addressing_getDeviceClass(void)
{
	return deviceType;
}

/**
 * Accessor to assigned address
 * @return
 */
uint8_t addressing_getAssignedAddress(void)
{
	return assignedAddress;
}
