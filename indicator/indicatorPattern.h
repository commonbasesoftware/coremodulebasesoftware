/**
 * @file indicatorPattern.h
 *
 * @date 17.09.2021
 * @author A.Niggebaum
 *
 * @brief Blink patterns for the red and green indicator on the core.
 *
 * @ingroup BaseSoftwareGroup
 */


#ifndef ILB_INDICATOR_INDICATORPATTERN_H_
#define ILB_INDICATOR_INDICATORPATTERN_H_

// execute factory reset
#define BLINK_PATTERN_EXECUTE_FACTORY_RESET_GREEN			0b00110011
#define BLINK_PATTERN_EXECUTE_FACTORY_RESET_RED				0b11001100

// execute settings reset
#define BLINK_PATTERN_EXECUTE_SETTINGS_RESET_GREEN			BLINK_PATTERN_EXECUTE_FACTORY_RESET_GREEN
#define BLINK_PATTERN_EXECUTE_SETTINGS_RESET_RED			BLINK_PATTERN_EXECUTE_FACTORY_RESET_RED

// power cycle/reset
#define BLINK_PATTERN_EXECUTE_RESET_GREEN					0b11110000
#define BLINK_PATTERN_EXECUTE_RESET_RED						0b00001111

// base software error pattern
#define BLINK_PATTERN_ERROR_GREEN							0b00000001
#define BLINK_PATTERN_ERROR_RED								0b11111111

// blink pattern to indicate that we are now in on/off mode
#define BLINK_PATTERN_ON_OFF_GREEN							0b00000001
#define BLINK_PATTERN_ON_OFF_RED							0b00000000

// blink pattern to indicate that we are now in follower mode
#define BLINK_PATTERN_FOLLOWER_GREEN						0b00000101
#define BLINK_PATTERN_FOLLOWER_RED							0b00000000

// blink pattern to indicate that we have booted into driver peripehral mode
#define BLINK_PATTERN_DRV_PERIPHERAL_GREEN					0b00010101
#define BLINK_PATTERN_DRV_PERIPHERAL_RED					0b00000000

// blink pattern to indicate that we are loading settings
#define BLINK_PATTERN_LOAD_DRIVER_SETTINGS_GREEN			0b01010101
#define BLINK_PATTERN_LOAD_DRIVER_SETTINGS_RED				0b00000000

// blink pattern for test mode
#define BLINK_PATTERN_TEST_MODE_GREEN						0b00110011
#define BLINK_PATTERN_TEST_MODE_RED							0b00001111

#endif /* ILB_INDICATOR_INDICATORPATTERN_H_ */
