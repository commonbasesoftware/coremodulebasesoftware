/*
 * logicLoading.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include "../ilb.h"
#include "loading.h"
#include "../settings/settings.h"
#include "../messaging/messaging.h"
#include "../baseSoftware/state.h"
#include "../register/register.h"
#include <string.h>

#define PAGE_SIZE_IN_BYTES			128						//!< size of a page in memory in bytes
#define LOGIC_REQUEST_CHUNK_SIZE	32						//!< size of a chunk that is requested by the device. The page size divided by the chunk size must be a full integer
#define PAGE_BUFFER_WORD_SIZE		(PAGE_SIZE_IN_BYTES/4)	//!< size of the page buffer in words.

#ifdef UNIT_TEST
	extern uint32_t 				interface_version;
	extern BaseSoftwareInterface_t*	interface_base;
	extern CoreLogicInterface_t*	interface_core;
#else
// logic version defined in the linker script
// see here how to handle linker symbols https://sourceware.org/binutils/docs/ld/Source-Code-Reference.html
extern uint32_t _interface_version;
extern uint32_t _interface_location;
extern uint32_t _LOGIC_ORIGIN;

// interface structure pointers
static BaseSoftwareInterface_t* interface_base = 0x00;
static CoreLogicInterface_t*    interface_core = 0x00;
#endif

//
//	Definition and enumerations for supported versions
//
#define SUPPORTED_LOGIC_VERSION			02
typedef enum logicUploadElements {
	LOGIC_UPLOAD_CMD_COMMON_HEADER		= 0x01,
	LOGIC_UPLOAD_CMD_GET_HEADER			= 0x02,
	LOGIC_UPLOAD_CMD_GET_LOGIC_BLOCK	= 0x03
} LogicUploadProgress_t;

// status of upload process
static LogicUploadStatus_t uploadProgess = LOGIC_UPLOAD_IDLE;
// size of logic to be uploaded
static uint32_t sizeOfLogicToUploadBytes = 0;
static uint32_t logicChecksum = 0;
static uint32_t logicBytesTransferred = 0;
static uint16_t logicChunkRequested = 0;
// structure of logic header to be written to memory
static CoreLogicInterface_t interfaceToLoad;

typedef struct pageBufferStruct {
	uint32_t wordBufferPage[PAGE_BUFFER_WORD_SIZE];
	uint8_t nOfBytesInBuffer;
	uint16_t nOfPagesToWrite;
	uint16_t currentPageIndex;
} PageBuffer_t;
static PageBuffer_t pageBuffer;

//
//	Private function prototypes
//
void logicUploadRequestCallback(ILBMessageStruct* msg);
void logicUploadRequestTimeout(void);
void logicUploadTerminate(LogicUploadStatus_t exitCode);		//!< centralised exit function to perform clean up tasks

/**
 * Loads the logic. Checks compatibility of interfaces
 *
 * @return ILB_HAL_OK when loading is found. Error otherwise
 */
ILB_HAL_StatusTypeDef logicLoading_init(void)
{
#ifdef UNIT_TEST
	// we are compiling for the unit test
	uint32_t version = interface_version;

	// check if the simulation provides the interface
	if (interface_base == NULL || interface_core == NULL)
	{
		return ILB_HAL_ERROR;
	}

#else
	// copy to local variables
	// ! linker script variables need to be accessed by address. See ld manual (https://sourceware.org/binutils/docs/ld/Source-Code-Reference.html)
	// one consequence is, that we cannot do calculations with the values. E.g. using interface_location - logicOrigin does NOT work as expected
	uint32_t version = (uint32_t)&_interface_version;
	uint32_t interface_location = (uint32_t)&_interface_location;
	uint32_t logicOrigin = (uint32_t)&_LOGIC_ORIGIN;
	// load the structures from memory
	interface_base = (BaseSoftwareInterface_t*)(interface_location);
	interface_core = (CoreLogicInterface_t*)(logicOrigin);
#endif

	// push information to register
	register_updateInterfaceRegister(interface_base, interface_core);

	// confirm version
	// compiler will throw warning of value and pointer comparison here. However *version does NOT work (see ld manual above)
	if (interface_base->version == version &&
		interface_core->version == version)
	{
#ifdef UNIT_TEST
		if (interface_core == NULL)
		{
			return ILB_HAL_ERROR;
		}
		if (interface_core->logic == NULL)
		{
			return ILB_HAL_ERROR;
		}
		if (interface_core->init == NULL)
		{
			return ILB_HAL_ERROR;
		}
		if (interface_core->commandHandler == NULL)
		{
			return ILB_HAL_ERROR;
		}
		if (interface_core->readHandler == NULL)
		{
			return ILB_HAL_ERROR;
		}
		if (interface_core->writeHandler == NULL)
		{
			return ILB_HAL_ERROR;
		}
#else
		// check that the target of the function pointer lies within the logic region
		// since we (currently) do not track the size of the logic, we can only assure that it does not lie in the base software region
		if ((uint32_t)interface_core->init < logicOrigin)
		{
			return ILB_HAL_ERROR;
		}

		if ((uint32_t)interface_core->logic < logicOrigin)
		{
			return ILB_HAL_ERROR;
		}

		if ((uint32_t)interface_core->commandHandler < logicOrigin)
		{
			return ILB_HAL_ERROR;
		}

		if ((uint32_t)interface_core->readHandler < logicOrigin)
		{
			return ILB_HAL_ERROR;
		}

		if ((uint32_t)interface_core->writeHandler < logicOrigin)
		{
			return ILB_HAL_ERROR;
		}
#endif
		// notify that there is a logic and that it should be executed.
		return ILB_HAL_OK;
	}

	return ILB_HAL_ERROR;
}

/**
 * Erase the logic
 */
ILB_HAL_StatusTypeDef logicLoading_eraseLogic(void)
{
	// !! when we erase the logic, we also need to erase any eventual address table
	// could just erase the complete memory that can be managed by the logic
	// The only criterion we can check later is the number of peripehrals but not the
	// type. This means we can exchange the logic and as long as the number of peripherals
	// stays the same, there is no way we can detect that the stored address table is incompatible
	// with the new logic.
	ILB_HAL_StatusTypeDef success = ILB_HAL_eraseLogic();
	return success;
}

/**
 * Start the process of uploading a logic
 */
void logicLoading_startUpload(void)
{
	// set status of base software
	ilb_setState(ILB_BASE_LOGIC_LOADING);

	// reset progress tracker
	uploadProgess = LOGIC_UPLOAD_GET_BASE_HEADER;

	// send out the request
	ILBMessageStruct request;
	request.address = ILB_MESSAGE_BROADCAST_ADDRESS;
	request.identifier = ILB_GENERATE_IDENTIFIER;
	request.messageType = ILB_MESSAGE_COMMAND;
	request.payloadSize = 2;
	request.payload[0] = ILB_UPLOAD_LOGIC;
	request.payload[1] = LOGIC_UPLOAD_CMD_COMMON_HEADER;

	// send
	messaging_sendMessage(&request, logicUploadRequestCallback, LOGIC_UPLOAD_TIMEOUT_MS, logicUploadRequestTimeout);
}

/**
 * Initialise structures and elements in the base software required by the logic
 * @param nOfSlowTimers
 * @return
 */
ILB_HAL_StatusTypeDef logicLoading_initBase(uint8_t nOfSlowTimers)
{
	return ILB_HAL_ERROR;
}

/**
 * Accessor to loaded interface.
 *
 * @param interface
 * @return
 */
ILB_HAL_StatusTypeDef logicLoading_getCoreInterface(CoreLogicInterface_t* interface)
{
	if (interface_core != 0x0)
	{
		*interface = *interface_core;
		return ILB_HAL_OK;
	}
	return ILB_HAL_ERROR;
}

/**
 * Callback to a request to a piece of information requested when uploading logic
 *
 * @param msg
 */
void logicUploadRequestCallback(ILBMessageStruct* msg)
{
	// check if the answer is an error (messages past here already are checked for ACK and matching identifier)
	if (msg->payloadSize == 1)
	{
		uploadProgess = LOGIC_UPLOAD_ERROR;
		return;
	}

	// check the stage of the progress

	// handle response to universal header request
	if (uploadProgess == LOGIC_UPLOAD_GET_BASE_HEADER)
	{
		// we expect 10 bytes.
		// 0: version MSB
		// 1: version LSB
		// 2: size of logic byte 3
		// 3: size of logic byte 2
		// 4: size of logic byte 1
		// 5: size of logic byte 0
		// 6: checksum of logic byte 3
		// 7: checksum of logic byte 2
		// 8: checksum of logic byte 1
		// 9: checksum of logic byte 0
		if (msg->payloadSize != 10)
		{
			// invalid number of bytes. Abort with error
			uploadProgess = LOGIC_UPLOAD_ERROR;
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}
		// decode message content
		uint16_t interfaceVersion = ((msg->payload[0] << 8) | msg->payload[1]);
		if (interfaceVersion != SUPPORTED_LOGIC_VERSION)
		{
			// not compatible version. Response error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			uploadProgess = LOGIC_UPLOAD_ERROR;
			return;
		}
		// decode message size
		sizeOfLogicToUploadBytes = ((msg->payload[2] << 24) | (msg->payload[3] << 16) | (msg->payload[4] << 8) | msg->payload[5]);
		logicChecksum 			 = ((msg->payload[6] << 24) | (msg->payload[7] << 16) | (msg->payload[8] << 8) | msg->payload[9]);

		// TODO: check supported logic size

		// acknowledge receival
		messaging_respondErrorMessage(msg, ILB_OK);

		// seems compatible. Move on to next piece of information
		interfaceToLoad.version = SUPPORTED_LOGIC_VERSION;

		// request next piece of information
		ILBMessageStruct request;
		request.address = ILB_MESSAGE_BROADCAST_ADDRESS;
		request.identifier = ILB_GENERATE_IDENTIFIER;
		request.messageType = ILB_MESSAGE_COMMAND;
		request.payloadSize = 2;
		request.payload[0]= ILB_UPLOAD_LOGIC;
		request.payload[1] = LOGIC_UPLOAD_CMD_GET_HEADER;

		// send
		messaging_sendMessage(&request, logicUploadRequestCallback, LOGIC_UPLOAD_TIMEOUT_MS, logicUploadRequestTimeout);

		// progress
		uploadProgess = LOGIC_UPLOAD_GET_HEADER;
		return;

	}
	// handle response to version specific header
	else if (uploadProgess == LOGIC_UPLOAD_GET_HEADER)
	{
		// we expect exactly 28 bytes payload
		// 00-05: 6 bytes of GTIN
		// 06-07: 2 byte of major version
		// 08-09: 2 byte of minor version
		// 10-13: Pointer to init function of logic
		// 14-17: Pointer to logic routine
		// 18-21: Pointer to command handler
		// 22-25: Pointer to read handler
		// 26-29: Pointer to write handler
		if (msg->payloadSize != 30)
		{
			// invalid number of bytes. Abort with error
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}
		interfaceToLoad.GTIN[0] 	 = msg->payload[0];
		interfaceToLoad.GTIN[1] 	 = msg->payload[1];
		interfaceToLoad.GTIN[2] 	 = msg->payload[2];
		interfaceToLoad.GTIN[3] 	 = msg->payload[3];
		interfaceToLoad.GTIN[4] 	 = msg->payload[4];
		interfaceToLoad.GTIN[5] 	 = msg->payload[5];
		interfaceToLoad.versionMajor = ((msg->payload[6] << 8) | msg->payload[7]);
		interfaceToLoad.versionMinor = ((msg->payload[8] << 8) | msg->payload[9]);
		// Extract the pointers
		uint32_t pointerToInit 		= ((msg->payload[10] << 24) | (msg->payload[11] << 16) | (msg->payload[12] << 8) | msg->payload[13]);
		uint32_t pointerToLogic 	= ((msg->payload[14] << 24) | (msg->payload[15] << 16) | (msg->payload[16] << 8) | msg->payload[17]);
		uint32_t pointerToCommand 	= ((msg->payload[18] << 24) | (msg->payload[19] << 16) | (msg->payload[20] << 8) | msg->payload[21]);
		uint32_t pointerToRead 		= ((msg->payload[22] << 24) | (msg->payload[23] << 16) | (msg->payload[24] << 8) | msg->payload[25]);
		uint32_t pointerToWrite 	= ((msg->payload[26] << 24) | (msg->payload[27] << 16) | (msg->payload[28] << 8) | msg->payload[29]);

#ifndef UNIT_TEST
		// range check for pointer
		uint32_t logicOrigin = (uint32_t)&_LOGIC_ORIGIN;
		if (pointerToInit > (logicOrigin + sizeOfLogicToUploadBytes) || pointerToInit < logicOrigin)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}

		if (pointerToLogic > (logicOrigin + sizeOfLogicToUploadBytes) || pointerToLogic < logicOrigin)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}

		if (pointerToCommand > (logicOrigin + sizeOfLogicToUploadBytes) || pointerToCommand < logicOrigin)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}

		if (pointerToRead > (logicOrigin + sizeOfLogicToUploadBytes) || pointerToRead < logicOrigin)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}

		if (pointerToWrite > (logicOrigin + sizeOfLogicToUploadBytes) || pointerToWrite < logicOrigin)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}
#endif

		// prepare the memory. Abort on error
		if (ILB_HAL_logicPrepareMemory() != ILB_HAL_OK)
		{
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		} else {
			// notify that message is ok
			messaging_respondErrorMessage(msg, ILB_OK);
		}

		// decide how many bytes to request
		if (sizeOfLogicToUploadBytes < LOGIC_REQUEST_CHUNK_SIZE)
		{
			logicChunkRequested = sizeOfLogicToUploadBytes;
		} else {
			logicChunkRequested =  LOGIC_REQUEST_CHUNK_SIZE;
		}

		// pointer seem ok. Store in structure
		interfaceToLoad.init 			= (initFunc_t)pointerToInit;
		interfaceToLoad.logic 			= (logicRoutine_t)pointerToLogic;
		interfaceToLoad.commandHandler 	= (messageHandler_t)pointerToCommand;
		interfaceToLoad.readHandler		= (messageHandler_t)pointerToRead;
		interfaceToLoad.writeHandler	= (messageHandler_t)pointerToWrite;

		// prepare buffer
		pageBuffer.nOfBytesInBuffer = 0;
		pageBuffer.currentPageIndex = 0;
		pageBuffer.nOfPagesToWrite = sizeOfLogicToUploadBytes / PAGE_SIZE_IN_BYTES + 1;

		// prepare tracker
		logicBytesTransferred = 0;

		// header seems ok. Proceed to next step
		ILBMessageStruct request;
		request.address = ILB_MESSAGE_BROADCAST_ADDRESS;
		request.identifier = ILB_GENERATE_IDENTIFIER;
		request.messageType = ILB_MESSAGE_COMMAND;
		request.payloadSize = 8;
		request.payload[0]= ILB_UPLOAD_LOGIC;
		request.payload[1] = LOGIC_UPLOAD_CMD_GET_LOGIC_BLOCK;
		request.payload[3] = 0;							// program offset byte 0
		request.payload[4] = 0;							// program offset byte 1
		request.payload[5] = 0;							// program offset byte 2
		request.payload[6] = 0;							// program offset byte 3
		request.payload[7] = logicChunkRequested;		// number of bytes to send

		// send
		messaging_sendMessage(&request, logicUploadRequestCallback, LOGIC_UPLOAD_TIMEOUT_MS, logicUploadRequestTimeout);

		// progress
		uploadProgess = LOGIC_UPLOAD_GET_LOGIC_BLOCK;
		return;

	}
	// handle response to request of logic block (the program itself)
	else if (uploadProgess == LOGIC_UPLOAD_GET_LOGIC_BLOCK)
	{
		// check if the payload size matches
		if (msg->payloadSize != logicChunkRequested ||
			(logicChunkRequested == 1 && msg->payloadSize != 2 ))
		{
			// we did not get the number of bytes we asked for. Abort with error
			logicUploadTerminate(LOGIC_UPLOAD_ERROR);
			// respond error
			messaging_respondErrorMessage(msg, ILB_ERROR);
			return;
		}

		// reply with acceptance of message
		messaging_respondErrorMessage(msg, ILB_OK);

		// handle the received bytes
		// we need to build up words and once a word is finished, it should be placed in the buffer.
		// Once the buffer is full, we must write it to the memory.
		uint32_t word = 0;
		uint8_t byteCounter = 0;
		for (uint8_t i=0; i<msg->payloadSize; i++)
		{
			// Note that we need to transform between little endian and big endian
			word |= (msg->payload[i] << (byteCounter * 8));
			// counter up byte within words
			byteCounter++;
			// check for word complete
			if (byteCounter >= 4)
			{
				pageBuffer.wordBufferPage[pageBuffer.nOfBytesInBuffer++] = word;

				// reset counter and word
				byteCounter = 0;
				word = 0;

				// check for buffer full
				if (pageBuffer.nOfBytesInBuffer >= PAGE_BUFFER_WORD_SIZE)
				{
					// write to memory
					// Note that the page index must be increased by one because the jump structure/interface resides in page index 0
					if (ILB_HAL_logicWritePage(1+pageBuffer.currentPageIndex++, &pageBuffer.wordBufferPage[0]) != ILB_HAL_OK)
					{
						// an error occured. Terminate
						logicUploadTerminate(LOGIC_UPLOAD_ERROR);
						messaging_respondErrorMessage(msg, ILB_ERROR);
						return;
					}

					// reset
					pageBuffer.nOfBytesInBuffer = 0;
				}
			}
		}

		// count up the number of transferred bytes
		logicBytesTransferred += logicChunkRequested;

		// check for complete
		if (logicBytesTransferred >= sizeOfLogicToUploadBytes)
		{
			// logic transfer done.

			// TODO: checksum

			// check if there is an incomplete page that needs to be written
			while (pageBuffer.nOfBytesInBuffer < PAGE_BUFFER_WORD_SIZE)
			{
				pageBuffer.wordBufferPage[pageBuffer.nOfBytesInBuffer++] = 0x00;
			}
			// write
			// Note that the page index must be increased by one because the jump structure/interface resides in page index 0
			if (ILB_HAL_logicWritePage(1+pageBuffer.currentPageIndex++, &pageBuffer.wordBufferPage[0]) != ILB_HAL_OK)
			{
				// an error occured. Terminate
				logicUploadTerminate(LOGIC_UPLOAD_ERROR);
				messaging_respondErrorMessage(msg, ILB_ERROR);
				return;
			}

			// to activate the logic, write the interface structure
			// prepare the buffer
			memcpy(&pageBuffer.wordBufferPage[0], &interfaceToLoad, sizeof(interfaceToLoad));
			pageBuffer.nOfBytesInBuffer = sizeof(interfaceToLoad);
			// fill rest with zeros
			while (pageBuffer.nOfBytesInBuffer < PAGE_BUFFER_WORD_SIZE)
			{
				pageBuffer.wordBufferPage[pageBuffer.nOfBytesInBuffer++] = 0x00;
			}
			// finally write the interface
			if (ILB_HAL_logicWritePage(0, &pageBuffer.wordBufferPage[0]) != ILB_HAL_OK)
			{
				logicUploadTerminate(LOGIC_UPLOAD_ERROR);
				messaging_respondErrorMessage(msg, ILB_ERROR);
				return;
			}

			// all done, congratulations to your new logic

			// secure the memory
			ILB_HAL_logicSecureLogic();

			// reply that process has finished
			messaging_respondErrorMessage(msg, ILB_OK);

			// mark process as done
			logicUploadTerminate(LOGIC_UPLOAD_DONE);
			return;

		}

		// proceed to next step
		if (sizeOfLogicToUploadBytes - logicBytesTransferred < LOGIC_REQUEST_CHUNK_SIZE)
		{
			logicChunkRequested = sizeOfLogicToUploadBytes - logicBytesTransferred;
		} else {
			logicChunkRequested = LOGIC_REQUEST_CHUNK_SIZE;
		}

		// build up message
		ILBMessageStruct request;
		request.address = ILB_MESSAGE_BROADCAST_ADDRESS;
		request.identifier = ILB_GENERATE_IDENTIFIER;
		request.messageType = ILB_MESSAGE_COMMAND;
		request.payloadSize = 8;
		request.payload[0]= ILB_UPLOAD_LOGIC;
		request.payload[1] = LOGIC_UPLOAD_CMD_GET_LOGIC_BLOCK;
		request.payload[3] = ((logicBytesTransferred >> 24) & 0xFF);							// program offset byte 0
		request.payload[4] = ((logicBytesTransferred >> 16) & 0xFF);							// program offset byte 1
		request.payload[5] = ((logicBytesTransferred >>  8) & 0xFF);							// program offset byte 2
		request.payload[6] = (logicBytesTransferred & 0xFF);									// program offset byte 3
		request.payload[7] = logicChunkRequested;		// number of bytes to send

		// send
		messaging_sendMessage(&request, logicUploadRequestCallback, LOGIC_UPLOAD_TIMEOUT_MS, logicUploadRequestTimeout);

		return;
	}
}

/**
 * Callback to
 */
void logicUploadRequestTimeout(void)
{
	// exit with error
	uploadProgess = LOGIC_UPLOAD_ABORT;
}

/**
 * Get the status of the upload progress
 *
 * @return
 */
LogicUploadStatus_t logicLoading_getStatus(void)
{
	return uploadProgess;
}

/**
 * Centralised exit function for upload procedure
 *
 * Clean up tasks (locking of memory are performed here.
 *
 * @param exitCode
 */
void logicUploadTerminate(LogicUploadStatus_t exitCode)
{
	ILB_HAL_logicSecureLogic();
	uploadProgess = exitCode;
}
