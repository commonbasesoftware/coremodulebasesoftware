/**
 * @file loading.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Functionality related with the loading of logic through the ILB interface
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_LOGIC_LOADING_H_
#define ILB_LOGIC_LOADING_H_

#include "../ilb.h"

/**
 * State of the upload state machine.
 */
typedef enum logicUploadStatusEnum {
	LOGIC_UPLOAD_IDLE,				//!< The upload mechanism is idle
	LOGIC_UPLOAD_DONE,				//!< The process has completed with success.
	LOGIC_UPLOAD_ABORT,				//!< The upload process was aborted
	LOGIC_UPLOAD_ERROR,				//!< An error occured during the upload process
	LOGIC_UPLOAD_GET_BASE_HEADER,	//!< At stage of getting the base header
	LOGIC_UPLOAD_GET_HEADER,		//!< At stage of getting the specific header
	LOGIC_UPLOAD_GET_LOGIC_BLOCK,	//!< At stage of getting a block of logic
} LogicUploadStatus_t;

/**
 * Initialises the uploading mechanism. Prepares the memory (unlocking)
 * @return ::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef logicLoading_init(void);

/**
 * Erases the currently stored logic. Specifically erases the header of the logic, which is equivalent to erasing the complete logic.
 * @return ::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef logicLoading_eraseLogic(void);

/**
 * Starts the upload procedure
 */
void logicLoading_startUpload(void);

/**
 * Initialises the base software with the requested number of slow timers. Memory is allocated for the timers.
 * @param nOfSlowTimers	Number of slow timers needed
 * @return				::ILB_HAL_OK on success
 */
ILB_HAL_StatusTypeDef logicLoading_initBase(uint8_t nOfSlowTimers);

/**
 * Accessor to core interface. Uses the linker file information to get the structure from the memory location
 * @param interface	Found interface.
 * @return			::ILB_HAL_OK if an interface was found, error code otherwise
 */
ILB_HAL_StatusTypeDef logicLoading_getCoreInterface(CoreLogicInterface_t* interface);

/**
 * Get the status of the logic upload state machine
 * @return	Current status.
 */
LogicUploadStatus_t logicLoading_getStatus(void);

#endif /* ILB_LOGIC_LOADING_H_ */
