/*
 * interface.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 *
 * This file contains the interface to the logic
 *
 */

#include "loading.h"
#include "../ilb.h"
#include "../ilbstd.h"
#include "../messaging/messaging.h"
#include "../scheduler/scheduler.h"
#include "../timer/timer.h"
#include "../lightEngine/lightEngine.h"
#include "../telemetry/telemetry.h"

const __interfaceBaseSoftware BaseSoftwareInterface_t baseSoftwareInterface = {
		2,									// version
		logicLoading_initBase,				// initialisation routine
		messaging_sendMessage,				// send a message
		messaging_respondErrorMessage,		// send a response
		scheduler_registerTimerFunction,	// register a timout triggered handler
		timer_registerSlowTimerCallback,	// slow timer (1s) timeout triggered callback register
		ILB_HAL_getTick,					// get the tick from the uC
		ILB_HAL_Delay,						// delay by a certain amount of ms
		ILB_HAL_GenerateRandomNumber,		// get a random number
		ILB_HAL_setIndicator,				// set the indicator on the hardware
		attachedLightEngine_getStatus,		// get status of light engine
		lightEngine_startFade,				// start the fade of the light engine
		ILB_HAL_EEPROMInterface,			// eeprom interface
		telemetry_getTelemetryValue,		// telemetry interface
};

//
//	Older versions of interface
//
/*

Changes:
1: Introduced telemetry interface telemetry_getTelemetryVale

const __interfaceBaseSoftware BaseSoftwareInterface_t baseSoftwareInterface = {
		1,									// version
		logicLoading_initBase,				// initialisation routine
		messaging_sendMessage,				// send a message
		messaging_respondErrorMessage,		// send a response
		scheduler_registerTimerFunction,	// register a timout triggered handler
		timer_registerSlowTimerCallback,	// slow timer (1s) timeout triggered callback register
		ILB_HAL_getTick,					// get the tick from the uC
		ILB_HAL_Delay,						// delay by a certain amount of ms
		ILB_HAL_GenerateRandomNumber,		// get a random number
		ILB_HAL_setIndicator,				// set the indicator on the hardware
		attachedLightEngine_getStatus,		// get status of light engine
		lightEngine_startFade,				// start the fade of the light engine
		ILB_HAL_EEPROMInterface,			// eeprom interface
};

*/
