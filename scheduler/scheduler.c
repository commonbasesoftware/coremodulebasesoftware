/*
 * scheduler.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include "scheduler.h"
#include "../ilb.h"
#include "../messaging/messaging.h"
#include "../settings/settings.h"
#include <stddef.h>
#include "../reset/detection.h"

/*
 * Element in the ILB scheduler. Handler for the message, handler if no response is received in time and timeout.
 */
typedef struct ILBSchedulerElement{
  void (*messageHandler)(ILBMessageStruct*);                                    //!< message handler function
  void (*timeoutHandler)(void);                                                 //!< timout handler function
  uint16_t timeout;                                                              //!< timeout counter
} ILBSchedulerEntryTypeDef;

typedef enum ILBSchedulerTasksStatus {
	ILB_SCHEDULER_TASKS_IDLE,													//!< indicates that no tasks need to be handled
	ILB_SCHEDULER_TASKS_WAITING													//!< indicates that tasks need to be executed
} ILBSchedulerTasksStatusTypeDefEnum;


static ILBSchedulerEntryTypeDef schedulerTable[(ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + ILB_NTIMER_SLOTS];

static ILBSchedulerTasksStatusTypeDefEnum schedulerStatus = ILB_SCHEDULER_TASKS_IDLE;


//
//	Private functions
//
void handleSchedulerTasks(void);

//
//	Start of function implementation block
//

ILB_HAL_StatusTypeDef scheduler_registerHandlerFunction(uint8_t identifier, void (*messageHandler)(ILBMessageStruct*), void (*timeoutHandler)(void), uint16_t timeout)
{
  if ( !identifier || identifier == 0) {
	  // add a time handler
	  // find the next free slot or the slot which contains the same handler
	  uint8_t freeTimerSlot = (ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1;
	  uint8_t handlerAlreadyInList = 0;
	  for (uint8_t i=0; i<ILB_NTIMER_SLOTS; i++) {
		  // check if the handler is already known. In that case we need to overwrite it
		  if (schedulerTable[ILB_MESSAGE_IDENTIFIER_MAX + i].timeoutHandler == timeoutHandler) {
			  handlerAlreadyInList = ILB_MESSAGE_IDENTIFIER_MAX + i;
			  break;
		  }
		  // otherwise mark as the free timer slow
		  if (schedulerTable[(ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + i].timeoutHandler == NULL) {
			  freeTimerSlot = (ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + i;
		  }
	  }
	  // overwrite handler if already in list
	  if (handlerAlreadyInList != 0) {
		  schedulerTable[handlerAlreadyInList].timeoutHandler = timeoutHandler;
		  schedulerTable[handlerAlreadyInList].timeout = timeout/ILB_SCHEDULER_INTERVAL_MS;
		  if (schedulerTable[freeTimerSlot].timeout == 0) {
			  schedulerTable[freeTimerSlot].timeout = 1;
		  }
		  return ILB_HAL_OK;
	  } else {
		  // add entry to free slot
		  schedulerTable[freeTimerSlot].timeoutHandler = timeoutHandler;
		  schedulerTable[freeTimerSlot].timeout = timeout/ILB_SCHEDULER_INTERVAL_MS;
		  if (schedulerTable[freeTimerSlot].timeout == 0) {
			  schedulerTable[freeTimerSlot].timeout = 1;
		  }
		  return ILB_HAL_OK;
	  }
  } else if (identifier >= ILB_MESSAGE_IDENTIFIER_MIN && identifier <= ILB_MESSAGE_IDENTIFIER_MAX) {
	  // add a handler for a message
	  schedulerTable[identifier].messageHandler = messageHandler;
	  schedulerTable[identifier].timeoutHandler = timeoutHandler;
	  if (timeout/ILB_SCHEDULER_INTERVAL_MS == 0 && timeout != 0) {
		  schedulerTable[identifier].timeout = 1;
	  } else {
		  schedulerTable[identifier].timeout = timeout/ILB_SCHEDULER_INTERVAL_MS;
	  }
  } else if (identifier > (ILB_MESSAGE_IDENTIFIER_MAX + ILB_NTIMER_SLOTS)) {
	  // out of range
	  return ILB_HAL_ERROR;
  }
  return ILB_HAL_OK;
}


ILB_HAL_StatusTypeDef scheduler_registerTimerFunction(void (*timeoutHandler)(void), uint16_t timeout)
{
	// passing 0 as identifier ensures that the function recognises the registration of a timer only function
	scheduler_registerHandlerFunction(0, NULL, timeoutHandler, timeout);
	return ILB_HAL_OK;
}


void scheduler_cancelResetWindowTimers(void)
{
	// iterate over the timer slots and erase the timers from the list
	for (uint8_t i=ILB_NTIMER_SLOTS; i<(ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + ILB_NTIMER_SLOTS; i++)
	{
		if (schedulerTable[i].timeoutHandler == resetDetection_windowEntered)
		{
			schedulerTable[i].timeoutHandler = NULL;
			schedulerTable[i].messageHandler = NULL;
			schedulerTable[i].timeout = 0;
			continue;
		}

		if (schedulerTable[i].timeoutHandler == resetDetection_windowExited)
		{
			schedulerTable[i].timeoutHandler = NULL;
			schedulerTable[i].messageHandler = NULL;
			schedulerTable[i].timeout = 0;
			continue;
		}
	}
}


void scheduler_handleTasks(void)
{
	/**
	 * if the idle flag is set, there are no waiting tasks.
	 */
	if (schedulerStatus == ILB_SCHEDULER_TASKS_IDLE) {
		return;
	}
	/*
	 * Reset status
	 */
	schedulerStatus = ILB_SCHEDULER_TASKS_IDLE;

	//
	// ISR for scheduled tasks
	//
	for (uint8_t i=0; i<(ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + ILB_NTIMER_SLOTS; i++) {
		// check if the timer is active
		if (schedulerTable[i].timeout == 0) {
			continue;
		} else if (schedulerTable[i].timeout == 1) {
			// store pointer to function in temporary variable before deleting it
			// this allows the called function to reset itself into the scheduler Table
			void (*timeoutHandler)(void) = schedulerTable[i].timeoutHandler;
			// delete the timeout handler function and normal handler function fromt the list
			schedulerTable[i].timeoutHandler = NULL;
			schedulerTable[i].messageHandler = NULL;
			// deactivate timer
			schedulerTable[i].timeout = 0;
			// call the function
			if (timeoutHandler != NULL) {
				(*timeoutHandler)();
			}
		} else {
			// count down regularly
			schedulerTable[i].timeout--;
		}
	}
}

void scheduler_handleAnswerToMessage(ILBMessageStruct* m)
{
	if(schedulerTable[m->identifier].messageHandler != NULL) {
		// store the target function in temporary variable
		// this order is necessary to allow the called function to re-register itself
		// otherwise it would be deleted, even if it wanted to re-register itself.
		void (*tmpFun)(ILBMessageStruct*) = schedulerTable[m->identifier].messageHandler;
		// remove the entry from the scheduler
		schedulerTable[m->identifier].messageHandler = NULL;
		schedulerTable[m->identifier].timeoutHandler = NULL,
		schedulerTable[m->identifier].timeout = 0;
		// call function
		(*tmpFun)(m);
	}
}

/**
 * Setter function for ILB_HAL interface for the status of the scheduler status
 *
 * This function is best declared inline for speed optimization
 */
inline void lib_setSchedulerStatusWaiting(void)
{
	schedulerStatus = ILB_SCHEDULER_TASKS_WAITING;
}

#ifdef UNIT_TEST
/**
 * Reset the scheduler table. This simulates a power cycle because it clears all elements in the scheduler
 */
void simulation_resetSchedulerTable(void)
{
	schedulerStatus = ILB_SCHEDULER_TASKS_IDLE;
	for (uint8_t i=0; i<(ILB_MESSAGE_IDENTIFIER_MAX - ILB_MESSAGE_IDENTIFIER_MIN) + 1 + ILB_NTIMER_SLOTS; i++)
	{
		schedulerTable[i].messageHandler = NULL;
		schedulerTable[i].timeout = 0;
		schedulerTable[i].timeoutHandler = NULL;
	}
}
#endif
