/**
 * @file scheduler.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Scheduler related functionality, mainly fast system timers and their callback
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_SCHEDULER_SCHEDULER_H_
#define ILB_SCHEDULER_SCHEDULER_H_

#include "../ilb.h"

#define ILB_NTIMER_SLOTS                0x0A                                    //!< number of timer slots and number of timers available.
#define ILB_IDLE_LINE_SCHEDULER_SLOT    0x00                                    //!< slot for the idle line reset callback in the scheduler table
#define ILB_SCHEDULER_INTERVAL_MS       0x05                                    //!< running interval of scheduler in ms

/**
 * Regularly called handler function. This function must be valled at the interval of the timers (currently 5ms). ::ILB_SCHEDULER_INTERVAL_MS must have the same value
 */
void scheduler_executeHandlerFunction(void);

/**
 * Handle pending tasks, such as pending message parsing and sending
 */
void scheduler_handleTasks(void);

/**
 * Registers a handler function with the scheduler logic.
 *
 * Handler functions for replies and the case of a timed out reply are registered. The timeout will be rounded to units of ::ILB_SCHEDULER_INTERVAL_MS.
 * The minimal interval if timeout != 0 is ::ILB_SCHEDULER_INTERVAL_MS. The parameter identifier needs to correspond to a valid message identifier. Upon
 * receival of a message with the same identifer, the handler function is called. A function can be registered to be called after a certain timeout if
 * the identifier parameter is set to NULL or 0. The message Handler parameter will be ignored and can be set to zero as well.
 * If a handler is already in the list, the timeout will be reset or the identifier overwritten.
 *
 * @param identifier		Identifier of reply message. If set to NULL or 0, the messageHandler parameter is ignored and a timer handler created
 * @param messageHandler	Pointer to handler function
 * @param timeoutHandler	Pointer to timeout handler function
 * @param timeout			timeout in ms, infinite if set to 0.
 */
ILB_HAL_StatusTypeDef scheduler_registerHandlerFunction(uint8_t identifier, void (*messageHandler)(ILBMessageStruct*), void (*timeoutHandler)(void), uint16_t timeout);

/**
 * Register a function that is called after a set timeout.
 * @param timeoutHandler	Handler/Function to be called when the time has counted down
 * @param timeout			Time is ms before the function is called. Resolution is ::ILB_SCHEDULER_INTERVAL_MS
 * @return
 */
ILB_HAL_StatusTypeDef scheduler_registerTimerFunction(void (*timeoutHandler)(void), uint16_t timeout);

/**
 * Interface window to cancel any eventually running reset window timers
 */
void scheduler_cancelResetWindowTimers(void);

/**
 * Sub handler to quickly access the handler for a specific message. If a matching identifier is found in the list of pending
 * handlers, it will execute. Otherwise no action is taken. The handler is cleared from the list
 * @param m	Acknowledge type message to which to look for a handler.
 */
void scheduler_handleAnswerToMessage(ILBMessageStruct* m);

#ifdef UNIT_TEST
void simulation_resetSchedulerTable(void);
#endif

#endif /* ILB_SCHEDULER_SCHEDULER_H_ */
