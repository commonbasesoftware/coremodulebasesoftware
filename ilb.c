/**
 * ilb.c
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief This file contains the main functions of the ILB library. It acts as a
 * hub to connect the functionality.
 *
 * @ingroup BaseSoftwareGroup
 *
 */

#include "ilb.h"
#include "baseSoftware/state.h"
#include "hardwareAPI/hardwareAPI.h"
#include "messaging/messaging.h"
#include "timer/timer.h"
#include "lightEngine/lightEngine.h"
#include "lightEngine/settings.h"
#include "reset/handling.h"
#include "scheduler/scheduler.h"
#include "logic/loading.h"
#include "indicator/indicatorPattern.h"
#include "register/register.h"
#include <stddef.h>

#ifndef LIBRARY_LOGIC
// dummy logic in case no logic was loaded
static const uint8_t emptyLogic(void) { return 0xFF; }
#endif

//
//	Private variables
//
static ILBBaseStatusTypeDef base_status = ILB_BASE_INITIALISING;				// state of the base software
#ifndef LIBRARY_LOGIC
static ILBBaseStatusTypeDef previousBaseStatus = ILB_BASE_INITIALISING;			// state to revert back to when a loading command is issued
#endif
#ifndef LIBRARY_LOGIC
static logicRoutine_t pLogicRoutine = emptyLogic;								// initialise to empty function to avoid null pointer issues
static CoreLogicInterface_t interface_coreLogic;
static uint8_t logicStatus = 0xFF;
#endif

//
//	Interface to external logic
//
#ifdef LIBRARY_LOGIC
extern uint8_t lib_logic(void);
#endif

//
//	SECTION: Function implementation
//

/**
 * Main logic routine
 *
 * This routine is called in the main event loop by the calling program
 */
void ilbLogic(void)
{

	// main switch
	switch (base_status)
	{
	// library has not been initialised. Handle all the initialisation tasks
	case ILB_BASE_INITIALISING:

		// check/set the chip level protection
#ifndef SKIP_CHIP_PROTECTION
		if (ILB_HAL_CheckChipProtection(ILB_TARGET_RDP_LEVEL);
#endif

		// start hardware
		if(ILB_HAL_hardwareInit() != ILB_HAL_OK)
		{
			base_status = ILB_BASE_ERROR;
			break;
		}

		// prepare the messaging mechanisms
		messaging_init();

		// prepare timer mechanisms
		timer_init();

		// check if we need to do tasks associated with reset flags
		resetPattern_handle();

// the following block is only required if we operate as a base device that can output light
#ifndef LIBRARY_LOGIC
		// load default settings
		uint8_t followerMembership = lightEngine_init();

		// decide what to do next.
		// If we are part of a follower group but no logic is found, we need to react to follower commands
		// If we have a logic, the logic will handle commands
		if (logicLoading_init() == ILB_HAL_OK)
		{
			// logic found. We need to operate off the logic
			if (logicLoading_getCoreInterface(&interface_coreLogic) != ILB_HAL_OK)
			{
				ilb_setState(ILB_BASE_ERROR);
				break;
			}
			// initialise the logic, get the number of slow timers that need to be created
			uint8_t nOfSlowTimers = 0;
			interface_coreLogic.init(&nOfSlowTimers);
			timer_createSlowTimers(nOfSlowTimers);
			// set the class
			if(addressing_init(ILB_CORE_CLASS, interface_coreLogic.commandHandler, interface_coreLogic.readHandler, interface_coreLogic.writeHandler) != ILB_HAL_OK)
			{
				// the device class is not supported
				ilb_setState(ILB_BASE_ERROR);
				break;
			}

			// store pointer to main logic that it can be executed in the main loop
			pLogicRoutine = interface_coreLogic.logic;

			// change state
			ilb_setState(ILB_BASE_LOGIC);
			// exit from case
			break;
		}

		// next, we need to decide if we should continue in on/off, follower or driver peripheral mode

		// try to initialise as a driver peripheral
		switch(addressing_init(ILB_PERIPHERAL_DRIVER_CLASS, NULL, NULL, NULL))
		{
		case ILB_HAL_OK:
			// initialisation is success
			ilb_setState(ILB_BASE_DRIVER_PERIPHERAL);
			break;
		case ILB_HAL_INVALID:
			// no address found. Is there a follower group? If so enter follower mode
			if (followerMembership != FOLLOWER_GROUP_NONE)
			{
				ilb_setState(ILB_BASE_FOLLOWER);
				break;
			} else {
				//  on/off
				ilb_setState(ILB_BASE_ON_OFF);
				break;
			}
		default:
			// error occured
			ilb_setState(ILB_BASE_ERROR);
			break;

		}
#endif
#ifdef LIBRARY_LOGIC
		// initialise addressing. We can use dummy values
		addressing_init(0, NULL, NULL, NULL);

		// enter state using external logic
		ilb_setState(ILB_BASE_LIBRARY_LOGIC);
#endif

		break;

#ifndef LIBRARY_LOGIC
	// handle tasks associated with on off functionality
	case ILB_BASE_ON_OFF:
		break;

	// handle follower case
	case ILB_BASE_FOLLOWER:
		break;

	// handle driver peripheral case
	case ILB_BASE_DRIVER_PERIPHERAL:
		break;

	// handle tasks when running a logic
	case ILB_BASE_LOGIC:
		// execute logic
		logicStatus = pLogicRoutine();
		break;

	// we are in the upload procedure for settings. Monitor the state
	case ILB_BASE_SETTINGS_LOADING:
		if (lightEngineSettings_getStatus() == LIGHT_ENGINE_SETTINGS_LOADING_DONE)
		{
			// reboot the system
			ilb_rebootSystem(RESET_DELAY_MS);
		}
		if (lightEngineSettings_getStatus() == LIGHT_ENGINE_SETTINGS_LOADING_ABORT)
		{
			// revert back to previous state
			ilb_setState(previousBaseStatus);
			break;
		}
		if (lightEngineSettings_getStatus() == LIGHT_ENGINE_SETTINGS_LOADING_ERROR)
		{
			// revert back to previous state
			ilb_setState(previousBaseStatus);
			break;
		}
		break;

	// we are in the upload procedure for logic. Monitor the state
	case ILB_BASE_LOGIC_LOADING:
		if (logicLoading_getStatus() == LOGIC_UPLOAD_DONE)
		{
			// upload done. We need to reboot
			ilb_rebootSystem(RESET_DELAY_MS);
		}
		if (logicLoading_getStatus() == LOGIC_UPLOAD_ABORT)
		{
			// revert back to previous state
			ilb_setState(previousBaseStatus);
			break;
		}
		if (logicLoading_getStatus() == LOGIC_UPLOAD_ERROR)
		{
			// revert back to previous state
			ilb_setState(previousBaseStatus);
			break;
		}
		break;
#endif

	case ILB_BASE_ERROR:
		break;

	case ILB_BASE_WAIT_FOR_REBOOT:
		break;

	case ILB_BASE_TEST:
		break;

#ifdef LIBRARY_LOGIC
	case ILB_BASE_LIBRARY_LOGIC:
	{
		register_udpateSpecificStatusRegister(lib_logic());
	}
		break;
#endif


	}

	// all common tasks

	// general ILB tasks
	/*
	 * Begin normal houskeeping routines
	 */

	// any pending tasks
	scheduler_handleTasks();

	// handle reived messages waiting to be handled
	messaging_handleWaitingMessages();

	// send messages waiting to be send
	messaging_sendWaitingMessages();

	// handle slow timer. This also includes telemetry
	timer_handle();

}

/**
 * Setter function for state of base software
 *
 * Signaling and tracking for states for special situations is handled here
 * @param newState
 */
void ilb_setState(ILBBaseStatusTypeDef newState)
{
	switch (newState)
	{
	case ILB_BASE_INITIALISING:
		break;

	case ILB_BASE_ERROR:
#ifndef LIBRARY_LOGIC
		// go to maximal output
		lightEngine_setLevel(0xFF, 0, INTENSITY_LOOKUP_LINEAR);
		ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_GREEN, BLINK_PATTERN_ERROR_RED);
#endif
		break;

#ifndef LIBRARY_LOGIC
	case ILB_BASE_DRIVER_PERIPHERAL:
		// go to default output
		lightEngine_setLevel(0xFF, 0, INTENSITY_LOOKUP_LINEAR);
		ILB_HAL_setIndicator(DEVICE_GREEN_ONESHOT | DEVICE_RED_ONESHOT | DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_DRV_PERIPHERAL_GREEN, BLINK_PATTERN_DRV_PERIPHERAL_RED);
		break;

	case ILB_BASE_FOLLOWER:
		// go to default output
		lightEngine_setLevel(0xFF, 0, INTENSITY_LOOKUP_LINEAR);
		ILB_HAL_setIndicator(DEVICE_GREEN_ONESHOT | DEVICE_RED_ONESHOT | DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_FOLLOWER_GREEN, BLINK_PATTERN_FOLLOWER_RED);
		break;

	case ILB_BASE_LOGIC:
		ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, 0x00, 0x00);
		break;

	case ILB_BASE_ON_OFF:
		// go to default output
		lightEngine_setLevel(0xFF, 0, INTENSITY_LOOKUP_LINEAR);
		ILB_HAL_setIndicator(DEVICE_GREEN_ONESHOT | DEVICE_RED_ONESHOT | DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ON_OFF_GREEN, BLINK_PATTERN_ON_OFF_RED);
		break;

	case ILB_BASE_SETTINGS_LOADING:
		// store current state to be able to revert back to
		previousBaseStatus = base_status;
		ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_LOAD_DRIVER_SETTINGS_GREEN, BLINK_PATTERN_LOAD_DRIVER_SETTINGS_RED);
		break;

	case ILB_BASE_LOGIC_LOADING:
		// store current state to be able to revert back to
		previousBaseStatus = base_status;
		ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_LOAD_DRIVER_SETTINGS_GREEN, BLINK_PATTERN_LOAD_DRIVER_SETTINGS_RED);
		break;
#endif

	default:
		break;
	}

	base_status = newState;
}

/**
 * Callback to be able to execute the reboot time delayed. That allows to finish tasks
 */
void system_executeReboot(void)
{
	// reboot the system
	ILB_HAL_systemReset();
}

/**
 * Execute a reboot after a timeout to be able to complete tasks
 * @param delayMS
 */
void ilb_rebootSystem(uint16_t delayMS)
{
	// set the status.
	// This step is important to avoid execution of logic that may have been erased!
	ilb_setState(ILB_BASE_WAIT_FOR_REBOOT);
	// schedule the event
	scheduler_registerTimerFunction(system_executeReboot, delayMS);
}

/**
 * Accessor to state of system
 *
 * @return
 */
ILBBaseStatusTypeDef ilb_getState(void)
{
	return base_status;
}

#ifndef LIBRARY_LOGIC
uint8_t ilb_getLogicState(void)
{
	return logicStatus;
}
#endif
