/**
 * timer.c
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Implementation of timer related functionality
 *
 * @ingroup BaseSoftwareGroup
 */

#include "../ilb.h"
#include "../hardwareAPI/hardwareAPI.h"
#include "../telemetry/telemetry.h"
#include <stdlib.h>

typedef enum TimerStatus {
	TIMER_IDLE,
	TIMER_ARMED
} TimerStatus_t;

static TimerStatus_t SlowTimerStatus = TIMER_IDLE;			// status of the timer

/**
 * Represents a slow (1s resolution) timer
 */
typedef struct slowTimerStruct {
	uint16_t timeout;			//!< Timeout counter. Triggered if reaching 1, disabled when reaching zero
	callbackHandle_t callback;	//!< callback to call when the timeout is reached
} SlowTimer_t;

// list of slow timer
static uint8_t nOfExistingSlowTimer = 0;		//!< number of slow timers
static SlowTimer_t* slowTimerList;				//!< points to the list of slow timers

/**
 * Initialisation routine for timer
 *
 * This routine does not initialise any slow timers requested by the logic
 */
void timer_init(void)
{
#ifndef LIBRARY_LOGIC
	telemetry_init();
#endif
}

/**
 * Called by main routine to handle all timer related tasks
 */
void timer_handle(void)
{
	// check if we need to handle tasks
	if (SlowTimerStatus != TIMER_ARMED)
	{
		return;
	}

	// count down the timer and execute task if expired
	for (uint8_t i=0; i<nOfExistingSlowTimer; i++)
	{
		if (slowTimerList[i].timeout == 1)
		{
			if (slowTimerList[i].timeout == 0)
			{
				continue;
			}
			// execute handler. Store first and then delete from list, then execute. This allows for re-triggering in routine
			callbackHandle_t callback = slowTimerList[i].callback;
			slowTimerList[i].timeout = 0;
			slowTimerList[i].callback = NULL;
			if (callback != NULL)
			{
				callback();
				continue;
			}
		} else if (slowTimerList[i].timeout > 1)
		{
			slowTimerList[i].timeout--;
		}
	}

	// handle telemetry tasks
#ifndef LIBRARY_LOGIC
	telemetry_tick();
#endif

	// mark timer as done
	SlowTimerStatus = TIMER_IDLE;
}

/**
 * Called by hardware layer to arm the 1 second timer (in 1 second interval)
 */
void lib_arm1sTimer(void)
{
	SlowTimerStatus = TIMER_ARMED;
}

/**
 * Routine to create slow timer. Called when logic is loaded
 *
 * @param nOfSlowTimer Number of timers to allocate memory for
 * @return	ILB_HAL_OK on success, error code otherwise.
 */
ILB_HAL_StatusTypeDef timer_createSlowTimers(uint8_t nOfSlowTimer)
{
	slowTimerList = (SlowTimer_t*)malloc(nOfSlowTimer * sizeof(*slowTimerList));
	nOfExistingSlowTimer = nOfSlowTimer;
	for (uint8_t i=0; i<nOfSlowTimer; i++)
	{
		slowTimerList[i].timeout = 0;
		slowTimerList[i].callback = NULL;
	}
	return ILB_HAL_OK;
}

/**
 * Register a callback with the slow timer
 *
 * @param callback	Callback to be called when timer expires
 * @param timeout	Timeout of timer in seconds
 */
ILB_HAL_StatusTypeDef timer_registerSlowTimerCallback(uint8_t index, callbackHandle_t callback, uint16_t timeout)
{
	if (index >= nOfExistingSlowTimer)
	{
		return ILB_HAL_INVALID;
	}
	slowTimerList[index].callback = callback;
	slowTimerList[index].timeout = timeout;
	return ILB_HAL_OK;;
}
