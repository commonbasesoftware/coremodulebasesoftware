/**
 * @file timer.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Definition of timer realted functions
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_TIMER_TIMER_H_
#define ILB_TIMER_TIMER_H_

#include "../ilb.h"

/**
 * Initializes timer related functionality, variables and memories
 */
void timer_init(void);

/**
 * Periodically called handle to execute timer. This function must be called at the interfavl of the timer
 */
void timer_handle(void);

/**
 * Allocates memory for the given number of slow timers
 *
 * @param nOfSlowTimer	Number of slow timers to initialize
 * @return	::ILB_HAL_OK on success, errorcode otherwise
 */
ILB_HAL_StatusTypeDef timer_createSlowTimers(uint8_t nOfSlowTimer);

/**
 * Registers a callback with a slow timer. Starts the timer if it is not running, resets it if already running.
 *
 * @param index Index of timer to use
 * @param callback	Callback to call upon timer timeout
 * @param timeout	Duration of timer in seconds. The timer is disabled if set to zero
 * @return	::ILB_HAL_OK on success, error code otherwise.
 */
ILB_HAL_StatusTypeDef timer_registerSlowTimerCallback(uint8_t index, callbackHandle_t callback, uint16_t timeout);

#endif /* ILB_TIMER_TIMER_H_ */
