/*
 * detection.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 */

#include "../settings/settings.h"
#include "../hardwareAPI/hardwareAPI.h"


void resetDetection_windowEntered(void)
{
	ILB_HAL_addPowerCycleFlag();
}

void resetDetection_windowExited(void)
{
	ILB_HAL_clearPowerCycleFlags();
}

void resetPattern_handle(void)
{
	// check flags
	if (ILB_HAL_getNumberOfPowerCycleFlags() >= RESET_CYCLE_FLAG_COUNT)
	{
		// TODO: perform reset
	}
}
