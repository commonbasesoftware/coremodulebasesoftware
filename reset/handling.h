/**
 * @file handling.h
 *
 * @date Sep 15, 2021
 * @author A.Niggebaum
 *
 * @brief Execution of reset routines
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_RESET_HANDLING_H_
#define ILB_RESET_HANDLING_H_

/**
 * Executed when the reset pattern matches and a reset is required.
 */
void resetPattern_handle(void);

#endif /* ILB_RESET_HANDLING_H_ */
