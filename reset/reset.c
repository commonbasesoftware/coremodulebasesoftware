/*
 * reset.c
 *
 *  Created on: 15.09.2021
 *      Author: A.Niggebaum
 *
 * Resest should not be executed immediately. We need time to send
 * The acknowledge and eventually display a pattern on the indicator.
 * Hence wait a bit before the actual action is executed
 *
 */

#include "../baseSoftware/state.h"
#include "../scheduler/scheduler.h"
#include "../settings/settings.h"
#include "../indicator/indicatorPattern.h"
#include "../hardwareAPI/hardwareAPI.h"
#include <stddef.h>

//
//	Inteface when compiled for external library use
//
#ifdef LIBRARY_LOGIC
extern void lib_executeFactoryReset(void);
extern void lib_executeSettingsReset(void);
#endif

//
//	Function prototpyes
//
static void (*taskToExecute)(void) = NULL;		// task we want to execute after timeout

//
//	Tasks to handle
//
static void executePowerCycle(void)
{
	ILB_HAL_systemReset();
}

/**
 * Trigger a power cycle
 */
void reset_triggerPowerCycle(void)
{
	// emit blink pattern
	ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_EXECUTE_RESET_GREEN, BLINK_PATTERN_EXECUTE_RESET_RED);

	taskToExecute = executePowerCycle;
	scheduler_registerTimerFunction(taskToExecute, RESET_DELAY_MS);
}

/**
 * Trigger a settings reset
 */
void reset_triggerSettingsReset(void)
{
	// emit blink pattern
	ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_EXECUTE_SETTINGS_RESET_GREEN, BLINK_PATTERN_EXECUTE_SETTINGS_RESET_RED);

#ifdef LIBRARY_LOGIC
	lib_executeSettingsReset();
#endif

	taskToExecute = executePowerCycle;
	scheduler_registerTimerFunction(taskToExecute, RESET_DELAY_MS);
}

/**
 * Trigger a factory reset
 */
void reset_triggerFactoryReset(void)
{
	// emit blink pattern
	ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_EXECUTE_FACTORY_RESET_GREEN, BLINK_PATTERN_EXECUTE_FACTORY_RESET_RED);


	// check if we are a driver peripheral or a core
	switch (ilb_getState())
	{
#ifndef LIBRARY_LOGIC
	// we are operating as a core. Erase the eeprom the logic has access to
	case ILB_BASE_LOGIC:
		ILB_HAL_EEPROMErase();
		break;


	// we are operating as a driver peripheral, means we have an address. Erase to reset
	case ILB_BASE_DRIVER_PERIPHERAL:
	{
		uint8_t tmp = 0;
		ILB_HAL_storeAddress(&tmp);
	}
		break;
#else
	case ILB_BASE_LIBRARY_LOGIC:
		lib_executeFactoryReset();
		{
			uint8_t tmp = 0;
			ILB_HAL_storeAddress(&tmp);
		}
		break;
#endif
	// erase address
	default:
	{
		uint8_t tmp = 0;
		ILB_HAL_storeAddress(&tmp);
	}
		break;
	}


	// power cycle
	taskToExecute = executePowerCycle;
	scheduler_registerTimerFunction(taskToExecute, RESET_DELAY_MS);
}
