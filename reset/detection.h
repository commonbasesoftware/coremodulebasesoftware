/**
 * @file detection.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief Routines to manage reset window detection
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_RESET_DETECTION_H_
#define ILB_RESET_DETECTION_H_

/**
 * Must be called when a reset window is entered. Sets the appropriate flags in the persistent memory
 */
void resetDetection_windowEntered(void);

/**
 * Must be called when the reset window is exited. Clears the reset flags from persistent memory
 */
void resetDetection_windowExited(void);

#endif /* ILB_RESET_DETECTION_H_ */
