/**
 * @file reset.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_RESET_RESET_H_
#define ILB_RESET_RESET_H_

/**
 * Trigger a power cycle of the device
 */
void reset_triggerPowerCycle(void);

/**
 * Trigger a settings reset cycle, followed by a power cycle
 */
void reset_triggerSettingsReset(void);

/**
 * Trigger a factory reset cycle.
 */
void reset_triggerFactoryReset(void);

#endif /* ILB_RESET_RESET_H_ */
