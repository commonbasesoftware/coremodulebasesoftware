/**
 * @file register.h
 *
 * @date Sep 15, 2021
 * @author A.Niggebaum
 *
 * @brief Handling of registers
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_REGISTER_REGISTER_H_
#define ILB_REGISTER_REGISTER_H_

#include "../ilb.h"

/**
 * Initialise the register
 */
void register_init(void);

/**
 * Push newest PWM information to appropriate register
 * @param settings
 */
void register_updatePWMInformation(LightEngineSettings_t* settings);

/**
 * Handle a write to register
 * @param m
 * @return
 */
ILB_HAL_StatusTypeDef register_handleWrite(ILBMessageStruct* m);

/**
 * Handle a read from registers
 * @param m
 * @return
 */
ILB_HAL_StatusTypeDef register_handleRead(ILBMessageStruct* m);

/**
 * Update the measurement register
 * @param converted
 * @param raw
 */
void register_updateMeasurement(ADCReadings_t* converted, ADCReadings_t* raw);

/**
 * Updat the interface register
 * @param pBaseInterface
 * @param pCoreInterface
 */
void register_updateInterfaceRegister(BaseSoftwareInterface_t* pBaseInterface, CoreLogicInterface_t* pCoreInterface);

/**
 * Updates the specific status flag for the device. Only the lower 6 bits are used
 * @param status
 */
void register_udpateSpecificStatusRegister(uint8_t status);

#endif /* ILB_REGISTER_REGISTER_H_ */
