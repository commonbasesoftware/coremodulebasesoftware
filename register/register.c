/*
 * register.c
 *
 *  Created on: Sep 15, 2021
 *      Author: A.Niggebaum
 */

#include "../messaging/messaging.h"
#include "../version/version.h"
#include "../telemetry/telemetry.h"
#include "../baseSoftware/state.h"
#include <string.h>
#include <stdlib.h>

enum basicRegisterEnum {
	REGISTER_INFORMATION,
	REGISTER_STATUS,
#ifndef LIBRARY_LOGIC
	REGISTER_MEASUREMENT,
	REGISTER_LOGIC,
#endif
	REGISTER_MAX
};

enum informationRegisterEnum {
	INFORMATION_UID_1,
	INFORMATION_UID_2,
	INFORMATION_UID_3,
	INFORMATION_UID_4,
	INFORMATION_ILB_BASE_MAJOR,
	INFORMATION_ILB_BASE_MINOR,
	INFORMATION_ILB_BASE_BUILD,
#ifndef LIBRARY_LOGIC
	INFORMATION_PWM_LIMIT_A,
	INFORMATION_PWM_LIMIT_B,
	INFORMATION_PWM_CHANNEL_MAP,
	INFORMATION_PWM_FOLLOWER_GROUP,
	INFORMATION_PWM_MIRED_MIN_MSB,
	INFORMATION_PWM_MIRED_MIN_LSB,
	INFORMATION_PWM_MIRED_MAX_MSB,
	INFORMATION_PWM_MIRED_MAX_LSB,
	INFORMATION_PWM_MIRED_STEP,
	INFORMATION_ON_TIME_COUNTER_3,
	INFORMATION_ON_TIME_COUNTER_2,
	INFORMATION_ON_TIME_COUNTER_1,
	INFORMATION_ON_TIME_COUNTER_0,
	INFORMATION_BURN_TIME_COUNTER_3,
	INFORMATION_BURN_TIME_COUNTER_2,
	INFORMATION_BURN_TIME_COUNTER_1,
	INFORMATION_BURN_TIME_COUNTER_0,
#endif
	INFORMATION_MAX,
};

enum statusRegisterEnum {
	STATUS_TRACKER,
	STATUS_STATE,
#ifndef LIBRARY_LOGIC
	STATUS_BSW,
	STATUS_LOGIC,
#endif
	STATUS_RDP,
	STATUS_MAX
};

#ifndef LIBRARY_LOGIC
enum measurementRegisterEnum {
	MEASUREMENT_I1_MSB,
	MEASUREMENT_I1_LSB,
	MEASUREMENT_U1_MSB,
	MEASUREMENT_U1_LSB,
	MEASUREMENT_I2_MSB,
	MEASUREMENT_I2_LSB,
	MEASUREMENT_U2_MSB,
	MEASUREMENT_U2_LSB,
	MEASUREMENT_TEMP_MSB,
	MEASUREMENT_TEMP_LSB,
	MEASUREMENT_I1_RAW_MSB,
	MEASUREMENT_I1_RAW_LSB,
	MEASUREMENT_U1_RAW_MSB,
	MEASUREMENT_U1_RAW_LSB,
	MEASUREMENT_I2_RAW_MSB,
	MEASUREMENT_I2_RAW_LSB,
	MEASUREMENT_U2_RAW_MSB,
	MEASUREMENT_U2_RAW_LSB,
	MEASUREMENT_TEMP_RAW_MSB,
	MEASUREMENT_TEMP_RAW_LSB,
	MEASUREMENT_MAX,
};

enum logicRegisterEnum {
	LOGIC_BASE_SOFTWARE_INTERFACE_VERSION,
	LOGIC_INTERFACE_VERSION,
	LOGIC_GTIN_0,
	LOGIC_GTIN_1,
	LOGIC_GTIN_2,
	LOGIC_GTIN_3,
	LOGIC_GTIN_4,
	LOGIC_GTIN_5,
	LOGIC_MAJOR_VERSION,
	LOGIC_MINOR_VERSION,
	LOGIC_MAX
};
#endif

// arrays with information
static uint8_t informationRegister[INFORMATION_MAX] = {0};
static uint8_t statusRegister[STATUS_MAX] = {0};
#ifndef LIBRARY_LOGIC
static uint8_t measurementRegister[MEASUREMENT_MAX] = {0};
static uint8_t logicRegister[LOGIC_MAX] = {0};


//
//	Function protoypes
//
static void updateTelemetryRegister(void);
#endif

/**
 * Initialisation function for registers
 */
void register_init(void)
{
	// fill the UID from the hardware
	uint32_t UID = ILB_HAL_getUID();

	// build up the information register
	informationRegister[INFORMATION_UID_1] = ((UID >> 24) & 0xFF);
	informationRegister[INFORMATION_UID_2] = ((UID >> 16) & 0xFF);
	informationRegister[INFORMATION_UID_3] = ((UID >> 8) & 0xFF);
	informationRegister[INFORMATION_UID_4] =  (UID & 0xFF);
	informationRegister[INFORMATION_ILB_BASE_MAJOR] = ILB_BASE_SW_VERSION_MAJOR;
	informationRegister[INFORMATION_ILB_BASE_MINOR] = ILB_BASE_SW_VERSION_MINOR;
	informationRegister[INFORMATION_ILB_BASE_BUILD] = ILB_BASE_SW_VERSION_BUILD;

	// push the read out protection status to the register
	statusRegister[STATUS_RDP] = ILB_HAL_GetChipProtection();

}

/**
 * Reset the register. This function is intended to be used primarily for simulation purposes and does therefore not appear in any headers.
 */
void register_reset(void)
{
	// clear out the content
	for(uint32_t i=0; i<INFORMATION_MAX; i++)
	{
		informationRegister[i] = 0;
	}
	for (uint32_t i=0; i<STATUS_MAX; i++)
	{
		statusRegister[i] = 0;
	}
#ifndef LIBRARY_LOGIC
	for (uint32_t i=0; i<MEASUREMENT_MAX; i++)
	{
		measurementRegister[i] = 0;
	}
#endif
	// re-initialise
	register_init();
}

#ifndef LIBRARY_LOGIC

void register_updateInterfaceRegister(BaseSoftwareInterface_t* pBaseInterface, CoreLogicInterface_t* pCoreInterface)
{
	logicRegister[LOGIC_BASE_SOFTWARE_INTERFACE_VERSION] = pBaseInterface->version;
	logicRegister[LOGIC_INTERFACE_VERSION]				 = pCoreInterface->version;
	logicRegister[LOGIC_GTIN_0]							 = pCoreInterface->GTIN[0];
	logicRegister[LOGIC_GTIN_1]							 = pCoreInterface->GTIN[1];
	logicRegister[LOGIC_GTIN_2]							 = pCoreInterface->GTIN[2];
	logicRegister[LOGIC_GTIN_3]							 = pCoreInterface->GTIN[3];
	logicRegister[LOGIC_GTIN_4]							 = pCoreInterface->GTIN[4];
	logicRegister[LOGIC_GTIN_5]							 = pCoreInterface->GTIN[5];
	logicRegister[LOGIC_MAJOR_VERSION]					 = pCoreInterface->versionMajor;
	logicRegister[LOGIC_MINOR_VERSION]					 = pCoreInterface->versionMinor;
}


void register_updatePWMInformation(LightEngineSettings_t* settings)
{
	informationRegister[INFORMATION_PWM_LIMIT_A] 		= settings->Limit_CHA;
	informationRegister[INFORMATION_PWM_LIMIT_B] 		= settings->Limit_CHB;
	informationRegister[INFORMATION_PWM_CHANNEL_MAP] 	= settings->channelMapping;
	informationRegister[INFORMATION_PWM_FOLLOWER_GROUP] = settings->followerGroup;
	informationRegister[INFORMATION_PWM_MIRED_MIN_MSB] 	= ((settings->miredMin >> 8) & 0xFF);
	informationRegister[INFORMATION_PWM_MIRED_MIN_LSB] 	= (settings->miredMin & 0xFF);
	informationRegister[INFORMATION_PWM_MIRED_MAX_MSB] 	= ((settings->miredMax >> 8) & 0xFF);
	informationRegister[INFORMATION_PWM_MIRED_MAX_LSB]	= (settings->miredMax & 0xFF);
	informationRegister[INFORMATION_PWM_MIRED_STEP]		= settings->miredStep;
}


void register_updateMeasurement(ADCReadings_t* pConverted, ADCReadings_t* pRaw)
{
	// converted values
	measurementRegister[MEASUREMENT_I1_MSB] = ((pConverted->I1 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_I1_LSB] =  (pConverted->I1 & 0xFF);
	measurementRegister[MEASUREMENT_U1_MSB] = ((pConverted->U1 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_U1_LSB] =  (pConverted->U1 & 0xFF);
	measurementRegister[MEASUREMENT_I2_MSB] = ((pConverted->I2 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_I2_LSB] =  (pConverted->I2 & 0xFF);
	measurementRegister[MEASUREMENT_U2_MSB] = ((pConverted->U2 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_U2_LSB] =  (pConverted->U2 & 0xFF);
	measurementRegister[MEASUREMENT_TEMP_MSB] = ((pConverted->T >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_TEMP_LSB] = (pConverted->T & 0xFF);

	// raw readings
	measurementRegister[MEASUREMENT_I1_RAW_MSB] = ((pRaw->I1 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_I1_RAW_LSB] =  (pRaw->I1 & 0xFF);
	measurementRegister[MEASUREMENT_U1_RAW_MSB] = ((pRaw->U1 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_U1_RAW_LSB] =  (pRaw->U1 & 0xFF);
	measurementRegister[MEASUREMENT_I2_RAW_MSB] = ((pRaw->I2 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_I2_RAW_LSB] =  (pRaw->I2 & 0xFF);
	measurementRegister[MEASUREMENT_U2_RAW_MSB] = ((pRaw->U2 >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_U2_RAW_LSB] =  (pRaw->U2 & 0xFF);
	measurementRegister[MEASUREMENT_TEMP_RAW_MSB] = ((pRaw->T >> 8) & 0xFF);
	measurementRegister[MEASUREMENT_TEMP_RAW_LSB] =  (pRaw->T & 0xFF);

}
#endif // LIBRARY_LOGIC

ILB_HAL_StatusTypeDef register_handleWrite(ILBMessageStruct* m)
{
	if (m->payloadSize < 3 || m->payload[0] >= REGISTER_MAX)
	{
		return ILB_HAL_ERROR;
	}

	switch (m->payload[0])
	{
	case REGISTER_INFORMATION:
		// not allowed to write
		messaging_respondErrorMessage(m, ILB_ERROR_NOT_ALLOWED);
		// signal we have handled the message
		return ILB_HAL_OK;

	case REGISTER_STATUS:
		// we are only allowed to write in the status register
		if (m->payload[1] == STATUS_TRACKER &&
			m->payloadSize == 3)
		{
			// write value
			statusRegister[STATUS_TRACKER] = m->payload[2];
			// respond ok
			messaging_respondErrorMessage(m, ILB_OK);
			// notify that we have handled the message
			return ILB_HAL_OK;
		}
		// invalid
		messaging_respondErrorMessage(m, ILB_ERROR_NOT_ALLOWED);
		// notify that we have handled the message
		return ILB_HAL_OK;

	default:
		return ILB_HAL_ERROR;
	}
}


ILB_HAL_StatusTypeDef register_handleRead(ILBMessageStruct* m)
{

	// range check and primary register selection.
	// If the syntax is different or we are not responsible for the requested register, forward.
	if (m->payloadSize != 3 || m->payload[0] >= REGISTER_MAX)
	{
		return ILB_HAL_ERROR;
	}

	// extract start stop
	uint8_t startRegister = m->payload[1];
	uint8_t nOfRegister = m->payload[2];

	// check valid lenght of read
	if (nOfRegister == 0)
	{
		messaging_respondErrorMessage(m, ILB_ERROR_INVALID);
		return ILB_HAL_OK;
	}

	// create object for answer (the +2 is to deal just with a single register and that we dont need to bother with ifs)
	uint8_t* payload;
	payload = malloc(sizeof(*payload) * (nOfRegister + 2));

	switch (m->payload[0])
	{
	case REGISTER_INFORMATION:
		if (startRegister + nOfRegister > INFORMATION_MAX)
		{
			// send out of bounds as answer
			messaging_respondErrorMessage(m, ILB_ERROR_OUT_OF_BOUNDS);
			// signal that we have handled the message
			return ILB_HAL_OK;
		}
#ifndef LIBRARY_LOGIC
		// update telemetry register
		updateTelemetryRegister();
#endif
		// fill the answer
		memcpy(&payload[0], &informationRegister[startRegister], nOfRegister);
		break;

	case REGISTER_STATUS:
		if (startRegister + nOfRegister > STATUS_MAX)
		{
			// send out of bounds error
			messaging_respondErrorMessage(m, ILB_ERROR_OUT_OF_BOUNDS);
			// return we have handled the message
			return ILB_HAL_OK;
		}
#ifdef LIBRARY_LOGIC
		// fill answer
		memcpy(&payload[0], &statusRegister[startRegister], nOfRegister);
		break;
#else
		// update status
		statusRegister[STATUS_BSW] = ilb_getState();
		statusRegister[STATUS_LOGIC] = ilb_getLogicState();
		// fill answer
		memcpy(&payload[0], &statusRegister[startRegister], nOfRegister);
		break;

	case REGISTER_MEASUREMENT:
		if (startRegister + nOfRegister > MEASUREMENT_MAX)
		{
			// send out of bounds error
			messaging_respondErrorMessage(m, ILB_ERROR_OUT_OF_BOUNDS);
			return ILB_HAL_OK;
		}
		// fill answer
		memcpy(&payload[0], &measurementRegister[startRegister], nOfRegister);
		break;

	case REGISTER_LOGIC:
		if (startRegister + nOfRegister > LOGIC_MAX)
		{
			// send out of bounds error
			messaging_respondErrorMessage(m, ILB_ERROR_OUT_OF_BOUNDS);
			return ILB_HAL_OK;
		}
		// fill answer
		memcpy(&payload[0], &logicRegister[startRegister], nOfRegister);
		break;
#endif // LIBRARY_LOGIC

	default:
		return ILB_HAL_ERROR;
	}

	// check if we qualify for minimal required answer length
	if (nOfRegister < 2)
	{
		nOfRegister = 2;
		payload[1] = 0x00;
	}
	// send message
	m->payloadSize = nOfRegister;
	messaging_sendAnswer(m, &payload[0], nOfRegister);

	// free memory
	free(payload);

	// signal we have handled the message
	return ILB_HAL_OK;
}

void register_udpateSpecificStatusRegister(uint8_t status)
{
	statusRegister[STATUS_STATE] = (statusRegister[STATUS_STATE] & 0x03) | ((statusRegister[STATUS_STATE] & 0x3F) << 2);
}

//
//	Implementation of private functions
//

#ifndef LIBRARY_LOGIC
/**
 * Update the information regarding telemetry
 */
static void updateTelemetryRegister(void)
{
	uint32_t onTime, burnTime = 0;
	telemetry_getCounter(&onTime, &burnTime);
	informationRegister[INFORMATION_ON_TIME_COUNTER_3] = ((onTime >> 24) & 0xFF);
	informationRegister[INFORMATION_ON_TIME_COUNTER_2] = ((onTime >> 16) & 0xFF);
	informationRegister[INFORMATION_ON_TIME_COUNTER_1] = ((onTime >> 8) & 0xFF);
	informationRegister[INFORMATION_ON_TIME_COUNTER_0] = (onTime & 0xFF);
	informationRegister[INFORMATION_BURN_TIME_COUNTER_3] = ((burnTime >> 24) & 0xFF);
	informationRegister[INFORMATION_BURN_TIME_COUNTER_2] = ((burnTime >> 16) & 0xFF);
	informationRegister[INFORMATION_BURN_TIME_COUNTER_1] = ((burnTime >> 8) & 0xFF);
	informationRegister[INFORMATION_BURN_TIME_COUNTER_0] = (burnTime & 0xFF);
}
#endif
