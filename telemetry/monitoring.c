/*
 * monitoring.c
 *
 *  Created on: 22.10.2021
 *      Author: A.Niggebaum
 *
 * Contains all monitoring functionality for
 * - driver temperature monitoring
 * - light engine output monitoring
 */

#include "../ilb.h"
#include "../register/register.h"
#include "../hardwareAPI/hardwareAPI.h"
#include "../lightEngine/lightEngine.h"
#include "temperature.h"

//
//	Private variables
//
static ADCReadings_t adcReadings;				//!< contains the "raw" ADC counts
static ADCReadings_t convertedReadings;			//!< contains the converted values to mV and mA

//
//	Function prototypes
//
void adcMeasurementCompleteCallback(void);
void convertADCReadingsToUnits(ADCReadings_t* input);

/**
 * Initialisation routine for all monitoring routines
 */
void monitoring_init(void)
{
	adcReadings.I1 = 0;
	adcReadings.U1 = 0;
	adcReadings.I2 = 0;
	adcReadings.U2 = 0;
	adcReadings.T  = 0;
}

/**
 * Regularly called function to trigger measurement
 */
void monitoring_tick(void)
{
	// start reading on channels
	ILB_HAL_StartADCMeasuremnt(&adcReadings, adcMeasurementCompleteCallback);
}

/**
 * Callback for complete mesurement
 */
void adcMeasurementCompleteCallback(void)
{
	// convert the values to mV and mA
	convertADCReadingsToUnits(&adcReadings);

#ifndef LIBRARY_LOGIC
	// push to register
	register_updateMeasurement(&convertedReadings, &adcReadings);
#endif

	// let the light engine know what the output is
	lightEngine_updateMeasuredOutput(&convertedReadings);
}

//
//	Internal: calibration of values
//
/**
 * Structure holding the correction for a type of ADC reading
 *
 * Assumed is a linear correction. This requires a factor and an offset. However, we can only do integer operations. Applying a factor
 * of 0.532 is therefore not possible. To solve this issue, all operations are made at a "magnitude". The example factor of 0.532 transforms to
 * 532 and a magnitude of 1000.
 */
typedef struct adcConversionStruct {
	uint16_t factor;					//!< correction factor
	int16_t offset;						//!< offset
	uint32_t magnitude;					//!< factor to apply the above corrections at (see above for explanation)
} adcConversionStructTypeDef;

//
//	Old values (Modules HW1.1 with 55V supply) pre 2022
//
//adcConversionStructTypeDef ADCVoltageConversion = {203, -11891, 10};
//adcConversionStructTypeDef ADCCurrentConversion = {535, -27054, 1000};

//
//	New values (Modules HW1.2 with 5V supuply) 2022 and later
//
adcConversionStructTypeDef ADCVoltageConversion = {183, 13, 10};
adcConversionStructTypeDef ADCCurrentConversion = {465, -48, 1000};

/**
 * Convert the ADC readings to units and store them in the appropriate core registers
 *
 * Takes the ADC readings of all 4 values that can be measured. Uses the appropriate calibration factors, converts the values to volts and amperes and
 * stores them in the appropriate registers of the core. The ADC values are expected to be calibrated against mV. E.g. a value of 3000 corresponds to a
 * measurement of 3000mV or 3.000V.
 *
 * @param ADC_U1	ADC read value for voltage on channel 1
 * @param ADC_A1	ADC read value for current on channel 1
 * @param ADC_U2	ADC read value for voltage on channel 2
 * @param ADC_A2	ADC read value for voltage on channel 2
 */
void convertADCReadingsToUnits(ADCReadings_t* input)
{
	// all ADC values come in mV. Our target values (internally) are in units of mV and mA.

	//
	//	Current conversion
	//
	int32_t A1tmp = (((int32_t)(input->I1) * (int32_t)ADCCurrentConversion.factor) + (int32_t)ADCCurrentConversion.offset)/(int32_t)ADCCurrentConversion.magnitude;
	int32_t A2tmp = (((int32_t)(input->I2) * (int32_t)ADCCurrentConversion.factor) + (int32_t)ADCCurrentConversion.offset)/(int32_t)ADCCurrentConversion.magnitude;
	// ==> yields the currents in mA

	//
	//	Voltage conversion
	//
	int32_t U1tmp = (((int32_t)(input->U1) * (int32_t)ADCVoltageConversion.factor) + (int32_t)ADCVoltageConversion.offset)/(int32_t)ADCVoltageConversion.magnitude;
	int32_t U2tmp = (((int32_t)(input->U2) * (int32_t)ADCVoltageConversion.factor) + (int32_t)ADCVoltageConversion.offset)/(int32_t)ADCVoltageConversion.magnitude;
	// ==> yields the voltages in mV


	//
	//	Check range and store in structure
	//
	if (A1tmp < 0) {
		A1tmp = 0;
	}
	if (A2tmp < 0) {
		A2tmp = 0;
	}
	if (U1tmp < 0) {
		U1tmp = 0;
	}
	if (U2tmp < 0) {
		U2tmp = 0;
	}

	convertedReadings.I1 = (uint32_t)A1tmp;		// leave at mA
	convertedReadings.U1 = (uint32_t)U1tmp/100;	// convert to units of 100mV
	convertedReadings.I2 = (uint32_t)A2tmp;		// leave at mA
	convertedReadings.U2 = (uint32_t)U2tmp/100; // convert to units of 100mV

	// NCT lookup
	uint8_t temperature = 0xFF;
	temperature_getTemperatureForMeasurement(&temperature, input->T);

	convertedReadings.T = temperature;
}

/**
 * Access telemetry values
 * @param target
 * @param type
 * @return
 */
ILB_HAL_StatusTypeDef telemetry_getTelemetryValue(uint8_t* target, TelemetryValue_t type)
{
	switch (type)
	{
	case TELEMETRY_DRIVER_TEMPERATURE:
		*target = convertedReadings.T;
		break;

	default:
		*target = 0x00;
	}
	return ILB_HAL_OK;
}
