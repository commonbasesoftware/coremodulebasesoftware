/**
 * @file counters.h
 *
 * @date 22.10.2021
 * @author A.Niggebaum
 *
 * @brief Burn and on time counter functionality
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_TELEMETRY_COUNTERS_H_
#define ILB_TELEMETRY_COUNTERS_H_

/**
 * Initialize all structures related to on and burn time counter. Also retreives any stored values from persistent memory
 */
void counters_init(void);

/**
 * Advance the timers by 1s. This routine MUST be called every 1s.
 */
void counters_tick1s(void);

#endif /* ILB_TELEMETRY_COUNTERS_H_ */
