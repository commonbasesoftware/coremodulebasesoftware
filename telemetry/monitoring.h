/**
 * @file monitoring.h
 *
 * @date 22.10.2021
 * @author A.Niggebaum
 *
 * @brief Functionality associated with measuring the power consumption on the secondary side.
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_TELEMETRY_MONITORING_H_
#define ILB_TELEMETRY_MONITORING_H_

/**
 * Initialises the monitoring functionality. This concerns power and temperature monitoring.
 */
void monitoring_init(void);

/**
 * Triggers the reading of the ADC channels and the conversion and interpretation of the values once the measurement is complete.
 */
void monitoring_tick(void);

#endif /* ILB_TELEMETRY_MONITORING_H_ */
