/*
 * counters.c
 *
 *  Created on: 22.09.2021
 *      Author: A.Niggebaum
 *
 * This file contains code and variables for the on time and burn time counters of the system
 *
 * Base unit is seconds. Storing the values in a 32bit variable allows for 136years of on time
 */

#include <stdint.h>
#include "../settings/settings.h"
#include "../hardwareAPI/hardwareAPI.h"

static uint32_t OnTimeCounterS = 0;		//!< on time counter in seconds
static uint32_t BurnTimeCounterS = 0;	//!< burn time counter in seconds
static uint16_t synchronisationCounter = 0;

void counters_init(void)
{
	// pull stored telemetry data from memory
	ILB_HAL_getTelemetry(&OnTimeCounterS, &BurnTimeCounterS);
}

/**
 * Accessor for counters
 * @param onTime
 * @param burnTime
 */
void telemetry_getCounter(uint32_t* onTime, uint32_t* burnTime)
{
	*onTime = OnTimeCounterS;
	*burnTime = BurnTimeCounterS;
}

/**
 * Tick routine for counters that should be triggered every second
 */
void counters_tick1s(void)
{
	// count up
	OnTimeCounterS++;
	uint16_t PWMA, PWMB = 0;
	// get PWM output
	ILB_HAL_core_getLightEngineOutput(&PWMA, &PWMB);
	if (PWMB != 0 || PWMB != 0)
	{
		BurnTimeCounterS++;
	}

	// handle syncrhonisation
	synchronisationCounter++;

	if (synchronisationCounter >= TELEMETRY_SYNC_INTERVAL_S)
	{
		// write to memeory
		ILB_HAL_storeTelemetry(OnTimeCounterS, BurnTimeCounterS);
		synchronisationCounter = 0;
	}
}
