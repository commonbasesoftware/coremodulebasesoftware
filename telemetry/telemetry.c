/*
 * telemetry.c
 *
 *  Created on: 22.10.2021
 *      Author: A.Niggebaum
 */

#include "../ilb.h"
#include "counters.h"
#include "monitoring.h"

/**
 * Initialisation routine of telemetry counter
 */
void telemetry_init(void)
{
	counters_init();
	monitoring_init();
}

/**
 * Tick routine of telemetry. This routine must be called very second
 */
void telemetry_tick(void)
{
	counters_tick1s();
	monitoring_tick();
}
