/**
 * @file telemetry.h
 *
 * @date 22.09.2021
 * @author A.Niggebaum
 *
 * @brief Telemetry handling and progression
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_TELEMETRY_TELEMETRY_H_
#define ILB_TELEMETRY_TELEMETRY_H_

/**
 * Initialises the telemetry: counters, power and temperature monitoring
 */
void telemetry_init(void);

/**
 * Progresses the telemetry one step forward. Expected to be executed every second.
 * Counts up the on and burn time counters by 1 second.
 */
void telemetry_tick(void);

/**
 * Get the counters for time the device was powered on and the time the device did output a non-zero PWM signal
 * @param onTime
 * @param burnTime
 */
void telemetry_getCounter(uint32_t* onTime, uint32_t* burnTime);

/**
 * Generic teletry interface for other values than timers
 * @param target
 * @param type
 * @return
 */
ILB_HAL_StatusTypeDef telemetry_getTelemetryValue(uint8_t* target, TelemetryValue_t type);

#endif /* ILB_TELEMETRY_TELEMETRY_H_ */
