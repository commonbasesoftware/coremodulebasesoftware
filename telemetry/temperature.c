/*
 * temperature.c
 *
 *  Created on: 11.01.2022
 *      Author: A.Niggebaum
 */

#include <stdint.h>
#include "temperature.h"

#define TEMP_LOOKUP_VOLTAGE_MIN_MV		870			// minimal value in temperature lookup table in mV
#define TEMP_LOOKUP_VOLTAGE_MAX_MV		3200		// maximal value in temperature lookup table in mV
#define TEMP_LOOKUP_VOLTAGE_STEP_MV		10			// step size in lookup table in mv
#define TEMP_LOOKUP_LENGTH				233			// length of lookup table

/**
 * Temperature look up table. The first entry corresponds to a measured voltage of TEMP_LOOKUP_VOLTAGE_MIN_MV,
 * the next to TEMP_LOOKUP_VOLTAGE_MIN_MV + TEMP_LOOKUP_VOLTAGE_STEP_MV, the following to TEMP_LOOKUP_VOLTAGE_MIN_MV +
 * 2 * TEMP_LOOKUP_VOLTAGE_STEP_MV, ... , up to TEMP_LOOKUP_VOLTAGE_MAX_MV.
 */
const uint8_t temperatureLookup[TEMP_LOOKUP_LENGTH] =   {125., 125., 124., 123., 123., 122., 122., 121., 120., 120., 119.,
	       	   	   	   	   	   	   	   119., 118., 118., 117., 116., 116., 115., 115., 114., 114., 113.,
									   113., 112., 112., 111., 111., 110., 110., 109., 109., 108., 108.,
									   107., 107., 106., 106., 105., 105., 104., 104., 103., 103., 103.,
									   102., 102., 101., 101., 100., 100.,  99.,  99.,  99.,  98.,  98.,
									   97.,  97.,  96.,  96.,  96.,  95.,  95.,  94.,  94.,  93.,  93.,
									   93.,  92.,  92.,  91.,  91.,  91.,  90.,  90.,  89.,  89.,  89.,
									   88.,  88.,  87.,  87.,  87.,  86.,  86.,  85.,  85.,  85.,  84.,
									   84.,  83.,  83.,  83.,  82.,  82.,  81.,  81.,  81.,  80.,  80.,
									   79.,  79.,  79.,  78.,  78.,  78.,  77.,  77.,  76.,  76.,  76.,
									   75.,  75.,  74.,  74.,  74.,  73.,  73.,  73.,  72.,  72.,  71.,
									   71.,  71.,  70.,  70.,  69.,  69.,  69.,  68.,  68.,  67.,  67.,
									   67.,  66.,  66.,  65.,  65.,  65.,  64.,  64.,  63.,  63.,  63.,
									   62.,  62.,  61.,  61.,  61.,  60.,  60.,  59.,  59.,  58.,  58.,
									   58.,  57.,  57.,  56.,  56.,  55.,  55.,  55.,  54.,  54.,  53.,
									   53.,  52.,  52.,  51.,  51.,  51.,  50.,  50.,  49.,  49.,  48.,
									   48.,  47.,  47.,  46.,  46.,  45.,  45.,  44.,  44.,  43.,  43.,
									   42.,  41.,  41.,  40.,  40.,  39.,  39.,  38.,  37.,  37.,  36.,
									   36.,  35.,  34.,  34.,  33.,  32.,  32.,  31.,  30.,  30.,  29.,
									   28.,  27.,  27.,  26.,  25.,  24.,  23.,  22.,  22.,  21.,  20.,
									   19.,  18.,  17.,  15.,  14.,  13.,  12.,  11.,   9.,   8.,   6.,
									   5.,   3.};

/**
 * Writes the converted temperature into the provided variable location
 *
 * @param temp			Converted temperature in °C
 * @param measurement	Measured ADC voltage in mv
 */
void temperature_getTemperatureForMeasurement(uint8_t* temp, uint32_t measurement)
{
	// obey boundaries
	if (measurement > TEMP_LOOKUP_VOLTAGE_MAX_MV)
	{
		measurement = TEMP_LOOKUP_VOLTAGE_MAX_MV;
	}

	if (measurement < TEMP_LOOKUP_VOLTAGE_MIN_MV)
	{
		measurement = TEMP_LOOKUP_VOLTAGE_MIN_MV;
	}

	// convert to index of array and write into provided variable
	uint8_t index = (measurement - TEMP_LOOKUP_VOLTAGE_MIN_MV)/TEMP_LOOKUP_VOLTAGE_STEP_MV;
	*temp = temperatureLookup[index];
}
