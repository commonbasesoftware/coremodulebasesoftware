/**
 * @file temperature.h
 *
 * @date 11.01.2022
 * @author A.Niggebaum
 *
 * @brief Temperature measurement handling
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_TELEMETRY_TEMPERATURE_H_
#define ILB_TELEMETRY_TEMPERATURE_H_

#include <stdint.h>

/**
 * Passes an ADC value through the lookup table to get the temperature equivalent in °C
 * @param temp			Temperature equivalent
 * @param measurement	ADC measurement in mV
 */
void temperature_getTemperatureForMeasurement(uint8_t* temp, uint32_t measurement);

#endif /* ILB_TELEMETRY_TEMPERATURE_H_ */
