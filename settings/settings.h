/**
 * @file settings.h
 *
 * @date 15.09.2021
 * @author A.Niggebaum
 *
 * @brief All settings related to the base software. Things such as masks, address ranges etc. are set here.
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_SETTINGS_SETTINGS_H_
#define ILB_SETTINGS_SETTINGS_H_

//
//	Automatic setting of read out protection
//
#define SKIP_CHIP_PROTECTION			// disable automatic chip protection for now
#define ILB_TARGET_RDP_LEVEL			1

//
//	Message sending and receival
//
#define ILB_MAX_MESSAGE_LENGTH			64
#define ILB_MESSAGE_RX_BUFFER_DEPTH     20                                      //!< Message buffer depth for for logic
#define ILB_MESSAGE_TX_BUFFER_DEPTH     20                                       //!< Message buffer depth for sending messages

#define ILB_MESSAGE_START_BYTE						0x23						//!< byte marking the start of an ILB message
#define ILB_MESSAGE_ESCAPE_BYTE						0x5C						//!< byte marking escape bytes
#define ILB_MESSAGE_ESCAPE_OPERATION(byte) 			(~byte)						//!< operation used on escaped bytes

#define ILB_MESSAGE_ADDRESS_BYTE_POSITION			0							//!< defines the byte index that contains the address byte
#define ILB_MESSAGE_LENGTH_BYTE_POSITION			1							//!< defines the byte index that contains the message length field
#define ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION		2							//!< defines the byte index that contains the byte defining the message type and identifier
#define ILB_MESSAGE_PAYLOAD_START					3
#define ILB_MESSAGE_NOF_EXTRA_BYTES					4							//!< number of bytes extra to ILB messages other than payload

#define ILB_MESSAGE_BROADCAST_ADDRESS		0x00								//!< Broadcast address
#define ILB_MESSAGE_BROADCAST_ADDRESS_CORE	0xFF								//!< additional broadcast address for core only
#define ILB_MESSAGE_IDENTIFIER_MASK     	0x3F                                //!< Mask for identifier
#define ILB_MESSAGE_IDENTIFIER_MIN      	0x01                                //!< Minimal possible value for identifier
#define ILB_MESSAGE_IDENTIFIER_MAX      	0x14                                //!< Maximal possible value for identifier
#define ILB_MESSAGE_TYPE_MASK           	0xC0

#define ILB_ADDRESS_MIN					0x01
#define ILB_ADDRESS_MAX					0xFE

//
//	Broadcast and status sync
//
#define SETTING_MIN_FADE_TIME_FOR_BROADCAST_MS		300									//!< if a fade time is below this value, only a single broadcast is emitted when the fade has finished

//
//	Power cycle flag detection
//
#define RESET_CYCLE_FLAG_COUNT			0x05

//
//	Security: Key Challenge
//
#define ILB_KEY_VALIDITY_MS						200								//!< maximal time an issued key is valid for

//
//	Reset
//
#define RESET_DELAY_MS					1000

//
//	Light engine handling and settings
//
#define CORE_ATTACHED_LIGHT_ENGINE_FADE_RESOLUTION_MS		20					//!< resolution of the fade of an attached light engine

//
//	Telemetry
//
#define TELEMETRY_SYNC_INTERVAL_S				(60 * 30)								//!< synchronisation interval of telemetry (on and burn time counter) in seconds to memeory

//
//	Logic upload
//
#define LOGIC_UPLOAD_TIMEOUT_MS				500

//
//	Conversion of secondary side measured power output to primary side power
//
// the relation between secondary and primary side power is assumed to be linear
// P_in = ((P_out * factor) + offset)/divisor
// the divisor allows the use of integers and avoid floating point calculations
#define SETTING_PRIM_SEC_CONV_OFFSET		71				//!< offset used in the calculation
#define SETTING_PRIM_SEC_CONF_FACTOR		112				//!< factor used in calculation
#define SETTING_PRIM_SEC_CONF_DIVISOR		100				//!< divisor used in calculation

//
//	Test mode
//
#define SETTINGS_TEST_MODE_DURATION_MS		10000			//!< time before the test mode is automatically exited

#endif /* ILB_SETTINGS_SETTINGS_H_ */
