/**
 * @brief state.h
 *
 * @date 17.09.2021
 * @author A.Niggebaum
 *
 * @brief State of the base software and state handling
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_BASESOFTWARE_STATE_H_
#define ILB_BASESOFTWARE_STATE_H_

#include <stdint.h>
#include "../ilb.h"

/**
 * Enum for base software mode
 */
typedef enum ilbBaseStatus {
	ILB_BASE_INITIALISING		= 0x01,		//!< indicates that the base software is initialising
	ILB_BASE_ERROR				= 0x02,		//!< indicates that an error occured and the base software cannot operate normally
#ifndef LIBRARY_LOGIC
	ILB_BASE_ON_OFF				= 0x03,		//!< indicates that the base software is operating without a logic
	ILB_BASE_FOLLOWER			= 0x04,		//!< indicates that the base software is operating in follower mode
	ILB_BASE_DRIVER_PERIPHERAL	= 0x05,		//!< indicates that the base software operates as an addressable driver peripheral
	ILB_BASE_LOGIC				= 0x06,		//!< indicates that the base software is operating on a (later) loaded logic
	ILB_BASE_SETTINGS_LOADING	= 0x07,		//!< indicates that the base software is loading settings
	ILB_BASE_LOGIC_LOADING		= 0x08,		//!< indicates that the base software is loading logic
#endif
	ILB_BASE_WAIT_FOR_REBOOT	= 0x09, 	//!< idle state waiting for reboot. All other tasks are suspended
#ifdef LIBRARY_LOGIC
	ILB_BASE_LIBRARY_LOGIC		= 0x0A,		//!< indicates that the base software is executing a logic that is compiled in via a library
#endif
	ILB_BASE_TEST				= 0x0B,
} ILBBaseStatusTypeDef;

/**
 * Set the state of the base software. Handles any eventually associated blink pattern. This function should be the primary way to set a state.
 * @param newState New state for the base software
 */
void ilb_setState(ILBBaseStatusTypeDef newState);

/**
 * Get the current state of the base software
 * @return State of the base software
 */
ILBBaseStatusTypeDef ilb_getState(void);

/**
 * Get the state of the logic
 * @return 0x00 if logic is running without error, error code (logic dependent) otherwise
 */
uint8_t ilb_getLogicState(void);

/**
 * Trigger a delayed reboot. The delay is added to allow any eventually pending confirmation messages to be sent before the device power cycles.
 * @param delayMS
 */
void ilb_rebootSystem(uint16_t delayMS);

#endif /* ILB_BASESOFTWARE_STATE_H_ */
