/**
 * @file ilbstd.h
 *
 *  @date 15.09.2021
 *  @author A.Niggebaum
 *
 *  @brief Commonly used macros and helpers as well decorations for section placelemnt
 *
 *  @ingroup BaseSoftwareGroup
 */

#ifndef ILB_ILBSTD_H_
#define ILB_ILBSTD_H_

#include <stdint.h>
#include <stddef.h>

#define IS_BIT_SET(var,bit) ((var & bit) == bit)								//!< checks if a bit in a byte is set

// Placement of interfaces
#define __interfaceBaseSoftware __attribute__((section(".sectionInterfaceBaseSoftware")))		//!< places the code in the interface section for the base software
#define __interfaceLogic		__attribute__((section(".sectionInterfaceLogic")))				//!< places the code in the interface section for the logic

#if defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050) /* ARM Compiler V6 */
  #ifndef __weak
    #define __weak  __attribute__((weak))
  #endif
  #ifndef __packed
    #define __packed  __attribute__((packed))
  #endif
#elif defined ( __GNUC__ ) && !defined (__CC_ARM) /* GNU Compiler */
  #ifndef __weak
    #define __weak   __attribute__((weak))
  #endif /* __weak */
  #ifndef __packed
    #define __packed __attribute__((__packed__))
  #endif /* __packed */

  #define __NOINLINE __attribute__ ( (noinline) )

#endif /* __GNUC__ */

#endif /* ILB_ILBSTD_H_ */
