/**
 * @test.h
 *
 * @date 20.01.2022
 * @author A.Niggebaum
 *
 * @brief Self test related functionality
 *
 * @ingroup BaseSoftwareGroup
 */

#ifndef ILB_SELFTEST_TEST_H_
#define ILB_SELFTEST_TEST_H_

/**
 * Start the the automated test sequence
 *
 * This mode applies a blink pattern to the indicator and a sequence of PWM outputs.
 * The module resets automatically after 10s
 *
 * @param m Message triggering the seqeunce
 */
void selfTest_startSequence(ILBMessageStruct* m);

/**
 * Apply provided output in test mode.
 *
 * This mode is not exited. A power cycle is necessary.
 *
 * @param m Message triggering the output.
 */
void selfTest_setSetting(ILBMessageStruct* m);

#endif /* ILB_SELFTEST_TEST_H_ */
