/*
 * test.c
 *
 *  Created on: 20.01.2022
 *      Author: A.Niggebaum
 */

#include "../ilb.h"
#include "../messaging/messaging.h"
#include "../scheduler/scheduler.h"
#include "../indicator/indicatorPattern.h"
#include "../baseSoftware/state.h"
#include "../settings/settings.h"

typedef enum TestSequenceStateEnum {
	TEST_START,
	TEST_PWM1_ONLY,
	TEST_PWM2_ONLY,
	TEST_PWM12,
	TEST_END
} TestSequence_t;

static TestSequence_t sequenceStatus = TEST_END;

//
//	Function prototypes
//
/**
 * Worker for automated self test sequence.
 */
void selfTest_worker(void);
void leaveSelfTestMode(void);

/**
 * Start the autonomous sequence
 */
void selfTest_startSequence(ILBMessageStruct* m)
{
	// respond ok
	messaging_respondErrorMessage(m, ILB_OK);

	// store current state of system
	// ILBBaseStatusTypeDef currentState = ilb_getState();

	// set the new state (This prevents execution of the logic)
	ilb_setState(ILB_BASE_TEST);

	// reset sequence
	sequenceStatus = TEST_START;

	// start the worker
	selfTest_worker();
}

/**
 * Apply a as part of a test sequence
 *
 * @param m
 */
void selfTest_setSetting(ILBMessageStruct* m)
{
	// respond ok
	messaging_respondErrorMessage(m, ILB_OK);

	// set state
	ilb_setState(ILB_BASE_TEST);

	// apply the output for
	ILB_HAL_core_setLightEngine(1000 * m->payload[1] / 0xFF, 1000 * m->payload[2] / 0xFF);

	// set the indicator
	ILB_HAL_setIndicator(0x03, BLINK_PATTERN_TEST_MODE_GREEN, BLINK_PATTERN_TEST_MODE_RED);
}

/**
 * Worker for test
 */
void selfTest_worker(void)
{
	switch (sequenceStatus)
	{
	case TEST_START:
		// beginning of test, proceed to next state
		sequenceStatus = TEST_PWM1_ONLY;
		scheduler_registerTimerFunction(selfTest_worker, 10);
		break;

	case TEST_PWM1_ONLY:
		// set the green led
		ILB_HAL_setIndicator(DEVICE_GREEN_INDICATOR, 0xFF, 0x00);
		// set the output
		ILB_HAL_core_setLightEngine(1000, 0);
		// trigger again
		scheduler_registerTimerFunction(selfTest_worker, 1000);
		// advance a step
		sequenceStatus++;
		break;

	case TEST_PWM2_ONLY:
		// set the red led
		ILB_HAL_setIndicator(DEVICE_RED_INDICATOR, 0x00, 0xFF);
		// set the PWM output
		ILB_HAL_core_setLightEngine(0, 1000);
		// trigger again
		scheduler_registerTimerFunction(selfTest_worker, 1000);
		// advance
		sequenceStatus++;
		break;

	case TEST_PWM12:
		// set both leds
		ILB_HAL_setIndicator(DEVICE_RED_INDICATOR | DEVICE_GREEN_INDICATOR, 0xFF, 0xFF);
		// set the pwm output
		ILB_HAL_core_setLightEngine(1000, 1000);
		// trigger again
		scheduler_registerTimerFunction(selfTest_worker, 1000);
		// advance
		sequenceStatus++;
		break;

	default:
		sequenceStatus = TEST_END;
		// end of sequence. Trigger reset
		ILB_HAL_systemReset();
	}
}

/**
 * Leave the self test mode through a system reset.
 */
void leaveSelfTestMode(void)
{
	ILB_HAL_systemReset();
}
